<?php
class Pagination_catalog {
	public $total = 0;
	public $page = 1;
	public $limit = 20;
	public $num_links = 8;
	public $url = '';
	public $text_first = '|&lt;';
	public $text_last = '&gt;|';
	public $text_next = '&gt;';
	public $text_prev = '&lt;';

	public function render() {
		$total = $this->total;

		if ($this->page < 1) {
			$page = 1;
		} else {
			$page = $this->page;
		}

		if (!(int)$this->limit) {
			$limit = 10;
		} else {
			$limit = $this->limit;
		}

		$num_links = $this->num_links;
		$num_pages = ceil($total / $limit);

		$this->url = str_replace('%7Bpage%7D', '{page}', $this->url);

//        <div class="catalog_page-count">
//                    <a class="page-count_btn page-count_btn-prev" href="#"></a>
//                    <ul class="page-count_list">
//                        <li class="page-count_list_item">
//<a
//                                class="page-count_list_item_link page-count_list_item_link-selected" href="#"><span
//                                class="page-count_list_item_link_index">1</span></a></li>
//                        <li class="page-count_list_item"><a class="page-count_list_item_link" href="#"> <span
//                                class="page-count_list_item_link_index">2</span></a></li>
//                        <li class="page-count_list_item"><a class="page-count_list_item_link" href="#"> <span
//                                class="page-count_list_item_link_index">3</span></a></li>
//                        <li class="page-count_list_item"><a class="page-count_list_item_link" href="#"> <span
//                                class="page-count_list_item_link_index">4</span></a></li>
//                        <li class="page-count_list_item"><a class="page-count_list_item_link" href="#"> <span
//                                class="page-count_list_item_link_index">5</span></a></li>
//                    </ul>
//                    <a class="page-count_btn page-count_btn-next" href="#"></a></div>
//            </div>

		$output = '<div class="catalog_page-count">';

		if ($page > 1) {
//			$output .= '<li><a href="' . str_replace('{page}', 1, $this->url) . '">' . $this->text_first . '</a></li>';
//			$output .= '<li><a href="' . str_replace('{page}', $page - 1, $this->url) . '">' . $this->text_prev . '</a></li>';
            $output .= '<a class="page-count_btn page-count_btn-prev" href="' . str_replace('{page}', $page - 1, $this->url) . '"></a>';
		}

        $output .= '<ul class="page-count_list">';

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);

				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}

				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= '<li class="page-count_list_item">
                                    <a class="page-count_list_item_link page-count_list_item_link-selected" href="' . str_replace('{page}', $i, $this->url) . '">
                                        <span class="page-count_list_item_link_index">' . $i . '</span></a></li>';

				} else {
					$output .= '<li class="page-count_list_item">
                                    <a class="page-count_list_item_link" href="' . str_replace('{page}', $i, $this->url) . '">
                                        <span class="page-count_list_item_link_index">' . $i . '</span></a></li>';
				}
			}
		}

        $output .= '</ul>';

		if ($page < $num_pages) {
//			$output .= '<li><a href="' . str_replace('{page}', $page + 1, $this->url) . '">' . $this->text_next . '</a></li>';
//			$output .= '<li><a href="' . str_replace('{page}', $num_pages, $this->url) . '">' . $this->text_last . '</a></li>';
            $output .= '<a class="page-count_btn page-count_btn-next" href="' . str_replace('{page}', $num_pages, $this->url) . '"></a>';
		}

		$output .= '</div>';

		if ($num_pages > 1) {
			return $output;
		} else {
			return '';
		}
	}
}