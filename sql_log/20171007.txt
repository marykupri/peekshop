ALTER TABLE `oc_option_value` ADD `size_code` VARCHAR(64) NULL DEFAULT NULL AFTER `color_code`;
ALTER TABLE `oc_order_option` ADD `code` VARCHAR(10) NULL DEFAULT NULL AFTER `value`;