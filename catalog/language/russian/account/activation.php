<?php

$_['heading_title']  = 'Подтверждение e-mail';

$_['text_home']   = 'Главная';
$_['text_activation']   = 'Подтверждение e-mail';
$_['text_message']   = 'Ваш e-mail был успешно подтвержден!';
$_['error_text_message']   = 'Что-то пошло не так, свяжитесь с нами через форму контактов!';
$_['error_wrong_activation']   = 'Отсутствует часть данных активации';
$_['error_not_mail_found'] = 'Отсутствует пользователь с такими данными';
?>