<?php
class ModelAccountActivation extends Model {

	public function getCustomerForActivation ($email) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($email) . "'");

		$query_data = array (
			'customer_id' => $query->row['customer_id'],
			'email' => $query->row['email']
		);

		return $query_data;
	}

	public function updateEmailActivation ($email) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET email_approved = '1' WHERE email = '" . $this->db->escape($email) . "'");
	}

}
?>