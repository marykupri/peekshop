<?php
class ModelTotalVoucher extends Model {
	public function addVoucher($order_id, $data,$customer_id) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "voucher SET order_id = '" . (int)$order_id . "',
		customer_id =  '" . $customer_id . "',
		code = '" . $this->db->escape($data['code']) . "',
		amount = '" . (float)$data['amount'] . "', status = '1', date_added = NOW()");

		return $this->db->getLastId();
	}

    /*from_name = '" . $this->db->escape($data['from_name']) . "',
    from_email = '" . $this->db->escape($data['from_email']) . "',
    to_name = '" . $this->db->escape($data['to_name']) . "',
    to_email = '" . $this->db->escape($data['to_email']) . "',
    voucher_theme_id = '" . (int)$data['voucher_theme_id'] . "',
    message = '" . $this->db->escape($data['message']) . "', */

	public function disableVoucher($order_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "voucher SET status = '0' WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getVoucher($code) {
		$status = true;

		$voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "voucher v  WHERE v.code = '" . $this->db->escape($code) . "' AND v.status = '1'");

		if ($voucher_query->num_rows) {
			if ($voucher_query->row['order_id']) {
				$implode = array();

				foreach ($this->config->get('config_complete_status') as $order_status_id) {
					$implode[] = "'" . (int)$order_status_id . "'";
				}

				$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$voucher_query->row['order_id'] . "' AND order_status_id IN(" . implode(",", $implode) . ")");

                if (!$order_query->num_rows) {
					$status = false;
				}

				$order_voucher_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$voucher_query->row['order_id'] . "' AND voucher_id = '" . (int)$voucher_query->row['voucher_id'] . "'");

				if (!$order_voucher_query->num_rows) {
					$status = false;
				}
			}

			$voucher_history_query = $this->db->query("SELECT SUM(amount) AS total FROM `" . DB_PREFIX . "voucher_history` vh WHERE vh.voucher_id = '" . (int)$voucher_query->row['voucher_id'] . "' GROUP BY vh.voucher_id");

			if ($voucher_history_query->num_rows) {
				$amount = $voucher_query->row['amount'] + $voucher_history_query->row['total'];
			} else {
				$amount = $voucher_query->row['amount'];
			}

			if ($amount <= 0) {
				$status = false;
			}
		} else {
			$status = false;
		}

		if ($status) {
			return array(
				'voucher_id'       => $voucher_query->row['voucher_id'],
				'code'             => $voucher_query->row['code'],
				'amount'           => $amount,
				'status'           => $voucher_query->row['status'],
				'date_added'       => $voucher_query->row['date_added']
			);
		}
	}
    public function getAllCustomerVouchers() {
        $vouchers_data = array();

        $sql = "SELECT v.date_added,v.voucher_id,v.code,vh.voucher_history_id,v.amount
                FROM " . DB_PREFIX . "voucher v
                left join `" . DB_PREFIX . "order` o on o.order_id = v.order_id
                left join `" . DB_PREFIX . "order_voucher` ov on ov.order_id = v.order_id AND ov.voucher_id = v.voucher_id
                left join `" . DB_PREFIX . "voucher_history` vh on vh.voucher_id = v.voucher_id
                WHERE v.customer_id = '" . (int)$this->customer->getId() . "' order by v.date_added asc";

        $query = $this->db->query($sql);

        if($query->num_rows > 0)
            foreach ($query->rows as $result) {
                if(!$result['voucher_history_id'])
                    $vouchers_data['active'][] = array(
                        'voucher_id' => $result['voucher_id'],
                        'code' => $result['code'],
                        'amount' => $result['amount'],
                        'date_added' => $result['date_added']
                    );
                else
                    $vouchers_data['archive'][] = array(
                        'voucher_id' => $result['voucher_id'],
                        'code' => $result['code'],
                        'amount' => $result['amount'],
                        'date_added' => $result['date_added']
                    );
            }

        return $vouchers_data;
    }


    public function getVouchers() {
        $vouchers_data = array();
        $implode = array();

        foreach ($this->config->get('config_complete_status') as $order_status_id) {
            $implode[] = "'" . (int)$order_status_id . "'";
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "voucher v
        left join `" . DB_PREFIX . "order` o on o.order_id = v.order_id
        left join `" . DB_PREFIX . "order_voucher` ov on ov.order_id = v.order_id AND ov.voucher_id = v.voucher_id
        WHERE v.customer_id = '" . (int)$this->customer->getId() . "' and status = 1  AND o.order_status_id IN(" . implode(",", $implode) . ") 
            and v.voucher_id not in (select DISTINCT voucher_id from oc_voucher_history)");

        if($query->num_rows > 0)
            foreach ($query->rows as $result) {
                $vouchers_data[] = array(
                    'voucher_id' => $result['voucher_id'],
                    'code' => $result['code'],
                    'amount' => $result['amount']
                );
            }

        return $vouchers_data;
    }

	public function getTotal(&$total_data, &$total, &$taxes) {
		if (isset($this->session->data['voucher'])) {
			$this->load->language('total/voucher');

			$this->load->model('total/coupon');

			$voucher_info = $this->getVoucher($this->session->data['voucher']);

			if ($voucher_info) {
				if ($voucher_info['amount'] > $total) {
					$amount = $total;
				} else {
					$amount = $voucher_info['amount'];
				}

				if ($amount > 0) {
					$total_data[] = array(
						'code'       => 'voucher',
						'title'      => sprintf($this->language->get('text_voucher'), $this->session->data['voucher']),
						'value'      => -$amount,
						'sort_order' => $this->config->get('voucher_sort_order')
					);

					$total -= $amount;
				} else {
					unset($this->session->data['voucher']);
				}
			} else {
				unset($this->session->data['voucher']);
			}
		}
	}

	public function confirm($order_info, $order_total) {
		$code = '';

		$start = strpos($order_total['title'], '(') + 1;
		$end = strrpos($order_total['title'], ')');

		if ($start && $end) {
			$code = substr($order_total['title'], $start, $end - $start);
		}

		if ($code) {
			$voucher_info = $this->getVoucher($code);

			if ($voucher_info) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "voucher_history` SET voucher_id = '" . (int)$voucher_info['voucher_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
			} else {
	            return $this->config->get('config_fraud_status_id');
	        }
		}
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "voucher_history` WHERE order_id = '" . (int)$order_id . "'");
	}
}
