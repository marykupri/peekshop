<?php
class ModelCatalogCategory extends Model {
	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row;
	}

	public function getCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}

	public function getCategoryFilters($category_id) {
		$implode = array();

		$query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order 
                        FROM " . DB_PREFIX . "filter f 
                        LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) 
                        LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) 
                        WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' and f.filter_group_id != 3
                        GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();

				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}

				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}

		return $filter_group_data;
	}

	public function getCategoryFiltersPrices($category_id){
	    $sql = "SELECT max(op.price) as maxprice, min(op.price) as minprice 
                  FROM `oc_product` op 
                    join `oc_product_to_category` pc on pc.product_id = op.product_id 
                      WHERE pc.category_id = " . $category_id . " and op.status = 1";
        $query = $this->db->query($sql);
        $arr = array();
        $arr['minprice'] = $query->row['minprice'];
        $arr['maxprice'] = $query->row['maxprice'];

        return $arr;
	}

    public function getCategoryFiltersBrands($category_id){
        $sql = "SELECT op.manufacturer_id as filter_id,name
                FROM `oc_product` op
                left join oc_manufacturer m on m.manufacturer_id = op.manufacturer_id
                join `oc_product_to_category` pc on pc.product_id = op.product_id 
                WHERE pc.category_id = " . $category_id . " and op.status = 1
                GROUP BY op.manufacturer_id";

        $query = $this->db->query($sql);

        $filter_array = array();

        if(!empty($query->rows))
            $filter_array = array(
                'filter_group_id' => '3',
                'name' => 'Бренд',
                'name_alias' => 'brand',
                'filter' => $query->rows
            );

        return $filter_array;
    }
    public function getCategoryFiltersColors($category_id){
        $sql = "SELECT ovd.option_value_id as filter_id,ovd.name
                FROM `oc_product` p
                join `oc_product_option` po on po.product_id = p.product_id
                join `oc_product_option_value` pov on po.product_option_id = pov.product_option_id
                join `oc_option_value_description` ovd on pov.option_value_id = ovd.option_value_id
                join `oc_product_to_category` pc on pc.product_id = p.product_id 
                WHERE pc.category_id = " . $category_id . " and ovd.language_id = " . (int)$this->config->get('config_language_id') . " and po.option_id = 13 and p.status = 1 and pov.active = 1
                GROUP BY ovd.option_value_id";

        $query = $this->db->query($sql);

        $filter_array = array();

        if(!empty($query->rows))
            $filter_array = array(
                'filter_group_id' => '2',
                'name' => 'Цвет',
                'name_alias' => 'color',
                'filter' => $query->rows
            );

        return $filter_array;
    }

    public function getCategoryFiltersSizes($category_id){
        $sql = "SELECT ovd.option_value_id as filter_id,ovd.name
                FROM `oc_product` p
                join `oc_product_option` po on po.product_id = p.product_id
                join `oc_product_option_value` pov on po.product_option_id = pov.product_option_id
                join `oc_option_value_description` ovd on pov.option_value_id = ovd.option_value_id
                join `oc_product_to_category` pc on pc.product_id = p.product_id 
                WHERE pc.category_id = " . $category_id . " and ovd.language_id = " . (int)$this->config->get('config_language_id') . " and po.option_id = 11 and p.status = 1 and pov.active = 1
                GROUP BY ovd.option_value_id";

        $query = $this->db->query($sql);

        $filter_array = array();

        if(!empty($query->rows))
            $filter_array = array(
                'filter_group_id' => '4',
                'name' => 'Размер',
                'name_alias' => 'size',
                'filter' => $query->rows
            );

        return $filter_array;
    }


	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row['total'];
	}

	public function getSearchFilters($search){
	    $sql = "select p.product_id, pf.filter_id , fd.name, fd.filter_group_id, fgd.name as group_name
                from " . DB_PREFIX . "product p 
                left join " . DB_PREFIX . "manufacturer m on m.manufacturer_id = p.manufacturer_id
                join " . DB_PREFIX . "product_description pd on p.product_id = pd.product_id 
                join " . DB_PREFIX . "product_to_category pc on pc.product_id = p.product_id
                left join " . DB_PREFIX . "product_filter pf on pf.product_id = p.product_id
                left join " . DB_PREFIX . "filter f on f.filter_id = pf.filter_id
                left join " . DB_PREFIX . "filter_description fd on pf.filter_id = fd.filter_id and fd.language_id = " . (int)$this->config->get('config_language_id') . "
                left join " . DB_PREFIX . "filter_group_description fgd on fd.filter_group_id = fgd.filter_group_id and fgd.language_id = " . (int)$this->config->get('config_language_id') . "
                left join " . DB_PREFIX . "filter_group fg on fg.filter_group_id = fgd.filter_group_id
                where ( pd.name like '%$search%' or m.name like '%$search%' or pd.description like '%$search%') and p.status=1 ORDER BY fg.sort_order, LCASE(fgd.name),f.sort_order, LCASE(fd.name)";

        $query = $this->db->query($sql);

        $filter = array();
        $filter_arr = array();
        $product = array();

        foreach ($query->rows as $row){
            if(!in_array($row['product_id'], $product))
                $product[] = $row['product_id'];
            if(!in_array($row['filter_id'] , $filter_arr)) {
                $filter_arr[] = $row['filter_id'];
                $filter[$row['filter_group_id']][] = array(
                    'filter_id' => $row['filter_id'],
                    'name'      => $row['name']
                );
            }
        }

        $filter_group = array();
        $filter_arr = array();

        foreach ($query->rows as $row){
            if(!in_array($row['filter_group_id'] , $filter_arr)) {
                $filter_arr[] = $row['filter_group_id'];
                $filter_group[] = array(
                    'filter_group_id' => $row['filter_group_id'],
                    'name'            => $row['group_name'],
                    'filter'          => $filter[$row['filter_group_id']]
                );
            }
        }

        $data_arr = array();

        if(!empty($filter_group) || !empty($product))
            $data_arr = array('filters' => $filter_group, 'products' => $product);

        return $data_arr;

    }

    public function getSearchFiltersPrices($products_id){
        $sql = "SELECT max(op.price) as maxprice, min(op.price) as minprice FROM `oc_product` op  WHERE op.product_id in ( " . implode(',' , $products_id) . " ) ";
        $query = $this->db->query($sql);
        $arr = array();
        $arr['minprice'] = $query->row['minprice'];
        $arr['maxprice'] = $query->row['maxprice'];

        return $arr;
    }

    public function getSearchFiltersBrands($products_id){
        $sql = "SELECT p.manufacturer_id as filter_id,name
                FROM `oc_product` p
                left join oc_manufacturer m on m.manufacturer_id = p.manufacturer_id
                join `oc_product_to_category` pc on pc.product_id = p.product_id 
                WHERE p.product_id in ( " . implode(',' , $products_id) . " ) and p.status = 1
                GROUP BY p.manufacturer_id";

        $query = $this->db->query($sql);
        $filter_array = array();

        if(!empty($query->rows))
            $filter_array = array(
                'filter_group_id' => '3',
                'name' => 'Бренд',
                'name_alias' => 'brand',
                'filter' => $query->rows
            );

        return $filter_array;
    }

    public function getSearchFiltersColors($products_id){
        $sql = "SELECT ovd.option_value_id as filter_id,ovd.name
                FROM `oc_product` p
                join `oc_product_option` po on po.product_id = p.product_id
                join `oc_product_option_value` pov on po.product_option_id = pov.product_option_id
                join `oc_option_value_description` ovd on pov.option_value_id = ovd.option_value_id
                join `oc_product_to_category` pc on pc.product_id = p.product_id 
                WHERE p.product_id in ( " . implode(',' , $products_id) . " )  and ovd.language_id = " . (int)$this->config->get('config_language_id') . " and po.option_id = 13 and p.status = 1
                GROUP BY ovd.option_value_id";

        $query = $this->db->query($sql);
        $filter_array = array();

        if(!empty($query->rows))
            $filter_array = array(
                'filter_group_id' => '2',
                'name' => 'Цвет',
                'name_alias' => 'color',
                'filter' => $query->rows
            );

        return $filter_array;
    }

    public function getSearchFiltersSizes($products_id){
        $sql = "SELECT ovd.option_value_id as filter_id,ovd.name
                FROM `oc_product` p
                join `oc_product_option` po on po.product_id = p.product_id
                join `oc_product_option_value` pov on po.product_option_id = pov.product_option_id
                join `oc_option_value_description` ovd on pov.option_value_id = ovd.option_value_id
                join `oc_product_to_category` pc on pc.product_id = p.product_id 
                WHERE p.product_id in ( " . implode(',' , $products_id) . " )  and ovd.language_id = " . (int)$this->config->get('config_language_id') . " and po.option_id = 11 and p.status = 1
                GROUP BY ovd.option_value_id";

        $query = $this->db->query($sql);
        $filter_array = array();

        if(!empty($query->rows))
            $filter_array = array(
                'filter_group_id' => '4',
                'name' => 'Размер',
                'name_alias' => 'size',
                'filter' => $query->rows
            );

        return $filter_array;
    }
}