<?php
class ControllerAccountActivation extends Controller {
	private $error = array ();

	public function index (){
		$this->language->load('account/activation');
//        echo (md5(1));die;

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_activation'),
            'href' => $this->url->link('account/activation', '', 'SSL')
        );

        $this->load->model('account/customer');

		$this->load->model('account/activation');

		if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validate()) {

			$activation_data = $this->model_account_activation->getCustomerForActivation($this->request->get['email']);

			if ((md5($activation_data['customer_id'])) == $this->request->get['activation']){

				$data['description'] = $this->language->get('text_message');

                $this->model_account_activation->updateEmailActivation($this->request->get['email']);

			} else {
				$data['description'] = $this->language->get('error_text_message');
			}

		} else {
			$data['description'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');


		if (isset($this->error['message'])) {
			$data['error'] = $this->error['message'];
		} else {
			$data['error'] = '';
		}

//        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/activation/email.tpl')) {
//			$this->template = $this->config->get('config_template') . '/template/activation/email.tpl';
//		} else {
//			$this->template = 'default/template/activation/email.tpl';
//		}

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/activation.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/activation.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/activation.tpl', $data));
        }

// 		  $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
//        $this->response->setOutput($this->load->view('account/activation.tpl', $data));
	}

	private function validate() {

        if (!isset($this->request->get['activation'])) {
			$this->error['message'] = $this->language->get('error_wrong_activation');
		}

		if (!preg_match(EMAIL_PATTERN, $this->request->get['email'])) {
			$this->error['message'] = $this->language->get('error_wrong_activation');
		}else { $email = $this->request->get['email'];}

		if(!$this->model_account_customer->getTotalCustomersByEmail($email)) {
			$this->error['message'] = $this->language->get('error_not_mail_found');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>