<?php
class ControllerAccountAccount extends Controller {
    private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('common/home', '', 'SSL'));
		}

        $this->load->language('account/account');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->load->model('account/customer');

            $this->model_account_customer->editCustomer($this->request->post);
            $newsletter = isset($this->request->post['newsletter']) ? $this->request->post['newsletter'] : 0;
            $this->model_account_customer->editNewsletter($newsletter);

            $this->load->model('account/address');

            foreach ($this->request->post['address'] as $address => $data) {
                if($address > 0 )
                    $this->model_account_address->editAddress($address,$data);
                else
                    $this->model_account_address->addAddress($data);
            }

//                $this->session->data['success'] = $this->language->get('text_success');
//
//                // Add to activity log
//                $this->load->model('account/activity');
//
//                $activity_data = array(
//                    'customer_id' => $this->customer->getId(),
//                    'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
//                );
//
//                $this->model_account_activity->addActivity('edit', $activity_data);

            $this->response->redirect($this->url->link('account/account', '', 'SSL'));
        }

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('account/account', '', 'SSL');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', 'SSL');
		$data['password'] = $this->url->link('account/password', '', 'SSL');
		$data['address'] = $this->url->link('account/address', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['return'] = $this->url->link('account/return', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['recurring'] = $this->url->link('account/recurring', '', 'SSL');

		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', 'SSL');
		} else {
			$data['reward'] = '';
		}

        if (isset($this->error['firstname'])) {
            $data['error_firstname'] = $this->error['firstname'];
        } else {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $data['error_lastname'] = $this->error['lastname'];
        } else {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

        if (isset($this->error['male'])) {
            $data['error_male'] = $this->error['male'];
        } else {
            $data['error_male'] = '';
        }


		//---вывод информации о клиенте -->

        $this->load->model('account/customer');

        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
        }

        if (isset($this->request->post['firstname'])) {
            $data['firstname'] = $this->request->post['firstname'];
        } elseif (!empty($customer_info)) {
            $data['firstname'] = $customer_info['firstname'];
        } else {
            $data['firstname'] = '';
        }

        if (isset($this->request->post['lastname'])) {
            $data['lastname'] = $this->request->post['lastname'];
        } elseif (!empty($customer_info)) {
            $data['lastname'] = $customer_info['lastname'];
        } else {
            $data['lastname'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } elseif (!empty($customer_info)) {
            $data['email'] = $customer_info['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } elseif (!empty($customer_info)) {
            $data['telephone'] = $customer_info['telephone'];
        } else {
            $data['telephone'] = '';
        }

        if (isset($this->request->post['male'])) {
            $data['male'] = $this->request->post['male'];
        } elseif (!empty($customer_info)) {
            $data['male'] = $customer_info['male'];
        } else {
            $data['male'] = '';
        }

        if (isset($this->request->post['birthday'])) {
            $data['birthday'] = $this->request->post['birthday'];
        } elseif (!empty($customer_info)) {
            $data['birthday'] = $customer_info['birthday'];
        } else {
            $data['birthday'] = '';
        }

        $data['newsletter'] = $this->customer->getNewsletter();

        //--- вывод адресов клиента--->

        $this->load->model('account/address');

        $data['addresses'] = array();

        $results = $this->model_account_address->getAddresses();

        foreach ($results as $result) {
//            if ($result['address_format']) {
//                $format = $result['address_format'];
//            } else {
//                if($result['apartment'])
//                    $format = '{postcode}, {country}, {city}, {address_1}, {house}, {apartment}';
//                else
//                    $format = '{postcode}, {country}, {city}, {address_1}, {house}';
//            }
//
//            $find = array('{address_1}','{city}','{postcode}','{country}','{house}','{apartment}');

            $data['addresses'][] = array(
                'address_id' => $result['address_id'],
                'address_1'  => $result['address_1'],
                'city'       => $result['city'],
                'postcode'   => $result['postcode'],
                'country'    => $result['country'],
                'house'      => $result['house'],
                'apartment'  => $result['apartment'],
                'default'    => $this->customer->getAddressId() == $result['address_id'],
                'delete'     => $this->url->link('account/address/delete', 'address_id=' . $result['address_id'], 'SSL')
            );

//            $data['addresses'][] = array(
//                'address_id' => $result['address_id'],
//                'address'    => str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format)))),
//                'default'    => $this->customer->getAddressId() == $result['address_id']
//            );
        }

        //------

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/account.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
		}
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    protected function validate() {

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

//        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
//            $this->error['email'] = $this->language->get('error_email');
//        }
//
//        if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
//            $this->error['warning'] = $this->language->get('error_exists');
//        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        if (!isset($this->request->post['male'])) {
            $this->error['male'] = $this->language->get('error_male');
        }

        foreach ($this->request->post['address'] as $address => $adr) {
            if ((utf8_strlen(trim($adr['address_1'])) < 3) || (utf8_strlen(trim($adr['address_1'])) > 128)) {
                $this->error['address_1'] = $this->language->get('error_address_1');
            }

            if ((utf8_strlen(trim($adr['city'])) < 2) || (utf8_strlen(trim($adr['city'])) > 128)) {
                $this->error['city'] = $this->language->get('error_city');
            }

            if ((utf8_strlen(trim($adr['postcode'])) < 2 || utf8_strlen(trim($adr['postcode'])) > 10)) {
                $this->error['postcode'] = $this->language->get('error_postcode');
            }

            if ((utf8_strlen(trim($adr['country'])) < 2 || utf8_strlen(trim($adr['country'])) > 25)) {
                $this->error['country'] = $this->language->get('error_country');
            }

            if ((utf8_strlen(trim($adr['house_number'])) < 1 || utf8_strlen(trim($adr['house_number'])) > 6)) {
                $this->error['house_number'] = $this->language->get('house_number');
            }

            if (!is_numeric($adr['apartment_number'])) {
                $this->error['apartment_number'] = $this->language->get('error_apartment_number');
            }

            if(empty($adr['address_1']) && empty($adr['city']) && empty($adr['postcode']) && empty($adr['country']) && empty($adr['house_number']) && empty($adr['apartment_number']))
                unset($this->request->post['address'][$address]);
        }

        // Custom field validation
        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

        foreach ($custom_fields as $custom_field) {
            if (($custom_field['location'] == 'account') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
                $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
            }
        }

        return !$this->error;
    }
}
