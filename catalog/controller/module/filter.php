<?php
class ControllerModuleFilter extends Controller {
	public function index() {
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

        if (isset($this->request->get['search'])) {
            $search = $this->request->get['search'];
        } else {
            $search = '';
        }

//        if (isset($this->request->get['filter'])) {
//            $data['filter_category'] = explode(',', $this->request->get['filter']);
//        } else {
//            $data['filter_category'] = array();
//        }

        if (isset($this->request->get['brands'])) {
            $filter_choose_brands = explode(',', $this->request->get['brands']);
        } else {
            $filter_choose_brands = array();
        }

        if (isset($this->request->get['colors'])) {
            $filter_choose_colors = explode(',', $this->request->get['colors']);
        } else {
            $filter_choose_colors = array();
        }

        if (isset($this->request->get['sizes'])) {
            $filter_choose_sizes = explode(',', $this->request->get['sizes']);
        } else {
            $filter_choose_sizes = array();
        }

        $data['filter_category']['brand'] = $filter_choose_brands;
        $data['filter_category']['size'] = $filter_choose_sizes;
        $data['filter_category']['color'] = $filter_choose_colors;

        if (isset($this->request->get['category_id'])) {
            $data['category_id'] = explode(',', $this->request->get['category_id']);
            $data['count_checked_category'] = count($data['category_id']);
        } else {
            $data['category_id'] = array();
            $data['count_checked_category'] = 0;
        }

        if (isset($this->request->get['minprice'])) {
            $data['price_category']['minprice'] = $this->request->get['minprice'] . $this->currency->getSymbolRight();
            $data['price_category_result']['minprice'] = $this->request->get['minprice'];
        } else {
            $data['price_category']['minprice'] = '';
            $data['price_category_result']['minprice'] = '';
        }

        if (isset($this->request->get['maxprice'])) {
            $data['price_category']['maxprice'] = $this->request->get['maxprice'] . $this->currency->getSymbolRight();
            $data['price_category_result']['maxprice'] = $this->request->get['maxprice'];
        } else {
            $data['price_category']['maxprice'] = '';
            $data['price_category_result']['maxprice'] = '';
        }

        $data['filter_category']['maxprice'] = $data['price_category_result']['maxprice'];
        $data['filter_category']['minprice'] = $data['price_category_result']['minprice'];

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['limit'])) {
            $url .= '&limit=' . $this->request->get['limit'];
        }

		$category_id = end($parts);

        $filter_groups = array();
        $filter_prices = array();

		$this->load->model('catalog/category');
        $this->load->model('catalog/product');

		$category_info = $this->model_catalog_category->getCategory($category_id);

        $this->load->language('module/filter');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['button_filter'] = $this->language->get('button_filter');

		if($search){

            $filters_info = $this->model_catalog_category->getSearchFilters($search);

            if($filters_info){

                $data['action'] = str_replace('&amp;', '&', $this->url->link('product/search', 'search=' . $search . $url));
                $data['no_action'] = str_replace('&amp;', '&', $this->url->link('product/search', 'search=' . $search ));

//                $filter_groups = $filters_info['filters'];

                $filter_prices = $this->model_catalog_category->getSearchFiltersPrices($filters_info['products']);

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $filter_prices['minprice'] = $this->currency->format($this->tax->calculate($filter_prices['minprice'], 9, $this->config->get('config_tax')));
                    $filter_prices['maxprice'] = $this->currency->format($this->tax->calculate($filter_prices['maxprice'], 9, $this->config->get('config_tax')));
                } else {
                    $filter_prices['minprice'] = false;
                    $filter_prices['maxprice'] = false;
                }

                if(empty($data['price_category']['maxprice']))
                    $data['price_category']['maxprice'] = $filter_prices['maxprice'];
                if(empty($data['price_category']['minprice']))
                    $data['price_category']['minprice'] = $filter_prices['minprice'];

                $filter_brands = $this->model_catalog_category->getSearchFiltersBrands($filters_info['products']);
                $filter_colors = $this->model_catalog_category->getSearchFiltersColors($filters_info['products']);
                $filter_sizes =  $this->model_catalog_category->getSearchFiltersSizes($filters_info['products']);

                if(!empty($filter_brands))
                    $filter_groups[] = $filter_brands;
                if(!empty($filter_sizes))
                    $filter_groups[] = $filter_sizes;
                if(!empty($filter_colors))
                    $filter_groups[] = $filter_colors;

                $data['filterSeacrhCategoris'] = $this->model_catalog_product->levelCategoryFilterSearch();
                $data['searchProductParents'] = $this->model_catalog_product->getParentsProductCategory($filters_info['products']);
            }

        }

		if ($category_info) {

			$data['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));
            $data['no_action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] ));

            $data['filter_prices'] = array();

            $filter_prices = $this->model_catalog_category->getCategoryFiltersPrices($category_id);

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $filter_prices['minprice'] = $this->currency->format($this->tax->calculate($filter_prices['minprice'], 9, $this->config->get('config_tax')));
                $filter_prices['maxprice'] = $this->currency->format($this->tax->calculate($filter_prices['maxprice'], 9, $this->config->get('config_tax')));
            } else {
                $filter_prices['minprice'] = false;
                $filter_prices['maxprice'] = false;
            }

            if(empty($data['price_category']['maxprice']))
                $data['price_category']['maxprice'] = $filter_prices['maxprice'];
            if(empty($data['price_category']['minprice']))
                $data['price_category']['minprice'] = $filter_prices['minprice'];

            $filter_brands = $this->model_catalog_category->getCategoryFiltersBrands($category_id);

            $filter_colors = $this->model_catalog_category->getCategoryFiltersColors($category_id);

            $filter_sizes =  $this->model_catalog_category->getCategoryFiltersSizes($category_id);

//			$filter_groups = $this->model_catalog_category->getCategoryFilters($category_id);
            if(!empty($filter_brands))
                $filter_groups[] = $filter_brands;
            if(!empty($filter_sizes))
                $filter_groups[] = $filter_sizes;
            if(!empty($filter_colors))
                $filter_groups[] = $filter_colors;
        }

        $data['filter_prices'] = $filter_prices;

        $isFiltered = false;

         if(!empty($data['category_id']) ||
            !empty($data['filter_category']['size']) ||
            !empty($data['filter_category']['brand']) ||
            !empty($data['filter_category']['color']) ||
            !empty($data['filter_category']['minprice']) ||
            !empty($data['filter_category']['maxprice']))
                $isFiltered = true;

        $data['isFiltered'] = $isFiltered;

        if(empty($data['price_category'])){
            $data['price_category']['minprice'] = $filter_prices['minprice'];
            $data['price_category']['maxprice'] = $filter_prices['maxprice'];
        }

        $data['filter_groups'] = array();

        if ($filter_groups) {
            foreach ($filter_groups as $filter_group) {
                $childen_data = array();

                $count_checked = 0;

                foreach ($filter_group['filter'] as $filter) {
                    $filter_data = array(
                        'filter_category_id' => $category_id,
                        'filter_filter'      => $filter['filter_id']
                    );

                    $childen_data[] = array(
                        'filter_id' => $filter['filter_id'],
                        'name'      => $filter['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '')
                    );

                    if(in_array($filter['filter_id'],$data['filter_category'][$filter_group['name_alias']])) $count_checked++;
                }

                $data['filter_groups'][] = array(
                    'filter_group_id' => $filter_group['filter_group_id'],
                    'name'            => $filter_group['name'],
                    'name_alias'      => $filter_group['name_alias'],
                    'filter'          => $childen_data,
                    'count_checked'   => $count_checked
                );
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/filter.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/filter.tpl', $data);
            } else {
                return $this->load->view('default/template/module/filter.tpl', $data);
            }
        }
	}
}