<?php
class ControllerModuleCarousel extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

        $views = '';

        if($setting['banner_id'] == 8)
            $views = '/template/module/carousel33.tpl';
        elseif($setting['banner_id'] == 10)
            $views = '/template/module/carousel10.tpl';
        else {
            $views = '/template/module/carousel.tpl';

            $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
            $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');
        }

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
                    'description' => $result['description'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		$data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . $views)) {
			return $this->load->view($this->config->get('config_template') . $views, $data);
		} else {
			return $this->load->view('default/'.$views, $data);
		}
	}
}