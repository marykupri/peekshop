<?php

class ControllerModuleLivesearch extends Controller
{
    public function index()
    {
        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        $limit = 10;

        if (!empty($_GET['query'])) {

            $sql = "SELECT p.product_id, pd.name, p.price, m.name as manufacturer FROM `" . DB_PREFIX . "product` p
        left join `" . DB_PREFIX . "product_description` pd on pd.product_id = p.product_id
            left join `" . DB_PREFIX . "manufacturer` m on p.manufacturer_id = m.manufacturer_id
            join `" . DB_PREFIX . "product_to_category` pc on pc.product_id = p.product_id
            WHERE ( pd.name LIKE '%" . $this->db->escape($this->request->get['query']) . "%' 
                    or m.name LIKE '%" . $this->db->escape($this->request->get['query']) . "%' 
                    or pd.description LIKE '%" . $this->db->escape($this->request->get['query']) . "%' )
                    and pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                    ORDER BY pd.name ASC LIMIT $limit";

            $query = $this->db->query($sql);

            if (!$query->rows) {

                $data[] = '<div class="search-field_show-result"><span class="search-field_show-result_text">Нет совпадений</span></div>';

            } else {

                $cutname = 350;

                $all = array();
                foreach ($query->rows as $row) {
                    if (in_array($row['product_id'], $all)) {
                        continue;
                    } // исключение дублирования
//
                    $sel = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product` WHERE `product_id`='" . $row['product_id'] . "'");
//
                    if ($sel->rows[0]['status'] == 0) {
                        continue;
                    } // если товар отключен, то не показываем его в поиске

                    $imgwidth = 30;
                    if ($imgwidth < 5) {
                        $imgwidth = 5;
                    }
                    $imgheight = 30;
                    if ($imgheight < 5) {
                        $imgheight = 5;
                    }
                    $original_image = $sel->rows[0]['image'];
                    if (is_file(DIR_IMAGE . $original_image)) {
                        $this->load->model('tool/image');
                        $image1 = $this->model_tool_image->resize($original_image, $imgwidth, $imgheight);
                    } else {
                        $image1 = $this->model_tool_image->resize('no_image.png', $imgwidth, $imgheight);
                    }
                    $image = '<img class="search-field_list_item_link_photo" src="' . $image1 . '">';

                    $original_price = $sel->rows[0]['price'];
                    $query_special_price = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_special` WHERE `product_id`='" . $row['product_id'] . "' ORDER BY `priority` ASC, `product_special_id` DESC");
                    if (isset($query_special_price->rows[0])) {
                        $special_price = $query_special_price->rows[0]['price'];
                        $price2 = " " . $this->currency->format($special_price, $this->config->get('config_currency'));
                        $style_price1 = " style='text-decoration:line-through'";
                    } else {
                        $style_price1 = "";
                        $price2 = "";
                    }

                    if ($original_price > 0) {
                        $this->load->model('catalog/product');
                        $price1 = $this->currency->format($original_price, $this->config->get('config_currency'));
                        $price = "<div style='float:right;margin-left:5px'><small> <span$style_price1>$price1</span>$price2</small></div>";
                        $price = $price1;
                    } else {
                        $price = "";
                    }

                    $name = html_entity_decode($row['name']);
                    if (strlen($name) > $cutname) {
                        $name = substr($name, 0, $cutname) . "...";
                    }

                    $manufacturer = html_entity_decode($row['manufacturer']);

                    $product_path = $this->model_catalog_product->getProductPath($row['product_id']);

                    $href = 'index.php?route=product/product&path='.$product_path.'&product_id=' . $row['product_id'];

                    $data[] = ' <li class="search-field_list_item"><a class="search-field_list_item_link" href="' . $href . '">' . $image . '<span class="search-field_list_item_link_name">' . $manufacturer . ' - ' . $name . ' - ' . $price . '</span><div class="search-field_fill"></div></a></li>';

                    $all[] = $row['product_id'];
                }

                $data[] = ' <div class="search-field_show-result"><a href="index.php?route=product/search&search=' . $this->request->get['query'] . '"><span class="search-field_show-result_text">Показать все результаты поиска</span></a></div>';

            }

            $data = "['" . implode("','", $data) . "']";
            print $data;


//            $this->response->addHeader('Content-Type: application/json');
//            $this->response->setOutput($data);
        }
    }

}

?>