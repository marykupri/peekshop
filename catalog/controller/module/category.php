<?php
class ControllerModuleCategory extends Controller {
	public function index() {
		$this->load->language('module/category');

		$data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->request->get['search'])) {
//            $filter_data = array(
//                'filter_name' => $this->request->get['search']
//            );
//
//            $results = $this->model_catalog_product->getProducts($filter_data);
//
//            $product_id_arr = array();
//
//            foreach ($results as $product){
//                $product_id_arr[] = $product['product_id'];
//            }
//
//            $parents = $this->model_catalog_product->getParentsProductCategory($product_id_arr);
            $search = $this->request->get['search'];

        } else {
            $search = '';
//            $parents = array();
        }

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

        if (isset($parts[2])) {
            $data['child2_id'] = $parts[2];
        } else {
            $data['child2_id'] = 0;
        }

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        if(!$search) {
            foreach ($categories as $category) {
                $children_data = array();

                if ($category['category_id'] == $data['category_id']) {

                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach ($children as $child) {
                        $children2_data = array();

//                        if ($child['category_id'] == $data['child_id']) {
                            $children2 = $this->model_catalog_category->getCategories($child['category_id']);

                            $children2_data = array();
                            foreach ($children2 as $child2) {
                                $filter_data = array('filter_category_id' => $child2['category_id'], 'filter_sub_category' => true);

                                $children2_data[] = array(
                                    'category_id' => $child2['category_id'],
                                    'name' => $child2['name'],
                                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'])
                                );
                            }
//                        }

                        $filter_data = array(
                            'filter_category_id' => $category['category_id'],
                            'filter_sub_category' => true
                        );

                        $children_data[] = array(
                            'category_id' => $child['category_id'],
                            'name' => $child['name'],
                            'children' => $children2_data,
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                        );
                    }

                    $data['categories'][] = array(
                        'category_id' => $category['category_id'],
                        'name' => $category['name'],
                        'children' => $children_data,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );

                }
            }
        }else {
            foreach ($categories as $category) {
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    $children2_data = array();

                    $children2 = $this->model_catalog_category->getCategories($child['category_id']);

                    $children2_data = array();
                    foreach ($children2 as $child2) {
                        $filter_data = array('filter_category_id' => $child2['category_id'], 'filter_sub_category' => true);

                        $children2_data[] = array(
                            'category_id' => $child2['category_id'],
                            'name' => $child2['name'],
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child2['category_id'])
                        );
                    }

                    $filter_data = array(
                        'filter_category_id' => $category['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name' => $child['name'],
                        'children' => $children2_data,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                $data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'],
                    'children' => $children_data,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category.tpl', $data);
		}
	}
}