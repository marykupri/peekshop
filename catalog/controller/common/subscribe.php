<?php
class ControllerCommonSubscribe extends Controller{
    public function index(){

        $this->load->model('extension/extension');
        $this->load->model('account/customer');

        $isSubscribed = false;

        if($this->request->post['email']){

            $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

            if($customer_info){
                if(!$customer_info['newsletter']) {
                    $this->model_account_customer->editNewsletterCustomerId($customer_info['customer_id']);
                    $isSubscribed = true;
                }

            }else{
                $subscriber = $this->model_account_customer->getTotalSubscriberByEmail($this->request->post['email']);

                if(!$subscriber) {
                    $this->model_account_customer->addSubscriberByEmail($this->request->post['email']);
                    $isSubscribed = true;
                }

            }
        }

    }

    function sendMessageSubscriber(){
        $this->load->language('mail/subcribe');

        $subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

//        $message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

//        if (!$customer_group_info['approval']) {
//            $message .= $this->language->get('text_login') . "\n";
//        } else {
//            $message = $this->language->get('text_approval') . "\n";
//        }

        $message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
        $message .= $this->language->get('text_services') . "\n\n";
        $message .= $this->language->get('text_thanks') . "\n";
        $message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($data['email']);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);
        $mail->setText($message);
        $mail->send();
    }
}
?>