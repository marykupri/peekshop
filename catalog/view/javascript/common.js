function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).ready(function () {
    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Currency
    $('#currency .currency-select').on('click', function (e) {
        e.preventDefault();

        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

        $('#currency').submit();
    });

    // Language
    $('#language a').on('click', function (e) {
        e.preventDefault();

        $('#language input[name=\'code\']').attr('value', $(this).attr('href'));

        $('#language').submit();
    });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function () {
        url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;
    });

    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    // Menu
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }
    });

    // Product List
    $('#list-view').click(function () {
        $('#content .product-grid > .clearfix').remove();

        //$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
        $('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');

        localStorage.setItem('display', 'list');
    });

    // Product Grid
    $('#grid-view').click(function () {
        // What a shame bootstrap does not take into account dynamically loaded columns
        cols = $('#column-right, #column-left').length;

        if (cols == 2) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
        } else {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
        }

        localStorage.setItem('display', 'grid');
    });

    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
    } else {
        $('#grid-view').trigger('click');
    }

    // Checkout
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });

    $('.size-select_dropdown_list_link').click(function () {
        $('ul.size-select_dropdown_list li').removeClass('size-select_dropdown_list_item_select');
        $(this).parent().addClass('size-select_dropdown_list_item_select');
    })

    $('.js-add-product-to-cart-close').click(function () {
        $(this).parent().parent().removeClass('modal-active');
    })

    $('.added-goods_btns_btn-continue').click(function () {
        $(this).parent().parent().parent().removeClass('modal-active');
    })

    $(document).on('click', '.js-account-address-item', function () {
        var text = $(this).text();
        var id = $(this).data('id');
        var arr = text.split(', ');

        $('.account-data_form_address input[name=postcode]').val(arr[0]);
        $('.account-data_form_address input[name=country]').val(arr[1]);
        $('.account-data_form_address input[name=city]').val(arr[2]);
        $('.account-data_form_address input[name=address_1]').val(arr[3]);
        $('.account-data_form_address input[name=house_number]').val(arr[4]);
        $('.account-data_form_address input[name=apartment_number]').val(arr[5]);
        $('.account-data_form_address input[name=address_id]').val(id);
    })

    $(document).on('click', '.js-delivery-address-order', function () {
        var text = $(this).text();
        var arr = text.split(', ');

        $('.order-product_form_fields_wrap .order-product_form_fields_content[name=postcode]').val(arr[0]);
        $('.order-product_form_fields_wrap .order-product_form_fields_content[name=country]').val(arr[1]);
        $('.order-product_form_fields_wrap .order-product_form_fields_content[name=city]').val(arr[2]);
        $('.order-product_form_fields_wrap .order-product_form_fields_content[name=address_1]').val(arr[3]);
        $('.order-product_form_fields_wrap .order-product_form_fields_content[name=house_number]').val(arr[4]);
        $('.order-product_form_fields_wrap .order-product_form_fields_content[name=apartment_number]').val(arr[5]);
    })

    $(document).on('click', '.js-address-delete', function () {
        var id = $(this).data('id');
        var this_parent = $(this).parent();
        $.ajax({
            url: 'index.php?route=account/address/delete',
            type: 'get',
            data: {'address_id': id},
            // dataType: 'json',
            success: function (json) {
                console.log(this_parent);
                $(this_parent).remove();
                // location.reload();
                // $('.account-data_form_address input.account-data_field_personal-data').val('');
                // var address = postcode + ', ' + country + ', ' + city + ', ' + address_1 + ', ' + house_number + ', ' + apartment_number;
                // $("<li class='account-data_choose-location_list-item js-account-address-item' data-id='" + json + "'>" + address + "</li>").appendTo($(".account-data_choose-location_list"));
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });

    })

    $(document).on('click', '.js-address-delete_nosave', function () {
        var this_parent = $(this).parent();
        this_parent.remove();
    })

    $('.js-order-voucher').on('click', function () {

        var code = $(this).data('code');
        var summa = $('.js-order-result-summa').text();

        $.ajax({
            url: 'index.php?route=total/voucher/voucher',
            type: 'post',
            data: 'voucher=' + encodeURIComponent(code),
            dataType: 'json',
            success: function (json) {

                summa = summa.replace(" ", "");
                var amount = json.amount;
                amount = amount.replace(" ", "");
                summa = parseInt(summa) - parseInt(amount);
                summa = thousandSeparator(summa) + json.symbol;

                $('.js-coupon-sum').text('-' + json.amount);
                $('.js-total-price_sum').text(summa);
            }
        });
    });


    $('.js-order-coupon_btn').on('click', function () {
        var coupon = $('.js-order-coupon').val();
        var summa = $('.js-order-result-summa').text();

        $.ajax({
            url: 'index.php?route=total/coupon/coupon',
            type: 'post',
            data: 'coupon=' + encodeURIComponent(coupon),
            dataType: 'json',
            success: function (json) {

                console.log(json);
                // $('.alert').remove();
                //
                if (json['error']) {
                    $('.order-coupon-error-msg').html(json['error']);
                }
                //
                // if (json['redirect']) {
                // 	location = json['redirect'];
                // }
            }
        });
    });

    var thousandSeparator = function (str) {
        var parts = (str + '').split('.'),
            main = parts[0],
            len = main.length,
            output = '',
            i = len - 1;

        while (i >= 0) {
            output = main.charAt(i) + output;
            if ((len - i) % 3 === 0 && i > 0) {
                output = ' ' + output;
            }
            --i;
        }

        if (parts.length > 1) {
            output += '.' + parts[1];
        }
        return output;
    };

    $('.js-product_form-order').on('click', function () {

        var isFormVailid = true;

        var country = $('.order-product_form_fields_wrap input[name=country]').val().replace(' ', '');
        var city = $('.order-product_form_fields_wrap input[name=city]').val().replace(' ', '');
        var postcode = $('.order-product_form_fields_wrap input[name=postcode]').val().replace(' ', '');
        var address_1 = $('.order-product_form_fields_wrap input[name=address_1]').val().replace(' ', '');
        var house = $('.order-product_form_fields_wrap input[name=house_number]').val().replace(' ', '');
        var apartment = $('.order-product_form_fields_wrap input[name=apartment_number]').val().replace(' ', '');
        var firstname = $('.order-product_form_fields_wrap input[name=firstname]').val().replace(' ', '');
        var telephone = $('.order-product_form_fields_wrap input[name=telephone]').val().replace(' ', '');

        if (country.length <= 0) {
            $('.order-product_form_fields_wrap input[name=country]').css('border', '1px solid red');
            isFormVailid = false;
        }
        if (city.length <= 0) {
            $('.order-product_form_fields_wrap input[name=city]').css('border', '1px solid red');
            isFormVailid = false;
        }
        if (postcode.length <= 0) {
            $('.order-product_form_fields_wrap input[name=postcode]').css('border', '1px solid red');
            isFormVailid = false;
        }
        if (address_1.length <= 0) {
            $('.order-product_form_fields_wrap input[name=address_1]').css('border', '1px solid red');
            isFormVailid = false;
        }
        if (house.length <= 0) {
            $('.order-product_form_fields_wrap input[name=house_number]').css('border', '1px solid red');
            isFormVailid = false;
        }
        if (apartment.length <= 0) {
            $('.order-product_form_fields_wrap input[name=apartment_number]').css('border', '1px solid red');
            isFormVailid = false;
        }

        if ( firstname.length <= 0) {
            $('.order-product_form_fields_wrap input[name=firstname]').css('border', '1px solid red');
            isFormVailid = false;
        }
        if (telephone.length <= 0) {
            $('.order-product_form_fields_wrap input[name=telephone]').css('border', '1px solid red');
            isFormVailid = false;
        }
        if($('.order-product_form_fields_wrap input[name=email]').length > 0) {
            var email = $('.order-product_form_fields_wrap input[name=email]').val();
            //тут надо проверить, что такой email еще не зарегистрирован
            if (email.replace(' ', '').length <= 0) {
                $('.order-product_form_fields_wrap input[name=email]').css('border', '1px solid red');
                isFormVailid = false;
            }
        }
        if($('.order-product_form_fields_wrap input[name=password]').length > 0) {
            var password = $('.order-product_form_fields_wrap input[name=password]').val().replace(' ', '');
            if (password.length <= 0) {
                $('.order-product_form_fields_wrap input[name=password]').css('border', '1px solid red');
                isFormVailid = false;
            }
        }

        if(isFormVailid)
            $('.ordering_order-product_form').submit();
    })

    var costShippingOrder = function(amount){
        var summa = $('.js-order-result-summa').text();
        summa = summa.replace(" ", "");
        amount = amount.replace(" ", "");
        summa = parseInt(summa) + parseInt(amount);
        var symbol = '';

        summa = thousandSeparator(summa);
        amount = thousandSeparator(parseInt(amount));

        $.get( "/index.php?route=common/currency/getcurrencySymbol", function( data ) {
            $('.js-total-price_sum').text(summa + data);
            $('.js-shipping-sum').text(amount + data);
        });
    }

    var amount = $('.order-product_form_btns_wrap').find('input[name=shipping_method_code]:checked').data('price');
    if(typeof amount !== 'undefined'){
        costShippingOrder(amount);
    }

    $('.order-product_form_btns_wrap input[name=shipping_method_code]').on('click', function (){
        var amount = $(this).data('price');
        costShippingOrder(amount);
    })


});

$(document).ready(function () {
    var suggest_count = 0;
    var input_initial_value = '';
    var suggest_selected = 0;

    var brand = [];

    var manufacturer = $(".search_manufacturer").parent().parent();

    var list = manufacturer.find('p');

    list.each(function (element) {
        brand.push($(this).text().toLowerCase());
    });

    // читаем ввод с клавиатуры
    $(".search_manufacturer").keyup(function (I) {
        // определяем какие действия нужно делать при нажатии на клавиатуру

        switch (I.keyCode) {
            // игнорируем нажатия на эти клавишы
            case 13:  // enter
            case 27:  // escape
            case 38:  // стрелка вверх
            case 40:  // стрелка вниз
                break;

            default:
                // производим поиск только при вводе более 2х символов
                if ($(this).val().length > 1) {
                    input_initial_value = $(this).val().toLowerCase();

                    manufacturer.find('li').css('display', 'none');
                    var arr = [];
                    arr = findPartial(brand, input_initial_value);

                    if (arr.length > 0) {
                        manufacturer.find('li').each(function (element) {
                            if (arr.indexOf(element) != -1) $(this).css('display', 'block');
                        });
                    }
                } else {
                    manufacturer.find('li').css('display', 'block');
                }
        }
    })
})

function findPartial(a, s) {
    var result = [];
    for (var i = 0; i < a.length; ++i) {
        if (a[i].indexOf(s) >= 0) {
            result.push(i);
        }
    }

    return result;
}

// Cart add remove functions
var cart = {
    'add': function (product_id, quantity) {

        var sizeblock = $('.aside_size-select').length;
        var size = $('.size-select_dropdown_list_item_select').data("value");
        var option = $('.size-select_dropdown_list').data("name");

        if (sizeblock > 0 && typeof size == 'undefined') {
            $('.js-product-view-size-select-list').addClass('size-select_dropdown-active');
        } else {
            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&option[' + option + '] = ' + size,
                dataType: 'json',
                // beforeSend: function() {
                // 	$('#cart > button').button('loading');
                // },
                // complete: function() {
                // 	$('#cart > button').button('reset');
                // },
                success: function (json) {
                    $('.js-add-product-to-cart').addClass('modal-active');

                    // $('.alert, .text-danger').remove();
                    //
                    // if (json['redirect']) {
                    // 	location = json['redirect'];
                    // }
                    //
                    // if (json['success']) {
                    // 	$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    //
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('.right-nav_unit_number').text(json['total']);
                    }, 100);
                    // 	$('html, body').animate({ scrollTop: 0 }, 'slow');
                    //
                    // 	$('#cart > ul').load('index.php?route=common/cart/info ul li');
                    // }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    },
    'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function () {

    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {

                // $('#wishlist-total span').html(json['total']);
                // $('#wishlist-total').attr('title', json['total']);

                // $('.product-description_in-basket_liked').addClass('product-description_in-basket_liked-active');

                $('.product_' + product_id).addClass('clothes_list_item-liked');
                $("li.product_" + product_id + " div button").replaceWith('<button type="button" class="clothes_like" onclick="wishlist.remove(' + product_id + ');"></button>');

                $("button.product-description_in-basket_liked").replaceWith('<button type="button" class="product-description_in-basket_liked product-description_in-basket_liked-active" onclick="wishlist.remove(' + product_id + ');"></button>')

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/remove',
            type: 'post',
            data: 'remove=' + product_id,
            // dataType: 'json',
            success: function (json) {

                // $('#wishlist-total span').html(json['total']);
                // $('#wishlist-total').attr('title', json['total']);

                // $('.product-description_in-basket_liked').removeClass('product-description_in-basket_liked-active');

                $('.product_' + product_id).removeClass('clothes_list_item-liked');
                $("li.product_" + product_id + " div button").replaceWith('<button type="button" class="clothes_like" onclick="wishlist.add(' + product_id + ');"></button>');

                $("button.product-description_in-basket_liked").replaceWith('<button type="button" class="product-description_in-basket_liked" onclick="wishlist.add(' + product_id + ');"></button>')

                $('.clothes_list-favorites li.product_' + product_id).css('display', 'none');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});

// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);
