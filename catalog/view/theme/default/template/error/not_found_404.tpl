<?php echo $header; ?>

<section class="page404"><img class="page404_background" src="catalog/view/theme/default/images/404-page/404-page-bgr.jpg">
  <div class="page404_content"><h2 class="page404_content_title">СТРАНИЦА НЕ НАЙДЕНА</h2>
    <p class="page404_content_description">ВЫ ПРОШЛИ ПО НЕСУЩЕСТВУЮЩЕЙ ССЫЛКЕ.</p>
    <p class="page404_content_description">ВЕРНИТЕЛЬ НА <a href='/' class='page404_content_description_link'>ГЛАВНУЮ</a>
      ИЛИ В <a href='#' class='page404_content_description_link'>КАТАЛОГ</a></p></div>
</section>

<?php echo $footer; ?>