<?php echo $header; ?>

<!---section class="page404"><img class="page404_background" src="catalog/view/theme/default/images/404-page/404-page-bgr.jpg">
  <div class="page404_content"><h2 class="page404_content_title">СТРАНИЦА НЕ НАЙДЕНА</h2>
    <p class="page404_content_description">ВЫ ПРОШЛИ ПО НЕСУЩЕСТВУЮЩЕЙ ССЫЛКЕ.</p>
    <p class="page404_content_description">ВЕРНИТЕЛЬ НА <a href='/' class='page404_content_description_link'>ГЛАВНУЮ</a>
      ИЛИ В <a href='#' class='page404_content_description_link'>КАТАЛОГ</a></p></div>
</section --->

<section class="app_content">
  <section class="offer-page">
    <div class="catalog_breadcrumbs">
      <ul class="breadcrumbs_list">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="offer-page_content"><h2 class="offer-page_content_title"><?php echo $heading_title; ?></h2>
        <p class="offer-page_content_description"><?php echo $text_error; ?></p></div>
    </div>
  </section>
</section>

<script>var pageName = 'ErrorPage';</script>

<!---div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_error; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div---->


<?php echo $footer; ?>