<div class="search-field">
    <div class="search-field_wrap" id="search">
      <input class="search-field_field" id="search-input" name="search" type="text" placeholder="Поиск" value="<?php echo $search; ?>">
      <div class="search-field_cls-btn"></div>
    </div>

    <ul id="search_advice_wrapper"></ul>

</div>