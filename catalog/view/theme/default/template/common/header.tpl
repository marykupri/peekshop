<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <title><?php echo $title; ?></title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" href="/dist/style.bundle.css">
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>

    <script src="catalog/view/javascript/jquery/jquery.js"></script>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- add jquery.ui.min if not exist
    <script>window.jQuery.ui || document.write('<script src="catalog/view/javascript/jquery/ui/jquery-ui-1.11.4.min.js"><\/script>');
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'catalog/view/javascript/jquery/ui/themes/base/jquery-ui-1.11.4.min.css') );</script>
    -->
    <!-- add autocomplete if not exist
    <script>jQuery.fn.autocomplete || document.write('<script src="catalog/view/javascript/jquery/ui/jquery-ui-1.11.4.min.js"><\/script>');
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'catalog/view/javascript/jquery/ui/themes/base/jquery-ui-1.11.4.min.css') );</script>
    -->
    <!-- add quick search
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/style_search_quick_opencartsu.css"/>
    <script src="catalog/view/javascript/search_quick_opencartsu.js"></script>
    -->
    <script src="catalog/view/javascript/livesearch.js"></script>
    <!---
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>   --->

    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>

</head>


<!---<body class="<?php echo $class; ?>">-->
<body class="body">
<section class="app_wrap">
    <header class="header" id="header">
        <div class="modal js-header-registration-form">
            <div class="modal_close-bgr js-header-registration-close"></div>
            <div class="modal_sign-in"><h2 class="modal_window_title">Регистрация</h2>
                <button class="modal_cls-btn js-header-registration-close"></button>
                <!---form class="registration_sign-in_form registration_sign-in_form-active" id="registration-form"--->
                <form action="<?php echo $register; ?>" method="post" enctype="multipart/form-data"
                      class="registration_sign-in_form registration_sign-in_form-active" id="registration-form">
                    <input class="registration_sign-in_field js-header-registration-mail" name="email" type="text"
                           placeholder="Эл. почта">
                    <div class="text-error-login js-registration-error"></div>
                    <label class="registration_sign-in_wrap">
                        <input class="registration_sign-in_check" name="newsletter" type="checkbox">
                        <div class="registration_sign-in_for-check js-header-registration-subscribe"></div>
                        <p class="registration_sign-in_description">Подписаться на новости и скидки</p>
                    </label>
                    <input class="registration_sign-in_field js-header-registration-password" name="password"
                           type="password" placeholder="Пароль">
                    <input class="registration_sign-in_field js-header-registration-name" name="firstname" type="text"
                           placeholder="Имя">
                    <label class="registration_sign-in_wrap">
                        <input class="registration_sign-in_check js-header-registration-agree-input" name="agree"
                               type="checkbox">
                        <div class="registration_sign-in_for-check js-header-registration-agree-marker"></div>
                        <p class="registration_sign-in_description">Я согласен с <a href='<?php echo $oferta;?>'
                                                                                    class='registration_sign-in_offerLink'>офертой</a>
                        </p>
                    </label>
                    <button class="registration_sign-in_sign-up-btn js-successfull-registration-btn" type="submit">
                        Зарегистрироваться
                    </button>
                    <div class="ordering_order-product_form_other-sources">
                        <p class="form_other-sources_description">или войти через</p>
                        <a class="form_other-sources_link form_other-sources_link-vk" href="#"></a>
                        <a class="form_other-sources_link form_other-sources_link-fb" href="#"></a>
                    </div>
                    <div class="registration_sign-in_sign-in-btn js-header-entrance-btn">Войти</div>
                </form>
            </div>
        </div>
        <div class="modal js-header-entrance-form">
            <div class="modal_close-bgr js-header-entrance-close"></div>
            <div class="modal_sign-in"><h2 class="modal_window_title">Вход</h2>
                <button class="modal_cls-btn js-header-entrance-close"></button>
                <form class="registration_sign-in_form js-login-form" id="js-login-form" method="post">
                    <input class="registration_sign-in_field js-login-mail" type="text" name="email"
                           placeholder="Эл. почта">
                    <div class="registration_sign-in_form_wrap">
                        <input class="registration_sign-in_field js-login-password" type="password" name="password"
                               placeholder="Пароль">
                        <div class="registration_sign-in_password js-lost-password-btn">Забыли пароль</div>
                    </div>
                    <div class="text-error-login js-login-error"></div>
                    <button class="registration_sign-in_sign-up-btn js-login-enter">Войти</button>
                    <div class="ordering_order-product_form_other-sources">
                        <p class="form_other-sources_description">или войти через</p>
                        <a class="form_other-sources_link form_other-sources_link-vk" href="#"></a>
                        <a class="form_other-sources_link form_other-sources_link-fb" href="#"></a>
                    </div>
                    <div class="registration_sign-in_sign-in-btn js-header-registration-btn">Зарегистрироваться</div>
                </form>
            </div>
        </div>
        <div class="modal js-lost-password-form">
            <div class="modal_close-bgr js-lost-password-close"></div>
            <div class="modal_sign-in">
                <h2 class="modal_window_title">Восстановить пароль</h2>
                <button class="modal_cls-btn js-lost-password-close"></button>
                <form class="registration_sign-in_form" method="POST" id="js-restoring-form">
                    <input class="registration_sign-in_field js-restoring-mail" name="email" type="text"
                           placeholder="Эл. почта">
                    <div class="text-error-lost-password js-lost-password-error"></div>
                    <div class="registration_sign-in_return js-header-entrance-btn">Помню пароль</div>
                    <button class="registration_sign-in_sign-up-btn js-restoring-btn">Выслать новый пароль</button>
                    <div class="ordering_order-product_form_other-sources">
                        <p class="form_other-sources_description">или войти через</p>
                        <a class="form_other-sources_link form_other-sources_link-vk" href="#"></a>
                        <a class="form_other-sources_link form_other-sources_link-fb" href="#"></a>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal js-successfull-registration-form">
            <div class="modal_close-bgr js-successfull-registration-close"></div>
            <div class="modal_notification"><h2 class="modal_window_title">Вы зарегестрированы!</h2>
                <button class="modal_cls-btn js-successfull-registration-close"></button>
                <p class="modal_notification_description">Спасибо за регистрацию! </p>
                <p class="modal_notification_description">На вашу почту придет письмо с подтверждением регистрации,
                    пройдите по ссылке, указанной в письме, чтобы активировать аккаунт.</p>
            </div>
        </div>
        <div class="modal js-successfull-lostpassword-form">
            <div class="modal_close-bgr js-successfull-lostpassword-close"></div>
            <div class="modal_notification"><h2 class="modal_window_title">Пароль восстановлен.</h2>
                <button class="modal_cls-btn js-successfull-lostpassword-close"></button>
                <p class="modal_notification_description">Новый пароль был сгенерирован и выслан на Ваш E-Mail.</p>
            </div>
        </div>
        <div class="show-menu"></div>

        <!---
        <?php echo $currency; ?>
        <?php echo $language; ?> --->

        <a class="homeward_link" href="#">
            <img class="homeward_link_logo" src="catalog/view/theme/default/images/header/nemepik.svg">
        </a>
        <ul class="center-nav center-nav-active">
            <li class="center-nav_unit center-nav_unit-active"><span class="center-nav_unit_link">Женщинам</span>
                <div class="center-nav_sub-menu center-nav_sub-menu-shadow js-menu-swipe-close">
                    <div class="sub-menu_section-right">
                        <ul class="section-right_list">
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link section-right_list_item_link-color" href="#">Распродажа %</a>
                            </li>
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link" href="#">Новинки1</a>
                            </li>
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link" href="#">Необходимое для лета </a>
                            </li>
                        </ul>
                    </div>
                    <div class="sub-menu_section-center">
                        <ul class="section-center_list">

                            <?php foreach ($categories as $category) { ?>

                            <?php if($category['id'] == 88){ ?>

                            <?php foreach ($category['children'] as $category_ch) { ?>

                            <?php $class = ''; if($category_ch['id'] == 66) $class = 'section-center_list_item-margin-btm';?>

                            <?php if ($category_ch['children']) { ?>
                            <li class="section-center_list_item <?php echo $class; ?>">
                                <span class="section-center_list_item_link"><?php echo $category_ch['name']; ?></span>
                                <?php $i=0;?>
                                <?php $count=0;?>

                                <?php if(in_array($category_ch['id'],$category4)) $count=4;?>
                                <?php if(in_array($category_ch['id'],$category12)) $count=12;?>

                                <ul class="sub-list">
                                    <?php foreach ($category_ch['children'] as $child) { ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                    </li>
                                    <?php $i++;?>
                                    <?php if($i == $count) break;?>
                                    <?php } ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $category['href']; ?>"><?php echo $text_all; ?></a>
                                    </li>
                                </ul>

                            </li>
                            <?php } else { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category_ch['name']; ?></a></li>
                            <?php } ?>

                            <?php } ?>
                            <?php } ?>
                            <?php } ?>

                            <li class="section-center_list_item section-center_list_item-margin-lft">
                                <a class="section-center_list_item_link" href="#">Топ бренды</a>
                                <ul class="sub-list">
                                    <?php foreach ($manufacturer as $manufact) { ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $manufact['href']; ?>"><?php echo $manufact['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $href_all_manufact; ?>"><?php echo $text_all_manufact; ?></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="sub-menu_section-left">
                        <div class="section-left_brand">
                            <div class="section-left_brand_photo">
                                <img class="section-left_brand_photo_img" src="catalog/view/theme/default/images/header/photo1.png">
                            </div>
                            <div class="section-left_brand_description">
                                <h2 class="section-left_brand_description_title">MANGO</h2>
                                <p class="section-left_brand_description_content">Линия элегантной классики</p>
                            </div>
                        </div>
                        <div class="section-left_brand">
                            <div class="section-left_brand_photo">
                                <img class="section-left_brand_photo_img" src="catalog/view/theme/default/images/header/photo2.png">
                            </div>
                            <div class="section-left_brand_description">
                                <h2 class="section-left_brand_description_title">CAMBIO</h2>
                                <p class="section-left_brand_description_content">Для тех, кто ценит оригинальный стиль</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="center-nav_unit"><span class="center-nav_unit_link">Мужчинам</span>
                <div class="center-nav_sub-menu center-nav_sub-menu-shadow">
                    <div class="sub-menu_section-right">
                        <ul class="section-right_list">
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link section-right_list_item_link-color" href="#">Распродажа %</a>
                            </li>
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link" href="#">Новинки2</a>
                            </li>
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link" href="#">Необходимое для лета </a>
                            </li>
                        </ul>
                    </div>
                    <div class="sub-menu_section-center">
                        <ul class="section-center_list">
                            <?php foreach ($categories as $category) { ?>

                            <?php if($category['id'] == 89){ ?>

                            <?php foreach ($category['children'] as $category_ch) { ?>


                            <?php $class = '';
                                    if($category_ch['id'] == 92) $class = 'section-center_list_item-margin-btm';?>

                            <?php if ($category_ch['children']) { ?>
                            <li class="section-center_list_item <?php echo $class; ?>">
                                <span class="section-center_list_item_link"><?php echo $category_ch['name']; ?></span>

                                <ul class="sub-list">
                                    <?php foreach ($category_ch['children'] as $child) { ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $category['href']; ?>"><?php echo $text_all; ?></a>
                                    </li>
                                </ul>

                            </li>
                            <?php } else { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category_ch['name']; ?></a></li>
                            <?php } ?>

                            <?php } ?>
                            <?php } ?>
                            <?php } ?>

                            <li class="section-center_list_item section-center_list_item-margin-lft">
                                <a class="section-center_list_item_link" href="#">Топ бренды</a>
                                <ul class="sub-list">
                                    <?php foreach ($manufacturer as $manufact) { ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $manufact['href']; ?>"><?php echo $manufact['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $href_all_manufact; ?>"><?php echo $text_all_manufact; ?></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="sub-menu_section-left">
                        <div class="section-left_brand">
                            <div class="section-left_brand_photo">
                                <img class="section-left_brand_photo_img" src="catalog/view/theme/default/images/header/photo1.png">
                            </div>
                            <div class="section-left_brand_description">
                                <h2 class="section-left_brand_description_title">MANGO</h2>
                                <p class="section-left_brand_description_content">Линия элегантной классики</p>
                            </div>
                        </div>
                        <div class="section-left_brand">
                            <div class="section-left_brand_photo">
                                <img class="section-left_brand_photo_img" src="catalog/view/theme/default/images/header/photo2.png">
                            </div>
                            <div class="section-left_brand_description">
                                <h2 class="section-left_brand_description_title">CAMBIO</h2>
                                <p class="section-left_brand_description_content">Для тех, кто ценит оригинальный стиль</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="center-nav_unit"><a class="center-nav_unit_link">Детям</a>
                <div class="center-nav_sub-menu center-nav_sub-menu-shadow">
                    <div class="sub-menu_section-right">
                        <ul class="section-right_list">
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link section-right_list_item_link-color" href="#">Распродажа %</a>
                            </li>
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link" href="#">Новинки3</a>
                            </li>
                            <li class="section-right_list_item">
                                <a class="section-right_list_item_link" href="#">Необходимое для лета </a>
                            </li>
                        </ul>
                    </div>
                    <div class="sub-menu_section-center">
                        <ul class="section-center_list">
                            <?php foreach ($categories as $category) { ?>

                            <?php if($category['id'] == 90){ ?>

                            <?php foreach ($category['children'] as $category_ch) { ?>


                            <?php $class = '';
                                    if($category_ch['id'] == 95) $class = 'section-center_list_item-margin-btm';?>

                            <?php if ($category_ch['children']) { ?>
                            <li class="section-center_list_item <?php echo $class; ?>">
                                <span class="section-center_list_item_link"><?php echo $category_ch['name']; ?></span>

                                <ul class="sub-list">
                                    <?php foreach ($category_ch['children'] as $child) { ?>
                                    <li class="sub-list_sub-item"><a class="sub-list_sub-item_sub-link"
                                                                     href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                    <li class="sub-list_sub-item"><a class="sub-list_sub-item_sub-link"
                                                                     href="<?php echo $category['href']; ?>"><?php echo $text_all; ?></a>
                                    </li>
                                </ul>

                            </li>
                            <?php } else { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category_ch['name']; ?></a></li>
                            <?php } ?>

                            <?php } ?>
                            <?php } ?>
                            <?php } ?>

                            <li class="section-center_list_item section-center_list_item-margin-lft">
                                <a class="section-center_list_item_link" href="#">Топ бренды</a>
                                <ul class="sub-list">
                                    <?php foreach ($manufacturer as $manufact) { ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $manufact['href']; ?>"><?php echo $manufact['name']; ?></a>
                                    </li>
                                    <?php } ?>
                                    <li class="sub-list_sub-item">
                                        <a class="sub-list_sub-item_sub-link" href="<?php echo $href_all_manufact; ?>"><?php echo $text_all_manufact; ?></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="sub-menu_section-left">
                        <div class="section-left_brand">
                            <div class="section-left_brand_photo">
                                <img class="section-left_brand_photo_img" src="catalog/view/theme/default/images/header/photo1.png">
                            </div>
                            <div class="section-left_brand_description">
                                <h2 class="section-left_brand_description_title">MANGO</h2>
                                <p class="section-left_brand_description_content">Линия элегантной классики</p>
                            </div>
                        </div>
                        <div class="section-left_brand">
                            <div class="section-left_brand_photo">
                                <img class="section-left_brand_photo_img" src="catalog/view/theme/default/images/header/photo2.png">
                            </div>
                            <div class="section-left_brand_description">
                                <h2 class="section-left_brand_description_title">CAMBIO</h2>
                                <p class="section-left_brand_description_content">Для тех, кто ценит оригинальный стиль</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="center-nav_unit-without-sub-menu">
                <a class="center-nav_unit_link center-nav_unit_link-padding" href="<?php echo $nemepichome;?>">Nemepik
                    home
                    <img class="center-nav_unit_link_icon" src="catalog/view/theme/default/images/header/home-icon.svg">
                </a>
            </li>
        </ul>
        <ul class="right-nav">

            <li class="right-nav_unit right-nav_unit-search">
                <a class="right-nav_unit_link right-nav_unit_link-search">
                    <img class="right-nav_unit_link_icon" src="/dist/assets/images/header/icon-search.svg">
                </a>
            </li>

            <li class="right-nav_unit">
                <a class="right-nav_unit_link" href="<?php echo $wishlist; ?>">
                    <img class="right-nav_unit_link_icon" src="catalog/view/theme/default/images/header/icon-like.svg">
                </a>
            </li>

            <li class="right-nav_unit right-nav_unit-basket">
                <a class="right-nav_unit_link" href="<?php echo $shopping_cart; ?>">
                    <img class="right-nav_unit_link_icon"
                         src="catalog/view/theme/default/images/header/icon-basket.svg">
                </a>
                <div class="right-nav_unit_number"><?php echo $countCart;?></div>
            </li>
            <?php if(!$logged) { ?>
            <li class="right-nav_unit right-nav_unit-sign-in js-header-entrance-btn">
                <span class="right-nav_unit_link"> Войти</span>
            </li>
            <?php } else { ?>
            <li class="right-nav_unit right-nav_unit-account js-header-myaccount-btn">
                <span class="right-nav_unit_link">Мой аккаунт</span>
                <div class="right-nav_unit_my-account js-header-myaccount-menu">
                    <ul class="aside_menu">
                        <li class="aside_menu_item">
                            <a class="aside_menu_item_link aside_menu_item_link-active" href="<?php echo $order; ?>">Мои
                                заказы</a>
                        </li>
                        <li class="aside_menu_item">
                            <a class="aside_menu_item_link" href="<?php echo $account; ?>">Мои данные</a>
                        </li>
                        <li class="aside_menu_item">
                            <a class="aside_menu_item_link" href="<?php echo $voucher; ?>">Мои купоны</a>
                        </li>
                        <li class="aside_menu_item">
                            <a class="aside_menu_item_link" href="<?php echo $wishlist; ?>">Избранное</a>
                        </li>
                        <li class="aside_menu_item aside_menu_item-sign-out">
                            <a class="aside_menu_item_link" href="<?php echo $logout; ?>">Выход
                                <img class="aside_menu_item_link_icon"
                                     src="catalog/view/theme/default/images/header/sign-in.svg"></a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php } ?>
        </ul>
    </header>
    <?php echo $search; ?>