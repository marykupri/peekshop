<?php echo $header; ?>


<section class="home_page">
    <div class="modal js-home-subscribe-modal">
        <div class="modal_close-bgr js-home-subscribe-close"></div>
        <div class="subscribe-modal_window"><h2 class="modal_window_title">Спасибо</h2>
            <button class="modal_cls-btn js-home-subscribe-close"></button>
            <p class="subscribe-modal_window_description">Вам на эл. почту отправлено письмо со ссылкой для
                подтверждения подписки. Eсли письмо не пришло, поищите его в папке со спамом или повторите
                подписку</p></div>
    </div>

    <?php echo $content_top; ?>

    <div class="news-subscribe">
        <div class="news-subscribe_wrapper"><img class="news-subscribe_wrapper_background"
                                                 src="catalog/view/theme/default/images/home/subscribe-photo.png"><img
                    class="news-subscribe_wrapper_background-small" src="catalog/view/theme/default/images/home/subscribe-photo-small.png">
            <div class="news-subscribe_wrapper_content"><p class="news-subscribe_wrapper_content_description">СКИДКА
                    10%</p>
                <p class="news-subscribe_wrapper_content_description news-subscribe_wrapper_content_description-other-font">
                    За подписку на наши новости</p><input
                        class="news-subscribe_wrapper_content_field js-home-subscribe-input" type="text"
                        placeholder="Ваша эл.почта">
                <button class="news-subscribe_wrapper_content_subscribe-btn js-home-subscribe-btn">Подписаться
                </button>
            </div>
        </div>
    </div>
    <div class="offers"><a class="offers_link-wrap" href="#"><img class="offers_link-wrap_icon"
                                                                  src="catalog/view/theme/default/images/home/brands-icon.svg">
            <h2 class="offers_link-wrap_title">ЭКСКЛЮЗИВНЫЕ БРЕНДЫ</h2>
            <p class="offers_link-wrap_offer">В Москве и 60 других крупных городах России вы получите свой заказ уже на
                следующий день!</p></a><a class="offers_link-wrap" href="#"><img class="offers_link-wrap_icon"
                                                                                 src="catalog/view/theme/default/images/home/delivery-icon.svg">
            <h2 class="offers_link-wrap_title">БEСПЛАТНАЯ СТАНДАРТНАЯ ДОСТАВКА</h2>
            <p class="offers_link-wrap_offer">В Москве и 60 других крупных городах России вы получите свой заказ уже на
                следующий день!</p></a><a class="offers_link-wrap" href="#"> <img class="offers_link-wrap_icon"
                                                                                  src="catalog/view/theme/default/images/home/offers-icon.svg">
            <h2 class="offers_link-wrap_title">ЕЖЕДНЕВНЫЕ СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ</h2>
            <p class="offers_link-wrap_offer">Вы можете оплатить покупки любыми банковскими картами, а также с помощью
                Paypal</p></a>
    </div>


    <!----div class="about"><h2 class="about_heading">Компания Nemepik - это качественная брендовая одежда по лучшей
            цене.</h2>
        <div class="about_envelope"><h2 class="about_envelope_title">Выбор одежды</h2>
            <p class="about_envelope_description">В интернет-магазине одежды Elegant.ru каждый может выбрать и
                купить одежду или аксессуары в пару кликов, у нас вы найдете постоянно обновляемый ассортимент,
                доступные цены и заманчивые предложения. В каталоге интернет-магазина elegant.ru более 70 брендов
                мужской и женской одежды и аксессуаров разных ценовых категорий, всего более 3 000 товаров. </p>
            <h2 class="about_envelope_title">Обмен или возврат</h2>
            <p class="about_envelope_description">Вы можете вернуть или поменять любой товар абсолютно
                бесплатно.</p></div>
        <div class="about_envelope"><h2 class="about_envelope_title">Помощь с размером</h2>
            <p class="about_envelope_description">В таблице размеров можно без труда определить свой размер. Тем не
                менее, не бойтесь ошибиться с размером и цветом — одежду можно бесплатно примерить у себя дома и
                вернуть то, что вам не подошло. Оплачивайте только то, что вам понравилось.</p>
            <h2 class="about_envelope_title">Доставка и оплата</h2>
            <p class="about_envelope_description">Наша служба доставки работает во всех крупнейших городах России.
                Срок доставки от одного до четырех дней. Вы можете оплатить покупки не только наличными, но и
                банковскими картами. У всех курьеров при себе имеется терминал для оплаты картами.</p></div>
    </div----->


</section>

<script>
    var pageName = 'HomePage';
</script>

<?php echo $footer; ?>