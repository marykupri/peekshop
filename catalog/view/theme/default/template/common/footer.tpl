<footer class="footer">
  <div class="content-wrapper">
    <div class="info"><h2 class="info_title">Контакты</h2>
      <ul class="info_list">
        <li class="info_list_item"><a class="info_list_item_link" href="#"><?php echo $address; ?></a></li>
        <li class="info_list_item"><a class="info_list_item_link" href="#"><?php echo $telephone; ?></a></li>
        <li class="info_list_item"><a class="info_list_item_link" href="#"><?php echo $email; ?></a></li>
      </ul>
    </div>
    <div class="info"><h2 class="info_title"><?php echo $text_service; ?></h2>
      <ul class="info_list">
        <?php foreach ($informations as $information) { ?>
        <li class="info_list_item"><a class="info_list_item_link" href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
        <?php } ?>
        <!--<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>-->
      </ul>
    </div>
    <div class="info"><h2 class="info_title">Мы в соц. сетях</h2>
      <ul class="info_list">
        <li class="info_list_item"><a class="info_list_item_link" href="#">Instagram</a></li>
        <li class="info_list_item"><a class="info_list_item_link" href="#">Вконтакте</a></li>
        <li class="info_list_item"><a class="info_list_item_link" href="#">Facebook</a></li>
      </ul>
    </div>
    <div class="info"><h2 class="info_title">Способы оплаты</h2>
      <div class="info_pay-methods">
        <a class="info_pay-methods_link" href="#">
          <img class="info_pay-methods_link_icon" src="catalog/view/theme/default/images/footer/visa.png">
        </a>
        <a class="info_pay-methods_link" href="#">
          <img class="info_pay-methods_link_icon" src="catalog/view/theme/default/images/footer/mastercard.png">
        </a>
        <a class="info_pay-methods_link" href="#">
          <img class="info_pay-methods_link_icon" src="catalog/view/theme/default/images/footer/paypal.png">
        </a>
      </div>
    </div>
  </div>
  <div class="content-wrapper">
    <div class="about-company"><a class="about-company_link" href="<?php echo $contact; ?>">О компании</a><a
              class="about-company_link" href="<?php echo $manufacturer; ?>">Все бренды</a></div>
    <div class="developer developer_link">&copy; 2017 <a href="http://topfloat.ru/"><span class="developer_link_color">Topfloat</span></a> & <a href="http://monodigital.ru/"><span class="developer_link_color">Mono</span></a>
    </div>
  </div>
</footer>
</section>
<!--<script src="/assets/jquery.js"></script>--->
<script src="/dist/script.bundle.js"></script>
<script src="/dist/assets/jquery.slimscroll.js"></script>
</body>
</html>