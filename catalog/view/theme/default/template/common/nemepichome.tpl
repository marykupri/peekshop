<?php echo $header; ?>

<section class="home-subscribe">
    <div class="modal js-home-subscribe-modal">
        <div class="modal_close-bgr js-home-subscribe-close"></div>
        <div class="subscribe-modal_window"><h2 class="modal_window_title">Спасибо</h2>
            <button class="modal_cls-btn js-home-subscribe-close"></button>
            <p class="subscribe-modal_window_description">Вам на эл. почту отправлено письмо со ссылкой для
                подтверждения подписки. Eсли письмо не пришло, поищите его в папке со спамом или повторите
                подписку</p></div>
    </div>
    <img class="home-subscribe_background" src="/dist/assets/images/home-subscribe/home-subscribe-bgr.jpg">
    <div class="home-subscribe_for-subscribe"><h2 class="home-subscribe_for-subscribe_title">ВСЕ ДЛЯ УЮТА</h2>
        <p class="home-subscribe_for-subscribe_description">ПОДПИШИТЕСЬ, ЧТОБЫ НЕ ПРОПУСТИТЬ ЗАПУСК</p>
        <form class="home-subscribe_for-subscribe_wrap">
            <input class="home-subscribe_for-subscribe_field js-home-subscribe-input" type="text" placeholder="Ваша эл. почта">
            <button type="button" class="home-subscribe_for-subscribe_btn js-home-subscribe-btn">Подписаться</button>
        </form>
    </div>
</section>
<script>var pageName = 'HomeSubscribePage';</script>

<?php echo $footer; ?>