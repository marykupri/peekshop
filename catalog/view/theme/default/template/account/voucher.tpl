<?php echo $header; ?>

<section class="app_content">
    <section class="account_page">
        <div class="catalog_breadcrumbs">
            <ul class="breadcrumbs_list">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link"
                                                     href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <div class="my-account"><h2 class="my-account_title"><?php echo $heading_title; ?></h2>
            <div class="my-account_aside">
                <?php echo $column_left; ?>
            </div>
            <div class="my-account_main">
                <div class="account-coupon account-coupon-active">
                    <ul class="account_menu_list account_menu_list-account-coupon">
                        <li class="account_menu_list_item js-coupon-list-btn" open="active">Активные</li>
                        <li class="account_menu_list_item js-coupon-list-btn" open="archive">Использованые</li>
                    </ul>

                    <div class="account-coupon_coupons js-coupon-list-window" list="active">
                        <?php if(!empty($listVouchers['active'])) { ?>
                        <?php foreach($listVouchers['active'] as $coupon) { ?>
                        <div class="account-coupon_coupons_unit">
                            <div class="account-coupon_coupons_unit_rightSide">
                                <p class="account-coupon_coupons_number">Купон №<?php echo $coupon['voucher_id']; ?></p>
                                <p class="account-coupon_coupons_date">от <?php echo $coupon['date_added']; ?></p>
                            </div>
                            <div class="account-coupon_coupons_unit_leftSide">
                                <p class="account-coupon_coupons_amount"><?php echo $coupon['amount']; ?></p>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>

                    <div class="account-coupon_coupons js-coupon-list-window" list="archive">
                        <?php if(!empty($listVouchers['archive'])) { ?>
                        <?php foreach($listVouchers['archive'] as $coupon) { ?>
                        <div class="account-coupon_coupons_unit">
                            <div class="account-coupon_coupons_unit_rightSide">
                                <p class="account-coupon_coupons_number">Купон №<?php echo $coupon['voucher_id']; ?></p>
                                <p class="account-coupon_coupons_date">от <?php echo $coupon['date_added']; ?></p>
                            </div>
                            <div class="account-coupon_coupons_unit_leftSide">
                                <p class="account-coupon_coupons_amount"><?php echo $coupon['amount']; ?></p>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="buy-coupon"><h2 class="buy-coupon_title">Купить купон</h2>
                    <p class="buy-coupon_description">Купон покупается через сайт. Покрывает до 50% цены. Номинал
                        купона от 1000 р до 60 000 р. Заложить возможность покупки подарочного купона (на сайте),
                        систему учета расходов в личном кабинете после покупки, систему учета на серверной
                        части.</p>
                    <p class="buy-coupon_description">Он отправляется почтой на указанный Вами адрес или в
                        магазин.Действительна во всех магазинах в России и на сайте для этой страны.</p>
                    <p class="buy-coupon_description">Подарочная карта действительна на протяжении трех лет с даты
                        покупки.После покупки подарочная карта не подлежит возврату.</p>

                    <form class="buy-coupon_form" action="<?php echo $action; ?>" method="post"
                          enctype="multipart/form-data">
                        <div class="buy-coupon_title">Выберите сумму</div>
                        <div class="buy-coupon_on-amount">
                            <p class="buy-coupon_on-amount_amount js-coupon-selector-display">1000 ₽</p>
                            <div class="buy-coupon_on-amount_range">
                                <input class="js-coupon-selector-input" name="amount" type="number" hidden>
                                <div class="buy-coupon_on-amount_range_btn buy-coupon_on-amount_range_btn-minus js-coupon-selector-minus"></div>
                                <div class="buy-coupon_on-amount_range_slider js-coupon-selector-slide">
                                    <div class="buy-coupon_on-amount_range_slider_runner js-coupon-selector-btn"></div>
                                </div>
                                <div class="buy-coupon_on-amount_range_btn buy-coupon_on-amount_range_btn-plus js-coupon-selector-plus"></div>
                            </div>
                        </div>
                        <!---div class="buy-coupon_pay-methods"><label class="buy-coupon_pay-methods_wrap"><input
                                    class="buy-coupon_pay-methods_check-method" type="radio" value="1" name="type">
                            <div class="buy-coupon_pay-methods_for-check-method"></div>
                            <p class="buy-coupon_pay-methods_name">PayPal</p></label><label
                                  class="buy-coupon_pay-methods_wrap"><input
                                    class="buy-coupon_pay-methods_check-method" type="radio" value="2" name="type">
                            <div class="buy-coupon_pay-methods_for-check-method"></div>
                            <p class="buy-coupon_pay-methods_name">Visa / Mastercard / Мир</p></label><label
                                  class="buy-coupon_pay-methods_wrap"><input
                                    class="buy-coupon_pay-methods_check-method" type="radio" value="3" name="type">
                            <div class="buy-coupon_pay-methods_for-check-method"></div>
                            <p class="buy-coupon_pay-methods_name">Webmoney</p></label></div---->
                        <button type="submit" class="buy-coupon_pay-btn">Купить</button>
                    </form>

                </div>
            </div>
        </div>
    </section>
</section>
<script>var pageName = 'AccountCouponPage';</script>

<?php echo $footer; ?>