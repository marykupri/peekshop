<div class="product-description-wrap">
    <h2 class="product-description-aside_name"><?php echo $manufacturer; ?></h2>
    <p class="product-description-aside_specification"><?php echo $name; ?></p>
    <p class="product-description-aside_price"><?php echo $price; ?></p>
</div>
<div class="product-description-aside">
    <h2 class="product-description-aside_name"><?php echo $manufacturer; ?></h2>
    <p class="product-description-aside_specification"><?php echo $name; ?></p>
    <p class="product-description-aside_price"><?php echo $price; ?></p>

    <ul class="aside_color-list">
        <?php foreach ($color as $val) { ?>
        <?php $background = '' ; ?>
        <?php if(!empty($val['code'])) $background = 'style="background: ' . $val['code'] . '"'; ?>

        <li class="aside_color-list_item">
            <label class="aside_color-list_item_wrap">
                <!--input class="aside_color-list_item_check" type="radio" name="color"--->
                <a href="<?php echo $val['href']; ?>" title="<?php echo $val['color']; ?>"><span
                            class="aside_color-list_item_for-check" <?php echo $background; ?>></span></a>
            </label>
        </li>

        <?php } ?>
    </ul>

    <?php if ($options) { ?>
    <?php foreach ($options as $option) { ?>
    <?php if ($option['type'] == 'select' && $option['option_id'] == 11) { ?>
    <div class="aside_size-select">
        <div class="aside_size-select_for-click js-product-view-size-select-clicked"></div>
        <input class="aside_size-select_direction js-order-size-select" type="text" readonly
               placeholder="Выберите размер">
        <div class="size-select_dropdown js-order-size-select-list">
            <ul class="size-select_dropdown_list" data-name="<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <li class="size-select_dropdown_list_item size-select_dropdown_list_item_notAvailable"
                    data-value="<?php echo $option_value['product_option_value_id']; ?>">
                    <span class="size-select_dropdown_list_link js-order-size-select-item"><?php echo $option_value['name']; ?></span>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    <?php } ?>
    <?php } ?>

    <div class="product-description_links">
        <a class="product-description_links_content" href="#">Подобрать размер</a>
        <a class="product-description_links_content" href="#">Таблица размеров</a>
    </div>
    <div class="product-description_in-basket">
        <button class="product-description_in-basket_add js-order-to-busket"
                onclick="cart.add('<?php echo $product_id; ?>', '<?php echo $minimum; ?>');">В корзину
        </button>
        <?php if(in_array($product_id, $wishlist_product)) { ?>
        <button type="button" class="product-description_in-basket_liked product-description_in-basket_liked-active"
                onclick="wishlist.remove('<?php echo $product_id; ?>');"></button>
        <?php } else { ?>
        <button type="button" class="product-description_in-basket_liked"
                onclick="wishlist.add('<?php echo $product_id; ?>');"></button>
        <?php } ?>
    </div>
</div>

<?php if ($thumb || $images) { ?>
<div class="product-description_gallery js-product-view-slider">
    <div class="product-description_gallery_for-active-photo"><img
                class="product-description_gallery_photo js-product-view-slider-full-img"
                src="<?php echo $images[0]['popup']; ?>"></div>
    <ul class="product-description_gallery_list">

        <?php if ($images) { ?>
        <?php foreach ($images as $image) { ?>
        <li class="product-description_gallery_list_item product-description_gallery_list_item-active">
            <img class="product-description_gallery_photo js-product-view-slider-item"
                 src="<?php echo $image['popup']; ?>"></li>
        <?php } ?>
        <?php } ?>
    </ul>
</div>

<?php } else { ?>
<li class="product-description_gallery_list_item"><img
            class="product-description_gallery_photo js-product-slider-item"
            src="<?php echo $thumb; ?>"></li>
<?php } ?>

<div class="product-description_see-more">
    <a href="<?php echo $href;?>">
        <button class="product-description_see-more_btn">Полное описание</button>
    </a>
</div>