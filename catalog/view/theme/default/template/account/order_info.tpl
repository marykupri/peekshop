<?php echo $header; ?>

<section class="app_content">
  <section class="account_page">
    <div class="modal js-subscribe-product-receipts">
      <div class="modal_close-bgr js-subscribe-product-receipts-close"></div>
      <form class="modal_out-of-stock js-subscribe-product-receipts-form"><h2 class="modal_window_title">
          Узнать о поступлении товара</h2>
        <button class="modal_cls-btn js-subscribe-product-receipts-close"></button>
        <p class="modal_out-of-stock_description">К сожалению, данного размера сейчас нет в наличии. Мы вас
          уведомим, как только он появиться.</p>
        <ul class="dropdown_selection_list">
          <li class="dropdown_selection_list_item"><label class="dropdown_selection_input-wrap"><input
                      class="dropdown_selection_checked" type="checkbox">
              <div class="dropdown_selection_for-checked"></div>
              <p class="dropdown_selection_description">44</p></label></li>
          <li class="dropdown_selection_list_item"><label class="dropdown_selection_input-wrap"><input
                      class="dropdown_selection_checked" type="checkbox">
              <div class="dropdown_selection_for-checked"></div>
              <p class="dropdown_selection_description">44</p></label></li>
          <li class="dropdown_selection_list_item"><label class="dropdown_selection_input-wrap"><input
                      class="dropdown_selection_checked" type="checkbox">
              <div class="dropdown_selection_for-checked"></div>
              <p class="dropdown_selection_description">44</p></label></li>
          <li class="dropdown_selection_list_item"><label class="dropdown_selection_input-wrap"><input
                      class="dropdown_selection_checked" type="checkbox">
              <div class="dropdown_selection_for-checked"></div>
              <p class="dropdown_selection_description">44</p></label></li>
        </ul>
        <div class="home-subscribe_for-subscribe_wrap"><input
                  class="home-subscribe_for-subscribe_field js-subscribe-product-receipts-mail" type="text"
                  placeholder="Ваша эл. почта">
          <button class="home-subscribe_for-subscribe_btn js-subscribe-product-receipts-send"
                  type="submit">Подписаться
          </button>
        </div>
      </form>
    </div>
    <div class="modal modal-quick-view js-product-quick-view">
      <div class="modal_close-bgr js-product-quick-view-close"></div>
      <div class="quick-view">
        <button class="modal_cls-btn js-product-quick-view-close"></button>
        <div class="product-description">
          <div class="product-description-wrap"><h2 class="product-description-aside_name">Christian Berg
              Woman Selection</h2>
            <p class="product-description-aside_specification">Платья с художественными узорами - темно
              - синий</p>
            <p class="product-description-aside_price">14 400 ₽</p></div>
          <div class="product-description-aside"><h2 class="product-description-aside_name">Christian Berg
              Woman Selection</h2>
            <p class="product-description-aside_specification">Платья с художественными узорами - темно
              - синий</p>
            <p class="product-description-aside_price">14 400 ₽</p>
            <ul class="aside_color-list">
              <li class="aside_color-list_item"><label class="aside_color-list_item_wrap"><input
                          class="aside_color-list_item_check" type="radio" name="color">
                  <div class="aside_color-list_item_for-check"></div>
                </label></li>
              <li class="aside_color-list_item"><label class="aside_color-list_item_wrap"><input
                          class="aside_color-list_item_check" type="radio" name="color">
                  <div class="aside_color-list_item_for-check"></div>
                </label></li>
              <li class="aside_color-list_item"><label class="aside_color-list_item_wrap"><input
                          class="aside_color-list_item_check" type="radio" name="color">
                  <div class="aside_color-list_item_for-check"></div>
                </label></li>
              <li class="aside_color-list_item"><label class="aside_color-list_item_wrap"><input
                          class="aside_color-list_item_check" type="radio" name="color">
                  <div class="aside_color-list_item_for-check"></div>
                </label></li>
            </ul>
            <div class="aside_size-select"><input
                      class="aside_size-select_direction js-order-size-select" type="text"
                      placeholder="Выберите размер" readonly>
              <div class="size-select_dropdown js-order-size-select-list">
                <ul class="size-select_dropdown_list">
                  <li class="size-select_dropdown_list_item size-select_dropdown_list_item_notAvailable">
                    <span class="size-select_dropdown_list_link js-order-size-select-item">42</span>
                  </li>
                  <li class="size-select_dropdown_list_item size-select_dropdown_list_item_not-available js-subscribe-product-receipts-open">
                                            <span class="size-select_dropdown_list_link">44<span
                                                      class="size-select_link_not-available">Узнать о поступлении</span></span>
                  </li>
                  <li class="size-select_dropdown_list_item"><span
                            class="size-select_dropdown_list_link js-order-size-select-item">46</span>
                  </li>
                  <li class="size-select_dropdown_list_item"><span
                            class="size-select_dropdown_list_link js-order-size-select-item">48</span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="product-description_links"><a class="product-description_links_content"
                                                      href="#">Подобрать размер</a><a
                      class="product-description_links_content" href="#">Таблица размеров</a></div>
            <div class="product-description_in-basket">
              <button class="product-description_in-basket_add">В корзину</button>
              <button class="product-description_in-basket_liked product-description_in-basket_liked-active"></button>
            </div>
          </div>
          <div class="product-description_gallery js-product-view-slider">
            <div class="product-description_gallery_forActivePhoto"><img
                      class="product-description_gallery_photo js-product-view-slider-full-img"
                      src="/assets/images/product-view/product-photo1.png"></div>
            <ul class="product-description_gallery_list">
              <li class="product-description_gallery_list_item"><img
                        class="product-description_gallery_photo js-product-view-slider-item"
                        src="/assets/images/product-view/product-photo1.png"></li>
              <li class="product-description_gallery_list_item"><img
                        class="product-description_gallery_photo js-product-view-slider-item"
                        src="/assets/images/product-view/product-photo2.png"></li>
              <li class="product-description_gallery_list_item"><img
                        class="product-description_gallery_photo js-product-view-slider-item"
                        src="/assets/images/product-view/product-photo3.png"></li>
              <li class="product-description_gallery_list_item"><img
                        class="product-description_gallery_photo js-product-view-slider-item"
                        src="/assets/images/product-view/product-photo4.png"></li>
            </ul>
          </div>
          <div class="product-description_see-more">
            <button class="product-description_see-more_btn">Полное описание</button>
          </div>
        </div>
      </div>
    </div>
    <div class="catalog_breadcrumbs">
      <ul class="breadcrumbs_list">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="my-account"><h2 class="my-account_title"><?php echo $heading_title; ?></h2>
      <div class="my-account_aside">
        <?php echo $column_left; ?>
      </div>
      <div class="my-account_main">
        <div class="account_order"><h2 class="account_order_title">Заказ №<?php echo $order_id; ?> от <?php echo $date_added; ?></h2>
          <div class="account_order_parameters">
            <div class="account_order_parameters_contents">
              <p class="account_order_parameters_description">Статус</p>
            </div>
            <div class="account_order_parameters_status">
              <p class="account_order_parameters_date">
                Отправлен <span class='account_order_parameters_date-color'><?php echo $date_added_text; ?></span>
              </p>
            </div>
          </div>
          <div class="account_order_parameters">
            <div class="account_order_parameters_contents">
              <p class="account_order_parameters_description">Состав заказа</p>
            </div>
            <div class="account_order_parameters_goods">
            <?php foreach ($products as $product) { ?>
              <p class="account_order_parameters_name js-product-quick-view-open" data-id="<?php echo $product['product_id']; ?>">
                <span class="account_order_parameters_name_link"><?php echo $product['name']; ?></span>
              </p>
            <?php } ?>

              <?php foreach ($vouchers as $voucher) { ?>
              <p class="account_order_parameters_name">
                <span class="account_order_parameters_name_link"><?php echo $voucher['description']; ?></span>
              </p>
              <?php } ?>
            </div>

            <div class="account_order_parameters_amount">
              <?php foreach ($products as $product) { ?>
              <p class="account_order_parameters_unit"><?php echo $product['quantity']; ?> шт.</p>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <p class="account_order_parameters_unit"></p>
              <?php } ?>
            </div>

            <div class="account_order_parameters_price">
              <?php foreach ($products as $product) { ?>
              <p class="account_order_parameters_value"><?php echo $product['total']; ?></p>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <p class="account_order_parameters_unit"><?php echo $voucher['amount']; ?></p>
              <?php } ?>
            </div>

          </div>
          <div class="account_order_parameters">
            <div class="account_order_parameters_contents">
              <p class="account_order_parameters_description">Общая стоимость</p>
            </div>
            <div class="account_order_parameters_totals">

              <p class="account_order_parameters_content">Сумма за товары</p>
              <p class="account_order_parameters_content">Стоимость доставки</p>
              <p class="account_order_parameters_content">Скидка</p>
              <p class="account_order_parameters_content">Итого</p>

            </div>
            <div class="account_order_parameters_sum">

              <?php if(isset($sub_total)) { ?>
              <p class="account_order_parameters_value"><?php echo $sub_total; ?></p>
              <?php } else { ?>
              <p class="account_order_parameters_value">0 ₽</p>
              <?php } ?>
              <p class="account_order_parameters_value"><?php echo $shipping_price; ?></p>
              <p class="account_order_parameters_value"><?php echo $total_discount; ?></p>
              <?php if(isset($total)) { ?>
              <p class="account_order_parameters_value"><?php echo $total; ?></p>
              <?php } else { ?>
              <p class="account_order_parameters_value">0 ₽</p>
              <?php } ?>
            </div>
          </div>
          <div class="account_order_parameters">
            <div class="account_order_parameters_contents"><p
                      class="account_order_parameters_description">Способ оплаты</p></div>
            <div class="account_order_parameters_pay"><p class="account_order_parameters_pay-method">
                <?php if ($payment_method) { ?>
                <?php echo $payment_method; ?>
                <?php } ?></p></div>
          </div>
          <div class="account_order_parameters">
            <div class="account_order_parameters_contents"><p
                      class="account_order_parameters_description">Доставка</p></div>
            <div class="account_order_parameters_recipient-pointers"><p
                      class="account_order_parameters_content">Адрес</p>
              <p class="account_order_parameters_content">Получатель</p>
              <p class="account_order_parameters_content">Телефон</p></div>
            <div class="account_order_parameters_recipient">
              <p class="account_order_parameters_info account_order_parameters_info-address">
                <?php if ($shipping_address) { ?>
                <?php echo $shipping_address; ?>
                <?php } ?>
              </p>
              <p class="account_order_parameters_info"><?php echo $shipping_firstname;?></p>
              <p class="account_order_parameters_info"><?php echo $telephone; ?></p></div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<script>var pageName = 'AccountOrderPage';</script>


<?php echo $footer; ?>