<?php echo $header; ?>

<section class="app_content">
    <section class="account_page">
        <div class="catalog_breadcrumbs">
            <ul class="breadcrumbs_list">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs_list_item">
                    <a class="breadcrumbs_list_item_link" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <div class="my-account"><h2 class="my-account_title"><?php echo $heading_title; ?></h2>
            <div class="my-account_aside">
                <?php echo $column_left; ?>
            </div>
            <div class="my-account_main">
                <div class="my-account_orders">
                    <ul class="account_menu_list">
                        <li class="account_menu_list_item js-account-list-select" open="active">Активные</li>
                        <li class="account_menu_list_item js-account-list-select" open="archive">В архиве</li>
                    </ul>
                    <div class="my-account_orders_goods js-account-list-view" list="active">
                        <?php if ($orders) { ?>
                        <?php foreach ($orders as $order) { ?>

                        <div class="my-account_orders_goods_order my-account_orders_goods_order-active">
                            <a class="my-account_orders_goods_order_link" href="<?php echo $order['href']; ?>"></a>
                            <div class="goods_order_identifier">
                                <p class="goods_order_identifier_order-number">Заказ №<span
                                            class='goods_order_identifier_order-number_number'><?php echo $order['order_id']; ?></span>
                                </p>
                                <p class="goods_order_identifier_time">от <span
                                            class='goods_product_identifier_time_date'><?php echo $order['date_added']; ?></span>
                                </p>
                            </div>
                            <div class="goods_order_pointers">
                                <p class="goods_order_pointers_content">Товары в заказе</p>
                                <p class="goods_order_pointers_content">Общая стоимость</p>
                                <p class="goods_order_pointers_content">Статус</p>
                            </div>
                            <div class="goods_order_description">
                                <p class="goods_order_description_name"><?php echo $order['first_item']; ?>
                                    <?php if($order['products'] > 0) { ?> и еще

                                        <span class='goods_order_description_name_goods-amount_number'><?php echo $order['products']; ?></span>

                                    <?php } ?>
                                </p>
                                <p class="goods_order_description_sum-price">
                                    <span class='goods_order_description_sum-price_number'>
                                      <?php echo $order['total']; ?>
                                    </span>
                                </p>
                                <p class="goods_order_description_status"><?php echo $order['status']; ?></p>
                            </div>
                        </div>

                        <?php } ?>

                        <?php } else { ?>
                        <div class="my-account_orders_goods_empty">
                            <p class="my-account_orders_goods_empty_description">У вас пока нет активных заказов.
                                Посмотрите в <a href='#' class='my-account_orders_goods_empty_description_link'>архиве</a> или
                                перейдите к <a href='#' class='my-account_orders_goods_empty_description_link'>покупкам</a>.
                            </p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="my-account_orders_goods js-account-list-view" list="archive">
                        <div class="my-account_orders_goods_empty my-account_orders_goods_order-active">
                            <p class="my-account_orders_goods_empty_description">В архиве находятся все выполненные заказы</p>
                        </div>

                        <?php if ($orders_archive) { ?>

                        <?php foreach ($orders_archive as $order) { ?>
                        <div class="my-account_orders_goods_order my-account_orders_goods_order-active">
                            <a class="my-account_orders_goods_order_link" href="<?php echo $order['href']; ?>"></a>
                            <div class="goods_order_identifier">
                                <p class="goods_order_identifier_order-number">Заказ №<span
                                            class='goods_order_identifier_order-number_number'><?php echo $order['order_id']; ?></span>
                                </p>
                                <p class="goods_order_identifier_time">от <span
                                            class='goods_product_identifier_time_date'><?php echo $order['date_added']; ?></span>
                                </p></div>
                            <div class="goods_order_pointers">
                                <p class="goods_order_pointers_content">Товары в заказе</p>
                                <p class="goods_order_pointers_content">Общая стоимость</p>
                                <p class="goods_order_pointers_content">Статус</p>
                            </div>
                            <div class="goods_order_description">
                                <p class="goods_order_description_name">Спортивные брюки Nike
                                        <span class='goods_order_description_name_goods-amount_number'><?php echo $order['products']; ?></span>
                                </p>
                                <p class="goods_order_description_sum-price"><span
                                            class='goods_order_description_sum-price_number'><?php echo $order['total']; ?></span>
                                </p>
                                <p class="goods_order_description_status"><?php echo $order['status']; ?></p></div>

                        </div>
                        <?php } ?>


                        <?php } else { ?>
                        <div class="my-account_orders_goods_empty">
                            <p class="my-account_orders_goods_empty_description">У вас пока нет завершенных заказов.
                            </p>
                        </div>
                        <?php } ?>

                    </div>
                </div>
                <!---div class="catalog_clothes"><h2 class="catalog_clothes_title">Выгодные предложения специально для
                    вас</h2>
                  <ul class="clothes_list clothes_list-productPage clothes_list-account-page">
                    <li class="clothes_list_item">
                      <div class="clothes_list_item_wrap">
                        <button class="clothes_like"></button>
                        <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                        <div class="clothes_discount"><p class="clothes_discont_content">75%</p></div>
                        <img class="clothes_photo" src="/assets/images/catalog/clothes-photo.png">
                        <p class="clothes_name">Swing</p>
                        <p class="clothes_description">Качели вечернее платье с кружевом</p>
                        <div class="clothes_prices"><p class="clothes_prices_price">14 340 ₽</p></div>
                      </div>
                    </li>
                    <li class="clothes_list_item clothes_list_item-new">
                      <div class="clothes_list_item_wrap">
                        <button class="clothes_like"></button>
                        <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                        <div class="clothes_discount"><p class="clothes_discont_content">75%</p></div>
                        <img class="clothes_photo" src="/assets/images/catalog/clothes-photo.png">
                        <p class="clothes_name">Swing</p>
                        <p class="clothes_description">Качели вечернее платье с кружевом</p>
                        <div class="clothes_prices"><p class="clothes_prices_price">14 340 ₽</p></div>
                      </div>
                    </li>
                    <li class="clothes_list_item clothes_list_item-discount">
                      <div class="clothes_list_item_wrap">
                        <button class="clothes_like"></button>
                        <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                        <div class="clothes_discount"><p class="clothes_discont_content">75%</p></div>
                        <img class="clothes_photo" src="/assets/images/catalog/clothes-photo.png">
                        <p class="clothes_name">Swing</p>
                        <p class="clothes_description">Качели вечернее платье с кружевом</p>
                        <div class="clothes_prices"><p class="clothes_prices_new-price">14 340 ₽</p>
                          <p class="clothes_prices_price">14 340 ₽</p></div>
                      </div>
                    </li>
                    <li class="clothes_list_item clothes_list_item-new clothes_list_item-liked">
                      <div class="clothes_list_item_wrap">
                        <button class="clothes_like"></button>
                        <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                        <div class="clothes_discount"><p class="clothes_discont_content">75%</p></div>
                        <img class="clothes_photo" src="/assets/images/catalog/clothes-photo.png">
                        <p class="clothes_name">Swing</p>
                        <p class="clothes_description">Качели вечернее платье с кружевом</p>
                        <div class="clothes_prices"><p class="clothes_prices_price">14 340 ₽</p></div>
                      </div>
                    </li>
                    <li class="clothes_list_item">
                      <div class="clothes_list_item_wrap">
                        <button class="clothes_like"></button>
                        <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                        <div class="clothes_discount"><p class="clothes_discont_content">75%</p></div>
                        <img class="clothes_photo" src="/assets/images/catalog/clothes-photo.png">
                        <p class="clothes_name">Swing</p>
                        <p class="clothes_description">Качели вечернее платье с кружевом</p>
                        <div class="clothes_prices"><p class="clothes_prices_price">14 340 ₽</p></div>
                      </div>
                    </li>
                  </ul>
                </div--->
            </div>
        </div>
    </section>
</section>
<script>var pageName = 'MyAccountPage';</script>

<?php echo $footer; ?>