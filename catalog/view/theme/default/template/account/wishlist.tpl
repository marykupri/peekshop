<?php echo $header; ?>

<section class="app_content">
    <section class="account_page">
        <div class="catalog_breadcrumbs">
            <ul class="breadcrumbs_list">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link"
                                                     href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <?php } ?>
                </li>
            </ul>
        </div>

        <?php if($logged){ ?>
        <div class="my-account"><h2 class="my-account_title">Мой аккаунт</h2>
            <div class="my-account_aside">
                <?php echo $column_left; ?>
            </div>

            <div class="my-account_main">
                <div class="account-favorites">
                    <?php } else { ?>
                    <div class="unauthorized-favorites">
                        <div class="catalog_clothes"><h2 class="catalog_clothes_title">Избранное</h2>
                            <p class="unauthorized-favorites_notice">Чтобы сохранить и просматривать список с других
                                устройств —
                                <span class="js-header-entrance-btn unauthorized-favorites_notice_link"> войти с паролем </span>
                            </p>
                            <?php } ?>

                            <?php if ($products) { ?>

                            <?php if($logged){ ?>
                            <div class="catalog_clothes account-favorites_content-active">
                                <h2 class="catalog_clothes_title"> Избранное</h2>
                                <?php } ?>

                                <ul class="clothes_list clothes_list-favorites">

                                    <?php foreach ($products as $product) { ?>

                                    <li class="clothes_list_item clothes_list_item-liked product_<?php echo $product['product_id']; ?>">
                                        <div class="clothes_list_item_wrap">
                                            <button type="button" class="clothes_like" onclick="wishlist.remove('<?php echo $product['product_id']; ?>');"></button>
                                            <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                                            <div class="clothes_discount">
                                                <p class="clothes_discont_content">75%</p>
                                            </div>
                                            <a href="<?php echo $product['href']; ?>">
                                                <img class="clothes_photo" src="<?php echo $product['thumb']; ?>">
                                            </a>
                                            <p class="clothes_name"><?php echo $product['manufacturer']; ?></p>
                                            <p class="clothes_description"><?php echo $product['name']; ?></p>
                                            <div class="clothes_prices"><p
                                                        class="clothes_prices_price"><?php echo $product['price']; ?></p>
                                            </div>
                                            <!---button type="button" onclick="location.href = '<?php echo $product['href']; ?>'" class="clothes_btn clothes_btn-in-basket">В корзину</button--->
                                        </div>
                                    </li>

                                    <?php } ?>

                                </ul>
                            </div>
                            <?php } else { ?>
                            <?php if($logged){ ?>
                            <div class="">
                                <h2 class="catalog_clothes_title">Избранное</h2>
                                <p class="account-favorites_empty_description">Хотите сохранить понравившиеся товары?
                                    Просто нажмите на значок в виде сердца на товаре, после чего этот товар появится здесь.</p>
                                <p class="account-favorites_empty_description">Добавив товар в Избранное, вы легко
                                    сможете
                                    вернуться к нему в удобное для вас время. Например, можно добавить понравившуюся вам
                                    модель в случае отсутствия подходящего размера, что позволит удобнее отслеживать
                                    пополнения ассортимента.</p>
                            </div>
                            <?php } ?>
                            <?php } ?>

                        </div>
                        <?php if($logged){ ?>
                    </div>
                </div>
                <?php } ?>
    </section>
</section>
<script>var pageName = 'AccountFavoritesAuthorizedPage';</script>

<?php echo $footer; ?>