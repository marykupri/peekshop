<?php echo $header; ?>

<section class="app_content">
    <section class="account_page">
        <div class="catalog_breadcrumbs">
            <ul class="breadcrumbs_list">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link"
                                                     href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <?php } ?>
                </li>
            </ul>
        </div>
        <div class="my-account"><h2 class="my-account_title"><?php echo $text_my_account; ?></h2>
            <div class="my-account_aside">
                <?php echo $column_left; ?>
            </div>
            <div class="my-account_main">
                <div class="account-data">
                    <div class="account-data_heading"><h2 class="account-data_heading_title">Мои данные</h2>
                        <p class="account-data_heading_email"><?php echo $email; ?></p></div>

                    <form class="account-data_form" action="<?php echo $action; ?>" method="POST">
                        <div class="account-data_field">
                            <input class="account-data_field_personal-data" type="text" value="<?php echo $firstname; ?>" name="firstname">
                            <p class="account-data_field_indicator">Имя</p>
                        </div>
                        <?php if ($error_firstname) { ?>
                        <div class="account-data_error"><?php echo $error_firstname; ?></div>
                        <?php } ?>
                        <div class="account-data_field">
                            <input class="account-data_field_personal-data" type="text" value="<?php echo $lastname; ?>" name="lastname">
                            <p class="account-data_field_indicator">Фамилия</p>
                        </div>
                        <?php if ($error_lastname) { ?>
                        <div class="account-data_error"><?php echo $error_lastname; ?></div>
                        <?php } ?>
                        <div class="account-data_field">
                            <input class="account-data_field_personal-data js-phone-delivery" type="text" value="<?php echo $telephone; ?>" name="telephone">
                            <p class="account-data_field_indicator">Телефон</p>
                        </div>
                        <?php if ($error_telephone) { ?>
                        <div class="account-data_error"><?php echo $error_telephone; ?></div>
                        <?php } ?>

                        <div class="account-data_field">
                            <input class="account-data_field_personal-data account-data_field_personal-data_birthday js-born-date" type="text" spellcheck="false" value="<?php echo $birthday; ?>" name="birthday">
                            <p class="account-data_field_indicator">Дата рождения</p>
                        </div>

                        <div class="account-data_male account-data_field">
                            <label class="account-data_male_wrap">
                                <input class="account-data_male_check-male" type="radio" name="male" value="1" <?php $male == 1 ? print 'checked' : '' ; ?> >
                                <div class="account-data_male_for-check-male <?php print ($error_male) ? 'account-data_field_error' : '' ; ?>">Женщина</div>
                            </label>
                            <label class="account-data_male_wrap">
                                <input class="account-data_male_check-male" type="radio" name="male" value="2" <?php $male == 2 ? print 'checked' : '' ; ?>>
                                <div class="account-data_male_for-check-male <?php print ($error_male) ? 'account-data_field_error' : '' ; ?>">Мужчина</div>
                            </label>
                        </div>
                        <?php if ($error_male) { ?>
                        <div class="account-data_error"><?php echo $error_male; ?></div>
                        <?php } ?>

                        <label class="account-data_subscribe">
                            <?php if ($newsletter) { ?>
                            <input class="account-data_subscribe_check" type="checkbox" value="0" checked="checked"
                                   name="newsletter">
                            <?php } else { ?>
                            <input class="account-data_subscribe_check" type="checkbox" value="1" name="newsletter">
                            <?php } ?>
                            <div class="account-data_subscribe_for-check"></div>
                            <p class="account-data_subscribe_content">Подписка на почтовую рассылку акций и скидок</p>
                        </label>

                        <h2 class="account-data_form_title">Мои размеры</h2>
                        <div class="account-data_size">
                            <div class="account-data_size_select-size js-account-size-clothes"><p
                                        class="account-data_size_select-size_content">Размер одежды</p><input
                                        class="account-data_size_select-size_size js-account-size-clothes-input"
                                        type="text" readonly value="44"></div>
                            <ul class="account-data_size_select-size_list js-account-size-clothes-list">
                                <li class="account-data_size_select-size_list_item js-account-size-clothes-item">
                                    45
                                </li>
                                <li class="account-data_size_select-size_list_item js-account-size-clothes-item">
                                    46
                                </li>
                                <li class="account-data_size_select-size_list_item js-account-size-clothes-item">
                                    47
                                </li>
                                <li class="account-data_size_select-size_list_item js-account-size-clothes-item">
                                    48
                                </li>
                            </ul>
                        </div>
                        <div class="account-data_size">
                            <div class="account-data_size_select-size js-account-size-shoes"><p
                                        class="account-data_size_select-size_content">Размер обуви</p><input
                                        class="account-data_size_select-size_size js-account-size-shoes-input"
                                        type="text" readonly value="47"></div>
                            <ul class="account-data_size_select-size_list js-account-size-shoes-list">
                                <li class="account-data_size_select-size_list_item js-account-size-shoes-item">45
                                </li>
                                <li class="account-data_size_select-size_list_item js-account-size-shoes-item">46
                                </li>
                                <li class="account-data_size_select-size_list_item js-account-size-shoes-item">47
                                </li>
                                <li class="account-data_size_select-size_list_item js-account-size-shoes-item">48
                                </li>
                            </ul>
                        </div>
                        <a class="account-data_choose-size" href="#">Подобрать размер</a>

                        <ul class="account-data_form_addresses_wrap">
                            <h2 class="account-data_form_title">Мои адреса</h2>

                            <?php if ($addresses) { ?>
                            <?php foreach ($addresses as $result => $adr) { ?>

                                <li class="account-data_form_address">
                                    <label class="account-data_field">
                                        <input class="account-data_field_personal-data" type="text" name="address[<?php print $adr['address_id']?>][country]" value="<?php print $adr['country']?>">
                                        <p class="account-data_field_indicator">Страна</p>
                                    </label>
                                    <label class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="address[<?php print $adr['address_id']?>][city]" value="<?php print $adr['city']?>">
                                        <p class="account-data_field_indicator">Город</p>
                                    </label>
                                    <label class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="address[<?php print $adr['address_id']?>][postcode]" value="<?php print $adr['postcode']?>">
                                        <p class="account-data_field_indicator">Индекс</p>
                                    </label>
                                    <label class="account-data_field">
                                        <input class="account-data_field_personal-data" type="text" name="address[<?php print $adr['address_id']?>][address_1]" value="<?php print $adr['address_1']?>">
                                        <p class="account-data_field_indicator">Улица</p>
                                    </label>
                                    <label class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="address[<?php print $adr['address_id']?>][house_number]" value="<?php print $adr['house']?>">
                                        <p class="account-data_field_indicator">Дом</p>
                                    </label>
                                    <label class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="address[<?php print $adr['address_id']?>][apartment_number]" value="<?php print $adr['apartment']?>">
                                        <p class="account-data_field_indicator">кв./офис</p>
                                    </label>
                                    <span class="account-data_form_address_delete js-address-delete" data-id="<?php print $adr['address_id']?>">Удалить</span>
                                </li>

                            <?php } ?>
                            <?php } ?>

                        </ul>
                        <span class="account-data_add-address">Добавить новый адрес</span>

                        <!---div class="account-data_form_address">
                            <h2 class="account-data_form_title">Мои адреса</h2>


                            <?php if ($addresses) { ?>

                                <?php foreach ($addresses as $result => $adr) { ?>

                                    <div class="account-data_field">
                                        <input class="account-data_field_personal-data" type="text" name="country" value="<?php print $adr['country']?>">
                                        <p class="account-data_field_indicator">Страна</p>
                                    </div>
                                    <div class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="city" value="<?php print $adr['city']?>">
                                        <p class="account-data_field_indicator">Город</p>
                                    </div>
                                    <div class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="postcode" value="<?php print $adr['postcode']?>">
                                        <p class="account-data_field_indicator">Индекс</p>
                                    </div>
                                    <div class="account-data_field">
                                        <input class="account-data_field_personal-data" type="text" name="address_1" value="<?php print $adr['address_1']?>">
                                        <p class="account-data_field_indicator">Улица</p>
                                    </div>
                                    <div class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="house_number" value="<?php print $adr['house']?>">
                                        <p class="account-data_field_indicator">Дом</p>
                                    </div>
                                    <div class="account-data_field account-data_field-width">
                                        <input class="account-data_field_personal-data" type="text" name="apartment_number" value="<?php print $adr['apartment']?>">
                                        <p class="account-data_field_indicator">кв./офис</p>
                                    </div>

                                    <input type="hidden" name="address_id">

                                <?php } ?>
                            <?php } ?>

                        </div---->

                        <div class="account-data_save-changes">
                            <button class="account-data_save-changes_btn account-data_save-changes_btn-save">
                                Сохранить
                            </button>
                            <button class="account-data_save-changes_btn account-data_save-changes_btn-cancel">
                                Отмена
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
<script>var pageName = 'AccountDataPage';</script>

<?php echo $footer; ?>
