<?php echo $header; ?>

<section class="app_content">
  <section class="offer-page">
    <div class="catalog_breadcrumbs">
      <ul class="breadcrumbs_list">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="offer-page_content"><h2 class="offer-page_content_title"><?php echo $heading_title; ?></h2>
      <?php if ($error) { ?>
      <div class="warning"><?php echo $error; ?></div>
      <?php } ?>
      <div class="offer-page_content_wrap">
        <p class="offer-page_content_description"><?php echo $description; ?></p>
      </div>
    </div>
  </section>
</section>
<script>var pageName = 'OfferPage';</script>

<?php echo $footer; ?>