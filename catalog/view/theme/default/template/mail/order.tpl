<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<table style="background: #ffffff;  width: 600px">
    <tbody>
    <tr>
        <td style="vertical-align: top; padding: 0">
            <div style="width: 600px; height: 100px; padding-top: 30px">
                <div style="width: 129px; padding-left: 235px">
                    <a style="display: inline-block; width: 129px; text-decoration: none;" href="<?php echo $store_url; ?>">
                        <img style="width: 129px; height: 17px" src="<?php print $social_icon_logo ; ?>" alt="">
                    </a>
                </div>
                <div style="width: 450px; margin-left: 70px; margin-top: 30px">
                    <a href="<?php echo $catalog_woman ; ?>"
                       style="text-decoration: none; color: #323232; font-family: Arial, Helvetica, sans-serif; -webkit-text-size-adjust: none; font-size: 16px; display: inline-block; margin-right: 30px">Женщинам</a>
                    <a href="<?php echo $catalog_man ; ?>"
                       style="text-decoration: none; color: #323232; font-family: Arial, Helvetica, sans-serif; -webkit-text-size-adjust: none; font-size: 16px; display: inline-block; margin-right: 30px">Мужчинам</a>
                    <a href="<?php echo $catalog_children ; ?>"
                       style="text-decoration: none; color: #323232; font-family: Arial, Helvetica, sans-serif; -webkit-text-size-adjust: none; font-size: 16px; display: inline-block; margin-right: 30px">Детям</a>
                    <a href="<?php echo $catalog_promohome ; ?>"
                       style="text-decoration: none; display: inline-block; color: #323232; font-family: Arial, Helvetica, sans-serif; -webkit-text-size-adjust: none; font-size: 16px">
                        <img src="<?php echo $social_icon_home ; ?>" alt=""
                             style="display: inline-block; width: 20px; height: 27px">
                        Nemepik home
                    </a>
                </div>
                <div>
        </td>
    </tr>
    </tbody>
    <tbody>
    <tr>
        <td style="vertical-align: top; padding: 0">
            <div style="width: 550px; padding: 50px 0 0 30px; height: 330px">
                <span style="display: block; margin-bottom: 20px; color: #000000; -webkit-text-size-adjust: none; font-size: 24px; font-family: Arial, Helvetica, sans-serif">Здравствуйте, <?php echo $customer_name ; ?>
                    !</span>
                <span style="display: block; padding-bottom: 180px; line-height: 21px; color: #000000; -webkit-text-size-adjust: none; font-size: 16px; font-family: Arial, Helvetica, sans-serif"><?php echo $message ; ?></span>
                <span style="display: block; line-height: 21px; color: #9B9B9B; -webkit-text-size-adjust: none; font-size: 16px; font-family: Arial, Helvetica, sans-serif">С уважением,<br>интернет-магазин <a
                            href="<?php echo $store_url; ?>" style="color: #9B9B9B">Nemepik.ru</a>.</span>
            </div>
        </td>
    </tr>
    </tbody>
    <table style="background: #F3F3F3">
        <tbody>
        <tr>
            <td style="vertical-align: top; padding: 0">
                <div style="width: 570px; height: 135px; padding: 45px 0 0 30px">
                    <div style="float: left; padding-right: 130px">
                        <span style="margin-bottom: 20px; display: block; font-family: Arial, Helvetica, sans-serif; -webkit-text-size-adjust: none; font-size: 14px; color: #000000">По всем вопросам обращайтесь к нам:</span>
                        <a style="margin-bottom: 5px; display: block; text-decoration: none; font-family: Arial, Helvetica, sans-serif; line-height: 23px; -webkit-text-size-adjust: none; font-size: 18px; color: #323232" href="tel:<?php echo $store_telephone ; ?>"><?php echo $store_telephone ; ?></a>
                        <a style="display: block; text-decoration: none; font-family: Arial, Helvetica, sans-serif; line-height: 23px; -webkit-text-size-adjust: none; font-size: 18px; color: #323232" href="mailto:<?php echo $store_email ; ?>" target="_blank"><?php echo $store_email ; ?></a>
                    </div>
                    <div style="display: inline-block">
                        <span style="margin-bottom: 20px; display: block; color: #000000; font-family: Arial, Helvetica, sans-serif; -webkit-text-size-adjust: none; font-size: 14px">Мы в соц. сетях</span>
                        <a style="margin-right: 10px; display: inline-block" href="">
                            <img src="<?php echo $social_icon_vk ; ?>" alt="">
                        </a>
                        <a style="margin-right: 10px; display: inline-block" href="">
                            <img src="<?php echo $social_icon_fb ; ?>" alt="">
                        </a>
                        <a style="display: inline-block" href="">
                            <img src="<?php echo $social_icon_ig ; ?>" alt="">
                        </a>
                    </div>
                </div>
            </td>
        <tr>
        </tbody>
    </table>
</table>
</body>
</html>