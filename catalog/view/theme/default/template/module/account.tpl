 <ul class="aside_menu">
    <li class="aside_menu_item"><a class="aside_menu_item_link <?php echo $active_class_order; ?>" href="<?php echo $order; ?>">Мои заказы</a></li>
    <li class="aside_menu_item"><a class="aside_menu_item_link <?php echo $active_class_account; ?>" href="<?php echo $account; ?>">Мои данные</a></li>
    <li class="aside_menu_item"><a class="aside_menu_item_link <?php echo $active_class_voucher; ?>" href="<?php echo $voucher; ?>">Мои купоны</a></li>
    <li class="aside_menu_item"><a class="aside_menu_item_link <?php echo $active_class_wishlist; ?>" href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
 </ul>