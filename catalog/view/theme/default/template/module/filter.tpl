<div class="catalog_param-selection js-select-filter-list" list="sorting">
    <ul class="param-selection_list">

        <?php foreach ($filter_groups as $filter_group) { ?>
        <?php $class = '';?>
        <?php if($filter_group['filter_group_id'] == 3) $class = 'selected-type_selection-brand';?>
        <?php if($filter_group['filter_group_id'] == 4) $class = 'selected-type_selection-size';?>
        <?php if($filter_group['filter_group_id'] == 2) $class = 'selected-type_selection-color';?>
        <?php if($filter_group['filter_group_id'] == 1) $class = 'selected-type_selection-price';?>

        <li class="param-selection_list_item">
            <div class="selected-type_selection <?php echo $class; ?>">
                <p class="selected-type_selection_content js-category-select-btn" open="filter_<?php print $filter_group['filter_group_id']; ?>">
                    <?php echo $filter_group['name']; ?>
                    <?php if($filter_group['count_checked'] > 0) { ?>
                    <?php print "<span class='selected-type_selection_content_filter'>" . $filter_group['count_checked'] . "</span>"; ?>
                    <?php } ?>
                </p>

                <div class="selected-type_dropdown_selection js-category-select-list" list="filter_<?php print $filter_group['filter_group_id']; ?>">
                    <?php if($filter_group['filter_group_id'] == 3) { ?>
                    <div class="dropdown_selection_search">
                        <input class="dropdown_selection_search_field search_manufacturer" type="text" placeholder="Поиск бренда">
                    </div>
                    <?php } ?>
                    <ul class="dropdown_selection_list">
                        <?php foreach ($filter_group['filter'] as $filter) { ?>
                        <li class="dropdown_selection_list_item">
                            <label class="dropdown_selection_input-wrap dropdown_selection_input-wrap--margin">
                                <?php if (in_array($filter['filter_id'], $filter_category[$filter_group['name_alias']])) { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="<?php print $filter_group['name_alias'];?>[]"
                                       value="<?php echo $filter['filter_id']; ?>" checked="checked">
                                <?php } else { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="<?php print $filter_group['name_alias'];?>[]"
                                       value="<?php echo $filter['filter_id']; ?>">
                                <?php } ?>
                                <div class="dropdown_selection_for-checked"></div>
                                <p class="dropdown_selection_description"><?php echo $filter['name']; ?></p>
                            </label>
                        </li>
                        <?php } ?>
                    </ul>
                    <div class="dropdown_selection_apply">
                        <button type="button" id="button-filter" class="dropdown_selection_apply_btn button-filter">Применить</button>
                    </div>

                </div>
            </div>
        </li>

        <?php } ?>

        <!-- на странице поиска фильтр по категориям --->
        <?php if(isset($searchProductParents)) { ?>
        <li class="param-selection_list_item">
            <div class="selected-type_selection <?php echo $class; ?>">
                <p class="selected-type_selection_content js-category-select-btn" open="filter_categoris">
                    Категории
                    <?php if($count_checked_category > 0) { ?>
                        <span class='selected-type_selection_content_filter'><?php print $count_checked_category; ?></span>
                    <?php } ?>
                </p>

                <div class="selected-type_dropdown_selection js-category-select-list" list="filter_categoris">

                    <ul class="dropdown_selection_list">
                        <?php foreach ($filterSeacrhCategoris as $category_1) { ?>

                        <?php if (in_array($category_1['category_id'], $searchProductParents)) { ?>
                        <li class="dropdown_selection_list_item">
                            <label class="dropdown_selection_input-wrap">
                                <?php if (in_array($category_1['category_id'], $category_id)) { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="category[]" value="<?php echo $category_1['category_id']; ?>" checked="checked">
                                <?php } else { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="category[]" value="<?php echo $category_1['category_id']; ?>">
                                <?php } ?>
                                <div class="dropdown_selection_for-checked"></div>
                                <p class="dropdown_selection_description"><?php echo $category_1['name']; ?></p>
                            </label>
                        </li>

                        <?php foreach ($category_1['children'] as $category_2) { ?>
                        <?php if (in_array($category_2['category_id'], $searchProductParents)) { ?>
                        <li class="dropdown_selection_list_item">
                            <label class="dropdown_selection_input-wrap">
                                <?php if (in_array($category_2['category_id'], $category_id)) { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="category[]" value="<?php echo $category_2['category_id']; ?>" checked="checked">
                                <?php } else { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="category[]" value="<?php echo $category_2['category_id']; ?>">
                                <?php } ?>
                                <div class="dropdown_selection_for-checked"></div>
                                <p class="dropdown_selection_description"> - <?php echo $category_2['name']; ?></p>
                            </label>
                        </li>
                        <?php } ?>
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <?php if (in_array($category_3['category_id'], $searchProductParents)) { ?>
                        <li class="dropdown_selection_list_item">
                            <label class="dropdown_selection_input-wrap">
                                <?php if (in_array($category_3['category_id'], $category_id)) { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="category[]" value="<?php echo $category_3['category_id']; ?>" checked="checked">
                                <?php } else { ?>
                                <input class="dropdown_selection_checked" type="checkbox" name="category[]" value="<?php echo $category_3['category_id']; ?>">
                                <?php } ?>
                                <div class="dropdown_selection_for-checked"></div>
                                <p class="dropdown_selection_description"> -- <?php echo $category_3['name']; ?></p>
                            </label>
                        </li>

                        <?php } ?>

                        <?php } ?>

                        <?php } ?>

                        <?php } ?>

                        <?php } ?>
                    </ul>

                    <div class="dropdown_selection_apply">
                        <button type="button" id="button-filter" class="dropdown_selection_apply_btn button-filter">Применить</button>
                    </div>
                </div>
            </div>
        </li>
        <?php } ?>

        <li class="param-selection_list_item">

            <div class="selected-type_selection selected-type_selection-price">
                <p class="selected-type_selection_content js-category-select-btn" open="price">
                    <?php echo $price_category['minprice']; ?> - <?php echo $price_category['maxprice']; ?>
                </p>

                <div class="selected-type_dropdown_selection js-category-select-list" list="price">
                    <div class="dropdown_selection_price-variation">
                        <p class="dropdown_selection_price-variation_index">от</p>
                        <input class="dropdown_selection_price-variation_field" type="text" name="price_min" value="<?php echo $price_category_result['minprice']; ?>" placeholder="<?php echo $filter_prices['minprice']; ?>">
                        <p class="dropdown_selection_price-variation_index">до</p>
                        <input class="dropdown_selection_price-variation_field" type="text" name="price_max" value="<?php echo $price_category_result['maxprice']; ?>" placeholder="<?php echo $filter_prices['maxprice']; ?>">
                    </div>
                    <div class="dropdown_selection_apply">
                        <button type="button" id="button-filter" class="dropdown_selection_apply_btn button-filter">Применить</button>
                    </div>
                </div>
            </div>
        </li>

        <?php if($isFiltered) { ?>
        <li class="param-selection_list_item param-selection_list_item-clean-filter">
            <div class="selected-type_selection selected-type_selection-brand">
                <a href="<?php echo $no_action; ?>" class="selected-type_selection_content">Очистить фильтрацию </a>
            </div>
        </li>
        <?php } ?>

    </ul>
</div>

<script type="text/javascript"><!--
    $('.button-filter').on('click', function () {
        filter = [];
        price_min = [];
        price_max = [];
        brand = [];
        color = [];
        size = [];
        category = [];
        pricedef = [];
        filter_param = '';

        $('input[name^=\'filter\']:checked').each(function (element) {
            filter.push(this.value);
        });

        $('input[name^=\'price_min\']').each(function (element) {
            if(this.value != '') price_min.push(this.value);
        });

        $('input[name^=\'price_max\']').each(function (element) {
            if(this.value != '') price_max.push(this.value);
        });

        $('input[name^=\'category\']:checked').each(function (element) {
            if(this.value != '') category.push(this.value);
        });

        $('input[name^=\'brand\']:checked').each(function (element) {
            if(this.value != '') brand.push(this.value);
        });

        $('input[name^=\'color\']:checked').each(function (element) {
            if(this.value != '') color.push(this.value);
        });

        $('input[name^=\'size\']:checked').each(function (element) {
            if(this.value != '') size.push(this.value);
        });

        if(filter.join(',') != '') filter_param = filter_param + '&filter=' + filter.join(',');
        if(price_min != '') filter_param = filter_param + '&minprice=' + price_min.join(',');
        if(price_max != '') filter_param = filter_param + '&maxprice=' + price_max.join(',');
        if(category != '') filter_param = filter_param + '&category_id=' + category.join(',');
        if(color != '') filter_param = filter_param + '&colors=' + color.join(',');
        if(size != '') filter_param = filter_param + '&sizes=' + size.join(',');
        if(brand != '') filter_param = filter_param + '&brands=' + brand.join(',');

        if(filter_param != '') location = '<?php echo $action; ?>' + filter_param;
    });
    //--></script>
