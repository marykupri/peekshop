<?php foreach ($categories as $category) { ?>
    <ul class="catalog_aside_list js-select-filter-list" list="category">
    <h2 class="catalog_aside_list_title"><?php echo $category['name']; ?></h2>
        <?php foreach ($category['children'] as $child) { ?>
                <li class="catalog_aside_list_item">
                    <span class="catalog_aside_list_item_link catalog_link-selected js-clothes-category-select-btn" open="cat_<?php print $child['category_id']; ?>"><?php echo $child['name']; ?></span>
                    <?php if ($child['children']) { ?>
                        <ul class="catalog_aside_sub-list js-clothes-category-select-list" list="cat_<?php print $child['category_id']; ?>">
                            <?php foreach ($child['children'] as $child2) { ?>
                                <?php if ($child2['category_id'] == $child2_id) { ?>
                                    <li class="catalog_aside_list_item">
                                        <a class="catalog_aside_list_item_link catalog_link-selected" href="<?php echo $child2['href']; ?>"><?php echo $child2['name']; ?></a>
                                    </li>
                                <?php } else { ?>
                                    <li class="catalog_aside_list_item">
                                        <a class="catalog_aside_list_item_link" href="<?php echo $child2['href']; ?>"><?php echo $child2['name']; ?></a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>

        <?php } ?>
    </ul>
<?php } ?>