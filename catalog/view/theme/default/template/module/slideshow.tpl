<ul class="slider" id="slider-home">
<?php foreach ($banners as $banner) { ?>
    <?php if ($banner['link']) { ?>
    <li class="slider_slide slider_slide-active">
        <a class="slider_slide_link" href="<?php echo $banner['link']; ?>">
            <img class="slider_slide_background" src="<?php echo $banner['image']; ?>">
            <div class="slider_slide_description">
                <h1 class="slider_slide_description_title"><?php echo $banner['title']; ?></h1>
                <p class="slider_slide_description_content"><?php echo $banner['description']; ?></p>
            </div>
        </a>
    </li>
    <?php } else { ?>
    <li class="slider_slide slider_slide-active">
        <img class="slider_slide_background" src="<?php echo $banner['image']; ?>">
        <div class="slider_slide_description">
            <h1 class="slider_slide_description_title"><?php echo $banner['title']; ?></h1>
            <p class="slider_slide_description_content"><?php echo $banner['description']; ?></p>
        </div>
    </li>
    <?php } ?>
<?php } ?>
</ul>