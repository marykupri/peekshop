<div class="logo-brands">
  <?php foreach ($banners as $banner) { ?>
  <?php if ($banner['link']) { ?>
  <a class="logo-brands_link" href="<?php echo $banner['link']; ?>"><img class="logo-brands_logo" src="<?php echo $banner['image']; ?>" /></a>
  <?php } else { ?>
  <a class="logo-brands_link" href="#"><img class="logo-brands_logo" src="<?php echo $banner['image']; ?>"/></a>
  <?php } ?>
  <?php } ?>
</div>