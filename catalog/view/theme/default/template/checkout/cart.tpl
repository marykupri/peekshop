<?php echo $header; ?>

<section class="app_content">
    <section class="ordering_page">

        <div class="modal">
            <div class="issued-order"><h2 class="modal_window_title">Заказ оформлен!</h2>
                <button class="modal_cls-btn"></button>
                <div class="issued-order_description">
                    <p class="issued-order_description_content">Спасибо за покупку!</p>
                    <p class="issued-order_description_content">Наш менеджер обязательно свяжется с вами в ближайшее
                        время.</p>
                    <p class="issued-order_description_content">В благодарность за заказ дарим вам промокод на следующую
                        покупку:</p>
                    <p class="issued-order_description_number">R45RRY8P4</p>
                </div>
            </div>
        </div>

        <div class="modal">
            <div class="modal_buy-coupon"><h2 class="modal_window_title">Купить купон</h2>
                <button class="modal_cls-btn"></button>
                <div class="modal_buy-coupon_coupon">
                    <div class="modal_buy-coupon_coupon_wrap">
                        <p class="modal_buy-coupon_coupon_number">Купон №088P</p>
                        <p class="modal_buy-coupon_coupon_date">от 18 февраля 2017 </p>
                    </div>
                    <div class="modal_buy-coupon_coupon_wrap">
                        <p class="modal_buy-coupon_coupon_price">14 457 ₽</p>
                    </div>
                </div>
                <form class="modal_buy-coupon_form">
                    <div class="buy-coupon_pay-methods">
                        <label class="buy-coupon_pay-methods_wrap">
                            <input class="buy-coupon_pay-methods_check-method" type="radio" name="buy-coupon_payMethod">
                            <div class="buy-coupon_pay-methods_for-check-method"></div>
                            <p class="buy-coupon_pay-methods_name">PayPal</p>
                        </label>
                        <label class="buy-coupon_pay-methods_wrap">
                            <input class="buy-coupon_pay-methods_check-method" type="radio" name="buy-coupon_payMethod">
                            <div class="buy-coupon_pay-methods_for-check-method"></div>
                            <p class="buy-coupon_pay-methods_name">Visa / Mastercard / Мир</p>
                        </label>
                        <label class="buy-coupon_pay-methods_wrap">
                            <input class="buy-coupon_pay-methods_check-method" type="radio" name="buy-coupon_payMethod">
                            <div class="buy-coupon_pay-methods_for-check-method"></div>
                            <p class="buy-coupon_pay-methods_name">Webmoney</p>
                        </label>
                    </div>
                    <button class="modal_buy-coupon_btn">Оплатить</button>
                </form>
            </div>
        </div>

        <div class="modal">
            <div class="modal_sign-in window-active"><h2 class="modal_window_title">Чтобы купить купон, необходимо
                    войти или зарегистрироваться</h2>
                <button class="modal_cls-btn"></button>
                <form class="registration_sign-in_form">
                    <input class="registration_sign-in_field" type="text" placeholder="Эл. почта">
                    <label class="registration_sign-in_wrap">
                        <input class="registration_sign-in_check" type="checkbox">
                        <div class="registration_sign-in_for-check"></div>
                        <p class="registration_sign-in_description">Подписаться на новости и скидки</p>
                    </label>
                    <input class="registration_sign-in_field" type="text" placeholder="Пароль">
                    <input class="registration_sign-in_field" type="text" placeholder="Имя">
                    <label class="registration_sign-in_wrap">
                        <input class="registration_sign-in_check" type="checkbox">
                        <div class="registration_sign-in_for-check"></div>
                        <p class="registration_sign-in_description">Я согласен с <a href='#'
                                                                                    class='registration_sign-in_offer-link'>офертой</a>
                        </p>
                    </label>
                    <button class="registration_sign-in_sign-up-btn">Зарегистрироваться</button>
                    <div class="ordering_order-product_form_other-sources">
                        <p class="form_other-sources_description">или войти через</p>
                        <a class="form_other-sources_link form_other-sources_link-vk" href="#"></a>
                        <a class="form_other-sources_link form_other-sources_link-fb" href="#"></a>
                    </div>
                    <button class="registration_sign-in_sign-ip-btn">Войти</button>
                </form>
            </div>
        </div>

        <div class="catalog_breadcrumbs">
            <ul class="breadcrumbs_list">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link"
                                                     href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <div class="ordering_orders"><h2 class="ordering_orders_title"><?php echo $heading_title; ?></h2>
            <div class="ordering_orders_nav">
                <div class="orders_nav_total"><p class="ordering_orders_content"><span
                                class='orders_nav_total_number'>5</span> товаров</p></div>
                <div class="orders_nav_amount"><P class="ordering_orders_content">Количество</P></div>
                <div class="orders_nav_price"><P class="ordering_orders_content">Цена</P></div>
            </div>

            <?php foreach ($products as $product) { ?>
            <div class="ordering_orders_product">

                <div class="orders_product_image">
                    <?php if ($product['thumb']) { ?>
                    <img class="orders_product_image_photo" src="<?php echo $product['thumb']; ?>">
                    <?php } ?>
                </div>

                <div class="orders_product_description">
                    <p class="ordering_orders_content orders_product_description_name"><?php echo $product['name']; ?></p>

                    <?php if ($product['option']) { ?>
                    <?php foreach ($product['option'] as $option) { ?>
                    <p class="ordering_orders_content orders_product_description_size"><?php echo $option['name']; ?>:
                        <span class='orders_product_size'><?php echo $option['value']; ?></span>
                    </p>
                    <?php } ?>
                    <?php } ?>

                    <!---p class="ordering_orders_content orders_product_description_presence">В наличии <span
                              class='orders_product_quantity'>2</span> шт.</p--->
                </div>
                <div class="orders_product_count js-product-counter">
                    <div class="orders_product_count_btn orders_product_count_btn-minus js-product-counter-minus"></div>
                    <input class="orders_product_count_field ordering_orders_content js-product-counter-input"
                           type="text" value="<?php echo $product['quantity']; ?>"
                           data-quantity="quantity[<?php echo $product['cart_id']; ?>]" readonly>
                    <div class="orders_product_count_btn orders_product_count_btn-plus js-product-counter-plus"></div>
                </div>
                <div class="orders_product_price">
                    <p class="ordering_orders_content orders_product_price_new">
                        <span class='orders_product_price_sum'><?php echo $product['total']; ?></span>
                    </p>
                    <button class="orders_product_price_delete"
                            onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><span
                                class='orders_product_price_delete_content'>Удалить</span>
                    </button>
                </div>
            </div>
            <?php } ?>

            <?php foreach ($vouchers as $vouchers) { ?>

            <div class="ordering_orders_product">

                <div class="orders_product_image">

                </div>

                <div class="orders_product_description">
                    <p class="ordering_orders_content orders_product_description_name"><?php echo $vouchers['description']; ?></p>
                    <!---
                                        <?php if ($product['option']) { ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                        <p class="ordering_orders_content orders_product_description_size"><?php echo $option['name']; ?>:
                                            <span class='orders_product_size'><?php echo $option['value']; ?></span>
                                   </p>
                                        <?php } ?>
                                        <?php } ?>

                                        <!---p class="ordering_orders_content orders_product_description_presence">В наличии <span
                                                  class='orders_product_quantity'>2</span> шт.</p--->
                </div>
                <div class="orders_product_count js-product-counter">
                    <!--div class="orders_product_count_btn orders_product_count_btn-minus js-product-counter-minus"></div>
                    <input class="orders_product_count_field ordering_orders_content js-product-counter-input"
                           type="text" value="<?php echo $product['quantity']; ?>"
                           data-quantity="quantity[<?php echo $product['cart_id']; ?>]" readonly>
                    <div class="orders_product_count_btn orders_product_count_btn-plus js-product-counter-plus"></div-->
                </div>
                <div class="orders_product_price">
                    <p class="ordering_orders_content orders_product_price_new">
                        <span class='orders_product_price_sum'><?php echo $vouchers['amount']; ?></span>
                    </p>
                    <button class="orders_product_price_delete"
                            onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><span
                                class='orders_product_price_delete_content'>Удалить</span>
                    </button>
                </div>
            </div>

            <!---tr>
                <td></td>
                <td class="text-left"><?php echo $vouchers['description']; ?></td>
                <td class="text-left"></td>
                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                        <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                        <span class="input-group-btn">
                            <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"
                                    onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>
                <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                <td class="text-right"><?php echo $vouchers['amount']; ?></td---->
            </tr>

            <?php } ?>


        </div>
        <div class="ordering_order-product">
            <?php if(!$logged) { ?>
            <div class="ordering_order-product_titles">
                <div class="ordering_order-product_title js-order-select" open="1">Я новый покупатель</div>
                <div class="ordering_order-product_title js-order-select" open="2">Я постоянный клиент Nemepik</div>
            </div>
            <?php } ?>

            <form class="ordering_order-product_form js-order-form" form="1" action="<?php print $ordering;?>"
                  method="post">
                <div class="order-product_form_fields order-product_form_fields-padding">
                    <div class="order-product_form_delivery">
                        <p class="ordering_orders_content">Доставка</p>
                    </div>

                    <div class="order-product_form_btns">
                        <?php if ($shipping_methods) { ?>
                            <?php $i=1;?>
                            <?php foreach ($shipping_methods as $shipping_method) { ?>
                                <?php if (!$shipping_method['error']) { ?>
                                    <?php foreach ($shipping_method['quote'] as $quote) { ?>
                                    <label class="order-product_form_btns_wrap">
                                        <input class="order-product_form_check" type="radio" name="shipping_method_code" data-price="<?php echo $quote['text']; ?>" value="<?php echo $quote['code']; ?>" <?php if($i == 1) echo 'checked'; ?>>
                                        <div class="order-product_form_for-check"></div>
                                        <p class="ordering_orders_content"><?php echo $quote['title']; ?></p>
                                    </label>
                                    <input class="order-shipping_method-title" type="hidden" name="shipping_method_<?php echo $quote['code']; ?>" value="<?php echo $quote['title']; ?>">
                                    <?php $i++;?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="order-product_form_delivery-price">

                        <?php if ($shipping_methods) { ?>
                            <?php foreach ($shipping_methods as $shipping_method) { ?>
                                <?php if (!$shipping_method['error']) { ?>
                                    <?php foreach ($shipping_method['quote'] as $quote) { ?>
                                        <p class="ordering_orders_content">
                                            <span class='form_delivery-price_price'><?php echo $quote['text']; ?></span>
                                        </p>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

                <h2 class="ordering_orders_content ordering_order-product_form_heading">Куда доставить заказ</h2>
                <h2 class="account-data_form_title">Мои адреса</h2>
                <div class="account-data_choose-location">

                    <?php if ($addresses) { ?>
                    <?php foreach ($addresses as $result => $adress) { if($result == 0) { ?>
                    <div class="account-data_choose-location_locality account-data_choose-location-border js-account-address">
                        <input class="account-data_choose-location_locality_select js-account-address-input" readonly type="text" value="<?php echo $adress['address']; ?>">
                    </div>
                    <?php } ?>
                    <?php } ?>

                    <ul class="account-data_choose-location_list account-data_choose-location-border js-account-address-list">

                        <?php foreach ($addresses as $result) { ?>
                        <li class="account-data_choose-location_list-item js-delivery-address-order js-account-address-item" data-id="<?php echo $result['address_id']; ?>"><?php echo $result['address']; ?></li>
                        <?php } ?>

                    </ul>
                    <?php } ?>
                </div>

                <div class="order-product_form_fields">
                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content" type="text" name="country">
                        <span class="order-product_form_fields_text">Страна</span>
                    </label>
                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content" type="text" name="city">
                        <span class="order-product_form_fields_text">Город</span>
                    </label>
                </div>
                <div class="order-product_form_fields">
                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content js-post-index" type="text" name="postcode">
                        <span class="order-product_form_fields_text">Почтовый индекс</span>
                    </label>
                </div>
                <div class="order-product_form_fields">

                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content" type="text" name="address_1">
                        <span class="order-product_form_fields_text">Улица</span>
                    </label>

                    <label class="order-product_form_fields_wrap order-product_form_fields_wrap-size">
                        <input class="order-product_form_fields_content js-post-index" type="text" name="house_number">
                        <span class="order-product_form_fields_text">Дом</span>
                    </label>
                    <label class="order-product_form_fields_wrap order-product_form_fields_wrap-size">
                        <input class="order-product_form_fields_content js-post-index" type="text" name="apartment_number">
                        <span class="order-product_form_fields_text">кв./офис</span>
                    </label>
                </div>
                <div class="order-product_form_fields order-product_form_fields-margin">
                    <textarea class="order-product_form_fields_field order-product_form_fields_field-comment" placeholder="Комментарий курьеру" name="comment"></textarea>
                </div>

                <h2 class="ordering_orders_content ordering_order-product_form_heading">Кому доставить заказ</h2>
                <div class="order-product_form_fields">
                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content" type="text" value="<?php echo $firstname . ' ' . $lastname;?>" name="firstname">
                        <span class="order-product_form_fields_text">Имя и фамилия</span>
                    </label>
                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content js-phone-delivery" type="text" value="<?php echo $telephone;?>" name="telephone">
                        <span class="order-product_form_fields_text">Моб. телефон</span>
                    </label>
                </div>

                <?php if(!$logged) { ?>
                <div class="order-product_form_fields order-product_form_fields-with-hint">
                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content js-mail-delivery" type="text" name="email">
                        <span class="order-product_form_fields_text">Эл. почта</span>
                    </label>
                    <span class="order-product_form_fields_hint">Электронная почта будет использоваться как логин для входа в личный кабинет</span>
                </div>
                <div class="order-product_form_fields order-product_form_fields-with-hint order-product_form_fields-margin">
                    <label class="order-product_form_fields_wrap">
                        <input class="order-product_form_fields_content" type="password" name="password">
                        <span class="order-product_form_fields_text">Пароль</span>
                    </label>
                    <span class="order-product_form_fields_hint">Придумайте пароль, для отслеживания заказа через личный кабинет</span>
                </div>
                <?php } ?>

                <h2 class="ordering_orders_content ordering_order-product_form_heading">Оплатить заказ</h2>
                <div class="order-product_form_fields">
                    <label class="order-product_form_fields_wrap order-product_form_fields_wrap-promocode">
                        <input class="order-product_form_fields_content" type="text" js-order-coupon>
                        <span class="order-product_form_fields_text">Промокод</span>
                    </label>
                    <button type="button" class="order-product_form_fields_apply js-order-coupon_btn">Применить</button>
                </div>
                <div class="order-coupon-error-msg"></div>

                <div class="order-product_form_fields order-product_form_fields-in-total">
                    <div class="order-product_form_btns order-product_form_btns-price-method">

                        <?php if ($payment_methods) { ?>
                            <?php $i=1;?>
                            <?php foreach ($payment_methods as $payment_method) { ?>
                                <label class="order-product_form_btns_wrap">
                                    <input class="order-product_form_check" type="radio" name="payment_method_code"
                                           value="<?php echo $payment_method['code']; ?>" <?php if($i == 1) echo 'checked'; ?>>
                                    <div class="order-product_form_for-check"></div>
                                    <p class="ordering_orders_content"><?php echo $payment_method['title']; ?></p>
                                </label>
                                <input class="order-payment_method-title" type="hidden" name="payment_method_<?php echo $payment_method['code']; ?>" value="<?php echo $payment_method['title']; ?>">
                                <?php $i++;?>
                            <?php } ?>
                        <?php } ?>

                        <?php if(!empty($klientVouchers)) { ?>

                        <label class="order-product_form_btns_wrap order-product_form_btns_wrap-with-coupon">
                            <input class="order-product_form_check order-product_form_check-coupon" type="radio"
                                   name="payment_method_code">
                            <div class="order-product_form_for-check"></div>
                            <div class="ordering_orders_content order-product_form-coupon">Использовать купон
                                <div class="ordering_orders_content-btn-to-list"
                                     onclick="document.querySelector('.order-product_form_btns_coupons').classList.toggle('order-product_form_btns_coupons--active')"></div>
                            </div>
                            <div class="order-product_form_btns_coupons">
                                <?php foreach($klientVouchers as $voucher) { ?>
                                <div class="order-product_form_btns_coupons_link js-order-voucher" data-code="<?php print $voucher['code']; ?>">
                                    <span class='form_coupon_amount'><?php print $voucher['amount']; ?></span>
                                </div>
                                <?php } ?>
                            </div>
                        </label>
                        <?php } ?>
                    </div>

                    <div class="order-product_form_total-price">
                        <p class="order-product_form_total-price_designation ordering_orders_content">Сумма за товары</p>
                        <p class="order-product_form_total-price_result ordering_orders_content js-order-result-summa"><?php echo $summa;?></p>

                        <p class="order-product_form_total-price_designation ordering_orders_content">Стоимость доставки</p>
                        <p class="order-product_form_total-price_result ordering_orders_content js-shipping-sum"></p>

                        <p class="order-product_form_total-price_designation ordering_orders_content">Скидка</p>
                        <?php if(isset($total_coupon)) { ?>
                        <p class="order-product_form_total-price_result ordering_orders_content js-coupon-sum"><?php echo $total_coupon; ?></p>
                        <?php } else { ?>
                        <p class="order-product_form_total-price_result ordering_orders_content js-coupon-sum">0 ₽</p>
                        <?php } ?>

                        <p class="order-product_form_total-price_designation ordering_orders_content">Купон</p>
                        <?php if(isset($total_voucher)) { ?>
                        <p class="order-product_form_total-price_result ordering_orders_content js-voucher-sum"><?php echo $total_voucher; ?></p>
                        <?php } else { ?>
                        <p class="order-product_form_total-price_result ordering_orders_content js-voucher-sum">0 ₽</p>
                        <?php } ?>

                        <p class="order-product_form_total-price_in-total order-product_form_total-price_in-total-text-align"> Итого </p>
                        <p class="order-product_form_total-price_in-total">
                            <span class='order-product_form_total-price_sum js-total-price_sum'><?php echo $total_summa;?></span></p>
                    </div>

                </div>

                <!--div class="order-product_form_fields order-product_form_fields-margin order-product_form_fields-buy-coupon order-product_form_fields-none">
                    <!--появляется если сумма превышает 1000 евро-->
                    <!--p class="order-product_form_buy-coupon">Сумма
                        за товары превышает €1000, чтобы избежать пошлины, рекомендуем вам приобрести купон в нашем
                        магазине, который позволяет оплатить до 50% стоимости покупки. Подробности в <a
                                class="order-product_form_buy-coupon_link" href="#">личном кабинете.</a></p>
                    <p class="order-product_form_conditions">Нажимая на кнопку «Оформить заказ», вы принимаете
                        условия <a href='<?php echo $oferta; ?>' class='order-product_form_conditions_link'>Публичной
                            оферты</a></p>
                    <button class="order-product_form_buy-btn">Купить купон</button>
                    <button class="order-product_form_place-an-order">Оформить заказ</button>
                </div-->

                <div class="order-product_form_fields order-product_form_fields-margin">
                    <!--если сумма превышает 1000 евро исчезает с помощью класса .order-product_form_fields-none-->
                    <p class="order-product_form_conditions">Нажимая на кнопку «Оформить заказ», вы принимаете<br>условия
                        <a href='<?php echo $oferta; ?>' class='order-product_form_conditions_link'>Публичной оферты</a>
                    </p>
                    <button type="button" class="order-product_form_place-an-order js-product_form-order">Оформить заказ</button>
                </div>
            </form>
            <?php if(!$logged) { ?>
            <form class="ordering_order-product_form ordering_order-product_form-sign-in js-order-form" form="2" action="<?php echo $login; ?>" method="post" enctype="multipart/form-data">
                <input class="ordering_order-product_form_account js-mail-login" type="text" name="email" placeholder="Эл. почта">
                <div class="ordering_order-product_form_password">
                    <input class="ordering_order-product_form_account ordering_order-product_form_account-padding" type="password" placeholder="Пароль" name="password">
                    <a class="ordering_order-product_form_forgot-password" href="#">Забыли пароль?</a>
                </div>
                <button type="submit" class="ordering_order-product_form_sign-in-btn ordering_order-product_form_sign-in-btn-clicked">
                    Войти
                </button>
                <div class="ordering_order-product_form_other-sources"><p class="form_other-sources_description">или
                        войти через</p><a class="form_other-sources_link form_other-sources_link-vk" href="#"></a><a
                            class="form_other-sources_link form_other-sources_link-fb" href="#"></a></div>
            </form>
            <?php } ?>
        </div>
    </section>
</section>
<script>var pageName = 'OrderingPage';</script>

<?php echo $footer; ?>
