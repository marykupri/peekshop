<?php echo $header; ?>

<section class="about-page">
    <div class="about-page_wrapper">
        <img class="about-page_wrapper_bgr" src="catalog/view/theme/default/images/about-page/about-page-bgr.png">
        <div class="about-page_wrapper_content">
            <h2 class="about-page_wrapper_content_title">ОДЕЖДА И ОБУВЬ<br>АВСТРИЙСКОГО КАЧЕСТВА </h2>
            <p class="about-page_wrapper_content_description">В ИНТЕРНЕТ-МАГАЗИНЕ ОДЕЖДЫ И ОБУВИ КАЖДЫЙ МОЖЕТ ВЫБРАТЬ И КУПИТЬ ОДЕЖДУ,<br>ОБУВЬ ИЛИ АКСЕССУАР В 2 КЛИКА.</p>
        </div>
    </div>
    <div class="about-page_description">
        <h2 class="about-page_description_title">Стиль — простой способ сказать сложные вещи</h2>
        <div class="about-page_description_article"><p
                    class="about-page_description_article_content about-page_description_article_content-margin">Nemepik
                – это интернет-магазин модной одежды, обуви, аксессуаров. Наша цель – оставить у клиента самые приятные
                впечатления от шопинга. Для этого мы предлагаем более 2 000 000 товаров и 1 000 брендов, быструю
                доставку, услугу предварительной примерки и удобные условия возврата. Именно поэтому нас выбирают более
                2 000 000 покупателей.</p>
            <p class="about-page_description_article_content">Гордость Nemepik – собственная инфраструктура: мощная
                IT-платформа, складской комплекс, служба доставки, call-центры и фотостудия. В 2016 году доступ к
                ним получили и другие ритейлеры: мы запустили маркетплейс и вышли на рынок B2B, предложив широкий
                спектр услуг третьим лицам.</p>
        </div>
        <div class="about-page_description_article">
            <p class="about-page_description_article_content">Нашу компанию
                основали Нильс Тонзен, Флориан Янсен, Доминик Пикер и Буркхард Биндер. В 2011 году они приехали в
                Россию, чтобы превратить бизнес-идею в успешный международный проект. Сегодня Nemepik также работает в
                Казахстане, Белоруссии и на Украине, имеет центр IT-разработки в Литве и офис собственной торговой марки
                в Великобритании. Общая численность компании превышает 5 000 человек.</p>
        </div>
        <div class="about-page_description_article">
            <img class="about-page_description_article_photo" src="catalog/view/theme/default/images/about-page/about-page-photo1.png">
        </div>
        <div class="about-page_description_article">
            <img class="about-page_description_article_photo" src="catalog/view/theme/default/images/about-page/about-page-photo2.png">
        </div>
    </div>
    <form class="about-page_form" id="about-page-message-form" action="<?php echo $action; ?>" method="post"
          enctype="multipart/form-data" class="form-horizontal">
        <h2 class="about-page_form_title">У вас есть вопросы или предложения?</h2>

        <p class="about-page_form_address">Адрес: <?php echo $address; ?></p>
        <p class="about-page_form_address">Телефон: <?php echo $telephone; ?></p>
        <p class="about-page_form_address">Электронная почта: <?php echo $email; ?></p>

        <input class="about-page_form_field js-about-page-message-email" name="email" type="text" placeholder="Ваша электронная почта">
        <input class="about-page_form_field js-about-page-message-text" name="message" type="text" placeholder="Оставьте нам сообщение">
        <?php echo $captcha; ?>
        <button class="about-page_form_btn js-about-page-message-sendbtn" type="submit">Отправить</button>
    </form>


</section>
<script>var pageName = 'AboutPage';</script>

<?php echo $footer; ?>
