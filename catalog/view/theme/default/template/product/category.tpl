<?php echo $header; ?>



<section class="app_content">
    <section class="catalog_page">
        <div class="catalog_breadcrumbs">
            <ul class="breadcrumbs_list">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs_list_item">
                    <a class="breadcrumbs_list_item_link" href="<?php echo $breadcrumb['href']; ?>">
                        <?php echo $breadcrumb['text']; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>

        <div class="catalog_aside">
            <div class="catalog_filtration">
                <ul class="filtration_sections">
                    <li class="filtration_sections_item js-select-filter-btn" open="category">Категории</li>
                    <li class="filtration_sections_item js-select-filter-btn" open="sorting">Фильтрация</li>
                </ul>
            </div>

            <div class="catalog_selected-type">
                <h2 class="selected-type_title"><?php echo $heading_title; ?></h2>

                <div class="selected-type_selection selected-type_selection-popularity">
                    <div class="selected-type_selection_content js-category-select-btn" open="relative">
                        <?php echo $currentSort; ?>
                    </div>
                    <div class="selected-type_dropdown_selection js-category-select-list" list="relative">
                        <ul class="dropdown_selection_list">
                            <?php foreach ($sorts as $sorts) { ?>
                            <li class="dropdown_selection_list_item">
                                <a class="dropdown_selection_list_item_link" href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

            </div>
            <?php echo $column_left; ?>

            <?php echo $content_top; ?>
        </div>
        <div class="catalog_main">
            <div class="catalog_clothes">
                <ul class="clothes_list">
                    <?php foreach ($products as $product) { ?>
                    <?php $class = ''; ?> <!---clothes_list_item-new---->
                    <?php $button = '<button type="button" class="clothes_like" onclick="wishlist.add(' . $product["product_id"] . ');"></button>' ?>
                    <?php if(in_array($product['product_id'], $wishlist_product)) { ?>
                    <?php $class = 'clothes_list_item-liked'; ?>
                    <?php $button = '<button type="button" class="clothes_like" onclick="wishlist.remove(' . $product["product_id"] . ');"></button>'; ?>
                    <?php } ?>

                    <li class="clothes_list_item <?php echo $class; ?> product_<?php echo $product['product_id']; ?>">
                        <div class="clothes_list_item_wrap">
                            <?php echo $button; ?>
                            <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                            <div class="clothes_discount"><p class="clothes_discont_content">75%</p></div>
                            <a href="<?php echo $product['href']; ?>">
                                <img class="clothes_photo" src="<?php echo $product['thumb']; ?>"></a>
                            <p class="clothes_name"><?php echo $product['manufacturer']; ?></p>
                            <p class="clothes_description"><?php echo $product['name']; ?></p>

                            <?php if ($product['price']) { ?>
                                <p class="price">
                                <?php if (!$product['special']) { ?>
                                    <div class="clothes_prices">
                                        <p class="clothes_prices_price"><?php echo $product['price']; ?></p>
                                    </div>
                                <?php } else { ?>
                                    <div class="clothes_prices">
                                        <p class="clothes_prices_newPrice"><?php echo $product['special']; ?></p>
                                        <p class="clothes_prices_price"><?php echo $product['price']; ?></p>
                                    </div>
                                <?php } ?>
                                </p>
                            <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        <?php echo $pagination; ?>
        </div>
    </section>
</section>
<script>var pageName = 'CatalogPage';</script>

<?php echo $footer; ?>
