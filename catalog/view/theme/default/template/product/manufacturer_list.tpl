<?php echo $header; ?>

<?php print $column_left;?>

<section class="app_content">
  <section class="brands-page">
    <div class="catalog_breadcrumbs">
      <ul class="breadcrumbs_list">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="breadcrumbs_list_item"> <a class="breadcrumbs_list_item_link" href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
        <?php } ?>
      </ul>
    </div>
    <div class="brands-page_content"><h2 class="brands-page_content_title">Бренды</h2>
      <div class="brands-page_content_quick-links">
        <ul class="quick-links_list">
        <?php foreach ($categories as $category) { ?>
        &nbsp;&nbsp;&nbsp;<li class="quick-links_list_item"><a class="quick-links_list_item_link" href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>">
              <?php echo $category['name']; ?></a></li>
        <?php } ?>
        </ul>
      </div>

      <div class="brands-page_content_sections">
      <?php foreach ($categories as $category) { ?>
        <ul class="brands-page_content_sections_list">
      <h2 class="brands-page_content_sections_list_title" id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h2>
      <?php if ($category['manufacturer']) { ?>
        <?php foreach ($category['manufacturer'] as $manufacturer) { ?>
          <li class="brands-page_content_sections_list_item"><a
                    class="brands-page_sections_list_item_link" href="#"><?php echo $manufacturer['name']; ?></a></li>
          <!---<?php echo $manufacturer['href']; ?> --->
        <?php } ?>
      <?php } ?>
        </ul>
      <?php } ?>

    </div>
  </section>
</section>
<script>var pageName = 'BrandsPage';</script>

<?php echo $footer; ?>