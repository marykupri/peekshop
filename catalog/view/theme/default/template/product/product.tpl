<?php echo $header; ?>

<section class="app_content">
    <section class="product-view_page">
        <!--- <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table">
                <h2 class="modal_window_title modal_window_title-table-size">Размеры женской верхней одежды</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ГРУДИ <span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">78-82
                                <div class="line_sub-list_item_allotment-block"></div>
                            </li>
                            <li class="line_sub-list_item">82-86</li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">90-94</li>
                            <li class="line_sub-list_item">94-98</li>
                            <li class="line_sub-list_item">98-102</li>
                            <li class="line_sub-list_item">102-106</li>
                            <li class="line_sub-list_item">106-110</li>
                            <li class="line_sub-list_item">110-114</li>
                            <li class="line_sub-list_item">114-118</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">58-62</li>
                            <li class="line_sub-list_item">62-66</li>
                            <li class="line_sub-list_item">66-70</li>
                            <li class="line_sub-list_item">70-74</li>
                            <li class="line_sub-list_item">74-78</li>
                            <li class="line_sub-list_item">78-82</li>
                            <li class="line_sub-list_item">82-86</li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">90-94</li>
                            <li class="line_sub-list_item">94-98</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ БЕДЕР<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">84-88</li>
                            <li class="line_sub-list_item">88-92</li>
                            <li class="line_sub-list_item">92-96</li>
                            <li class="line_sub-list_item">96-100</li>
                            <li class="line_sub-list_item">100-104</li>
                            <li class="line_sub-list_item">104-108</li>
                            <li class="line_sub-list_item">108-112</li>
                            <li class="line_sub-list_item">112-116</li>
                            <li class="line_sub-list_item">116-120</li>
                            <li class="line_sub-list_item">120-124</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА / ГЕРМАНИЯ<span
                                        class="line_sub-list_item_value">EUR / GER</span></li>
                            <li class="line_sub-list_item">32</li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ФРАНЦИЯ<span
                                        class="line_sub-list_item_value">FR </span></li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ИТАЛИЯ<span
                                        class="line_sub-list_item_value">IT</span></li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">США<span
                                        class="line_sub-list_item_value">US</span></li>
                            <li class="line_sub-list_item">0</li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">14</li>
                            <li class="line_sub-list_item">16</li>
                            <li class="line_sub-list_item">18</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">UK</span></li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">14</li>
                            <li class="line_sub-list_item">16</li>
                            <li class="line_sub-list_item">18</li>
                            <li class="line_sub-list_item">20</li>
                            <li class="line_sub-list_item">22</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS/XS</li>
                            <li class="line_sub-list_item">XS/S</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">L/XL</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">Европа<span
                                        class="line_sub-list_item_value">EUR2</span></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item">0</li>
                            <li class="line_sub-list_item">1</li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">3</li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">Международный<span
                                        class="line_sub-list_item_value">BM</span></li>
                            <li class="line_sub-list_item">T1</li>
                            <li class="line_sub-list_item">T1</li>
                            <li class="line_sub-list_item">T2</li>
                            <li class="line_sub-list_item">T2</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table">
                <h2 class="modal_window_title modal_window_title-table-size">Размеры женских брюк и джинс</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">58-62</li>
                            <li class="line_sub-list_item">62-66</li>
                            <li class="line_sub-list_item">66-70</li>
                            <li class="line_sub-list_item">70-74</li>
                            <li class="line_sub-list_item">74-78</li>
                            <li class="line_sub-list_item">78-82</li>
                            <li class="line_sub-list_item">82-86</li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">90-94</li>
                            <li class="line_sub-list_item">94-98</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ БЕДЕР<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">84-88</li>
                            <li class="line_sub-list_item">88-92</li>
                            <li class="line_sub-list_item">92-96</li>
                            <li class="line_sub-list_item">96-100</li>
                            <li class="line_sub-list_item">100-104</li>
                            <li class="line_sub-list_item">104-108</li>
                            <li class="line_sub-list_item">108-112</li>
                            <li class="line_sub-list_item">112-116</li>
                            <li class="line_sub-list_item">116-120</li>
                            <li class="line_sub-list_item">120-124</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА / ГЕРМАНИЯ<span
                                        class="line_sub-list_item_value">EUR / GER</span></li>
                            <li class="line_sub-list_item">32</li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ФРАНЦИЯ<span
                                        class="line_sub-list_item_value">FR </span></li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ИТАЛИЯ<span
                                        class="line_sub-list_item_value">IT</span></li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">США<span
                                        class="line_sub-list_item_value">US</span></li>
                            <li class="line_sub-list_item">0</li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">14</li>
                            <li class="line_sub-list_item">16</li>
                            <li class="line_sub-list_item">18</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">UK</span></li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">14</li>
                            <li class="line_sub-list_item">16</li>
                            <li class="line_sub-list_item">18</li>
                            <li class="line_sub-list_item">20</li>
                            <li class="line_sub-list_item">22</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS/XS</li>
                            <li class="line_sub-list_item">XS/S</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">L/XL</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">Европа<span
                                        class="line_sub-list_item_value">EUR2</span></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item">0</li>
                            <li class="line_sub-list_item">1</li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">3</li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">Международный<span
                                        class="line_sub-list_item_value">BM</span></li>
                            <li class="line_sub-list_item">T1</li>
                            <li class="line_sub-list_item">T1</li>
                            <li class="line_sub-list_item">T2</li>
                            <li class="line_sub-list_item">T2</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Женские джинсы</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line size-table_list_line-columns-1">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">58-59</li>
                            <li class="line_sub-list_item">60-62</li>
                            <li class="line_sub-list_item">63-64</li>
                            <li class="line_sub-list_item">65-67</li>
                            <li class="line_sub-list_item">68-69</li>
                            <li class="line_sub-list_item">70-72</li>
                            <li class="line_sub-list_item">73-74</li>
                            <li class="line_sub-list_item">75-77</li>
                            <li class="line_sub-list_item">78-80</li>
                            <li class="line_sub-list_item">81-82</li>
                            <li class="line_sub-list_item">83-85</li>
                            <li class="line_sub-list_item">86-87</li>
                            <li class="line_sub-list_item">88-90</li>
                            <li class="line_sub-list_item">91-92</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-1">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell">РОСТ<span
                                            class="line_sub-list_item_value">СМ</span></div>
                                <div class="line_sub-list_item_cell">ДЮЙМЫ</div>
                            </li>
                            <li class="line_sub-list_item">W23</li>
                            <li class="line_sub-list_item">W24</li>
                            <li class="line_sub-list_item">W25</li>
                            <li class="line_sub-list_item">W26</li>
                            <li class="line_sub-list_item">W27</li>
                            <li class="line_sub-list_item">W28</li>
                            <li class="line_sub-list_item">W29</li>
                            <li class="line_sub-list_item">W30</li>
                            <li class="line_sub-list_item">W31</li>
                            <li class="line_sub-list_item">W32</li>
                            <li class="line_sub-list_item">W33</li>
                            <li class="line_sub-list_item">W34</li>
                            <li class="line_sub-list_item">W35</li>
                            <li class="line_sub-list_item">W36</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-1">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">150-157
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L28</div>
                            </li>
                            <li class="line_sub-list_item">23/28</li>
                            <li class="line_sub-list_item">24/28</li>
                            <li class="line_sub-list_item">25/28</li>
                            <li class="line_sub-list_item">26/28</li>
                            <li class="line_sub-list_item">27/28</li>
                            <li class="line_sub-list_item">28/28</li>
                            <li class="line_sub-list_item">29/28</li>
                            <li class="line_sub-list_item">30/28</li>
                            <li class="line_sub-list_item">31/28</li>
                            <li class="line_sub-list_item">32/28</li>
                            <li class="line_sub-list_item">33/28</li>
                            <li class="line_sub-list_item">34/28</li>
                            <li class="line_sub-list_item">35/28</li>
                            <li class="line_sub-list_item">36/28</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-1">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">158-164
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L30</div>
                            </li>
                            <li class="line_sub-list_item">23/30</li>
                            <li class="line_sub-list_item">24/30</li>
                            <li class="line_sub-list_item">25/30</li>
                            <li class="line_sub-list_item">26/30</li>
                            <li class="line_sub-list_item">27/30</li>
                            <li class="line_sub-list_item">28/30</li>
                            <li class="line_sub-list_item">29/30</li>
                            <li class="line_sub-list_item">30/30</li>
                            <li class="line_sub-list_item">31/30</li>
                            <li class="line_sub-list_item">32/30</li>
                            <li class="line_sub-list_item">33/30</li>
                            <li class="line_sub-list_item">34/30</li>
                            <li class="line_sub-list_item">35/30</li>
                            <li class="line_sub-list_item">36/30</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-1">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">165-177
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L32</div>
                            </li>
                            <li class="line_sub-list_item">23/32</li>
                            <li class="line_sub-list_item">24/32</li>
                            <li class="line_sub-list_item">25/32</li>
                            <li class="line_sub-list_item">26/32</li>
                            <li class="line_sub-list_item">27/32</li>
                            <li class="line_sub-list_item">28/32</li>
                            <li class="line_sub-list_item">29/32</li>
                            <li class="line_sub-list_item">30/32</li>
                            <li class="line_sub-list_item">31/32</li>
                            <li class="line_sub-list_item">32/32</li>
                            <li class="line_sub-list_item">33/32</li>
                            <li class="line_sub-list_item">34/32</li>
                            <li class="line_sub-list_item">35/32</li>
                            <li class="line_sub-list_item">36/32</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-1">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">178-185
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L34</div>
                            </li>
                            <li class="line_sub-list_item">23/34</li>
                            <li class="line_sub-list_item">24/34</li>
                            <li class="line_sub-list_item">25/34</li>
                            <li class="line_sub-list_item">26/34</li>
                            <li class="line_sub-list_item">27/34</li>
                            <li class="line_sub-list_item">28/34</li>
                            <li class="line_sub-list_item">29/34</li>
                            <li class="line_sub-list_item">30/34</li>
                            <li class="line_sub-list_item">31/34</li>
                            <li class="line_sub-list_item">32/34</li>
                            <li class="line_sub-list_item">33/34</li>
                            <li class="line_sub-list_item">34/34</li>
                            <li class="line_sub-list_item">35/34</li>
                            <li class="line_sub-list_item">36/34</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-1">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">> 185</div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L36</div>
                            </li>
                            <li class="line_sub-list_item">23/36</li>
                            <li class="line_sub-list_item">24/36</li>
                            <li class="line_sub-list_item">25/36</li>
                            <li class="line_sub-list_item">26/36</li>
                            <li class="line_sub-list_item">27/36</li>
                            <li class="line_sub-list_item">28/36</li>
                            <li class="line_sub-list_item">29/36</li>
                            <li class="line_sub-list_item">30/36</li>
                            <li class="line_sub-list_item">31/36</li>
                            <li class="line_sub-list_item">32/36</li>
                            <li class="line_sub-list_item">33/36</li>
                            <li class="line_sub-list_item">34/36</li>
                            <li class="line_sub-list_item">35/36</li>
                            <li class="line_sub-list_item">36/36</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table">
                <h2 class="modal_window_title modal_window_title-table-size">Размеры женского нижнего белья</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">58-62</li>
                            <li class="line_sub-list_item">62-66</li>
                            <li class="line_sub-list_item">66-70</li>
                            <li class="line_sub-list_item">70-74</li>
                            <li class="line_sub-list_item">74-78</li>
                            <li class="line_sub-list_item">78-82</li>
                            <li class="line_sub-list_item">82-86</li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">90-94</li>
                            <li class="line_sub-list_item">94-98</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ БЕДЕР<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">84-88</li>
                            <li class="line_sub-list_item">88-92</li>
                            <li class="line_sub-list_item">92-96</li>
                            <li class="line_sub-list_item">96-100</li>
                            <li class="line_sub-list_item">100-104</li>
                            <li class="line_sub-list_item">104-108</li>
                            <li class="line_sub-list_item">108-112</li>
                            <li class="line_sub-list_item">112-116</li>
                            <li class="line_sub-list_item">116-120</li>
                            <li class="line_sub-list_item">120-124</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА / ГЕРМАНИЯ<span
                                        class="line_sub-list_item_value">EUR / GER</span></li>
                            <li class="line_sub-list_item">32</li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ФРАНЦИЯ<span
                                        class="line_sub-list_item_value">FR </span></li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ИТАЛИЯ<span
                                        class="line_sub-list_item_value">IT</span></li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">США<span
                                        class="line_sub-list_item_value">US</span></li>
                            <li class="line_sub-list_item">0</li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">14</li>
                            <li class="line_sub-list_item">16</li>
                            <li class="line_sub-list_item">18</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">UK</span></li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">14</li>
                            <li class="line_sub-list_item">16</li>
                            <li class="line_sub-list_item">18</li>
                            <li class="line_sub-list_item">20</li>
                            <li class="line_sub-list_item">22</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS/XS</li>
                            <li class="line_sub-list_item">XS/S</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">L/XL</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">Европа<span
                                        class="line_sub-list_item_value">EUR2</span></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item">0</li>
                            <li class="line_sub-list_item">1</li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">3</li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">Международный<span
                                        class="line_sub-list_item_value">BM</span></li>
                            <li class="line_sub-list_item">T1</li>
                            <li class="line_sub-list_item">T1</li>
                            <li class="line_sub-list_item">T2</li>
                            <li class="line_sub-list_item">T2</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item">T3</li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                            <li class="line_sub-list_item"></li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Бюстгалтеры</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells">
                            <li class="line_sub-list_item-other-item">Обхват под грудью</li>
                            <li class="line_sub-list_item-other-item">RUS / EUR</li>
                            <li class="line_sub-list_item-other-item">UK/US</li>
                            <li class="line_sub-list_item-other-item">FR</li>
                            <li class="line_sub-list_item-other-item">IT</li>
                            <li class="line_sub-list_item-other-item">Обхват груди, см</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item"></li>
                            <li class="line_sub-list_item-other-item"></li>
                            <li class="line_sub-list_item-other-item"></li>
                            <li class="line_sub-list_item-other-item"></li>
                            <li class="line_sub-list_item-other-item"></li>
                            <li class="line_sub-list_item-other-item">Чашка А</li>
                            <li class="line_sub-list_item-other-item">Чашка B</li>
                            <li class="line_sub-list_item-other-item">Чашка C</li>
                            <li class="line_sub-list_item-other-item">Чашка D</li>
                            <li class="line_sub-list_item-other-item">Чашка E</li>
                            <li class="line_sub-list_item-other-item">Чашка F</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">63-67</li>
                            <li class="line_sub-list_item-other-item">65</li>
                            <li class="line_sub-list_item-other-item">30</li>
                            <li class="line_sub-list_item-other-item">80</li>
                            <li class="line_sub-list_item-other-item">0</li>
                            <li class="line_sub-list_item-other-item">77-79</li>
                            <li class="line_sub-list_item-other-item">79-81</li>
                            <li class="line_sub-list_item-other-item">-</li>
                            <li class="line_sub-list_item-other-item">-</li>
                            <li class="line_sub-list_item-other-item">-</li>
                            <li class="line_sub-list_item-other-item">-</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">68-72</li>
                            <li class="line_sub-list_item-other-item">70</li>
                            <li class="line_sub-list_item-other-item">32</li>
                            <li class="line_sub-list_item-other-item">85</li>
                            <li class="line_sub-list_item-other-item">1</li>
                            <li class="line_sub-list_item-other-item">82-84</li>
                            <li class="line_sub-list_item-other-item">84-86</li>
                            <li class="line_sub-list_item-other-item">86-88</li>
                            <li class="line_sub-list_item-other-item">-</li>
                            <li class="line_sub-list_item-other-item">-</li>
                            <li class="line_sub-list_item-other-item">-</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">73-77</li>
                            <li class="line_sub-list_item-other-item">75</li>
                            <li class="line_sub-list_item-other-item">34</li>
                            <li class="line_sub-list_item-other-item">90</li>
                            <li class="line_sub-list_item-other-item">2</li>
                            <li class="line_sub-list_item-other-item">87-89</li>
                            <li class="line_sub-list_item-other-item">89-91</li>
                            <li class="line_sub-list_item-other-item">91-93</li>
                            <li class="line_sub-list_item-other-item">93-95</li>
                            <li class="line_sub-list_item-other-item">95-97</li>
                            <li class="line_sub-list_item-other-item">97-99</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">78-82</li>
                            <li class="line_sub-list_item-other-item">80</li>
                            <li class="line_sub-list_item-other-item">36</li>
                            <li class="line_sub-list_item-other-item">95</li>
                            <li class="line_sub-list_item-other-item">3</li>
                            <li class="line_sub-list_item-other-item">92-94</li>
                            <li class="line_sub-list_item-other-item">94-96</li>
                            <li class="line_sub-list_item-other-item">96-98</li>
                            <li class="line_sub-list_item-other-item">98-100</li>
                            <li class="line_sub-list_item-other-item">100-102</li>
                            <li class="line_sub-list_item-other-item">102-104</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">83-87</li>
                            <li class="line_sub-list_item-other-item">85</li>
                            <li class="line_sub-list_item-other-item">38</li>
                            <li class="line_sub-list_item-other-item">100</li>
                            <li class="line_sub-list_item-other-item">4</li>
                            <li class="line_sub-list_item-other-item">97-99</li>
                            <li class="line_sub-list_item-other-item">99-101</li>
                            <li class="line_sub-list_item-other-item">101-103</li>
                            <li class="line_sub-list_item-other-item">103-105</li>
                            <li class="line_sub-list_item-other-item">105-107</li>
                            <li class="line_sub-list_item-other-item">107-109</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">88-92</li>
                            <li class="line_sub-list_item-other-item">90</li>
                            <li class="line_sub-list_item-other-item">40</li>
                            <li class="line_sub-list_item-other-item">105</li>
                            <li class="line_sub-list_item-other-item">5</li>
                            <li class="line_sub-list_item-other-item">102-104</li>
                            <li class="line_sub-list_item-other-item">104-106</li>
                            <li class="line_sub-list_item-other-item">106-108</li>
                            <li class="line_sub-list_item-other-item">108-110</li>
                            <li class="line_sub-list_item-other-item">110-112</li>
                            <li class="line_sub-list_item-other-item">112-114</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">93-97</li>
                            <li class="line_sub-list_item-other-item">95</li>
                            <li class="line_sub-list_item-other-item">42</li>
                            <li class="line_sub-list_item-other-item">110</li>
                            <li class="line_sub-list_item-other-item">6</li>
                            <li class="line_sub-list_item-other-item">107-109</li>
                            <li class="line_sub-list_item-other-item">109-111</li>
                            <li class="line_sub-list_item-other-item">111-113</li>
                            <li class="line_sub-list_item-other-item">113-115</li>
                            <li class="line_sub-list_item-other-item">115-117</li>
                            <li class="line_sub-list_item-other-item">117-119</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-other-line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-2">
                            <li class="line_sub-list_item-other-item">98-102</li>
                            <li class="line_sub-list_item-other-item">100</li>
                            <li class="line_sub-list_item-other-item">44</li>
                            <li class="line_sub-list_item-other-item">115</li>
                            <li class="line_sub-list_item-other-item">7</li>
                            <li class="line_sub-list_item-other-item">112-114</li>
                            <li class="line_sub-list_item-other-item">114-116</li>
                            <li class="line_sub-list_item-other-item">116-118</li>
                            <li class="line_sub-list_item-other-item">118-120</li>
                            <li class="line_sub-list_item-other-item">120-122</li>
                            <li class="line_sub-list_item-other-item">122-124</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table">
                <h2 class="modal_window_title modal_window_title-table-size">Размеры женской обуви</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">РАЗМЕР СТОПЫ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">21</li>
                            <li class="line_sub-list_item">21,5</li>
                            <li class="line_sub-list_item">22,5</li>
                            <li class="line_sub-list_item">23</li>
                            <li class="line_sub-list_item">23,5</li>
                            <li class="line_sub-list_item">24,5</li>
                            <li class="line_sub-list_item">25</li>
                            <li class="line_sub-list_item">25,5</li>
                            <li class="line_sub-list_item">26,5</li>
                            <li class="line_sub-list_item">27</li>
                            <li class="line_sub-list_item">27,5</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item">33</li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">35</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">37</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">39</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">41</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">43</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА<span
                                        class="line_sub-list_item_value">EUR</span></li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">35</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">37</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">39</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">41</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">43</li>
                            <li class="line_sub-list_item">44</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">UK</span></li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">2.5</li>
                            <li class="line_sub-list_item">3.5</li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">5</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">6.5</li>
                            <li class="line_sub-list_item">7.5</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">9</li>
                            <li class="line_sub-list_item">9.5</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">США<span
                                        class="line_sub-list_item_value">US</span></li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">4.5</li>
                            <li class="line_sub-list_item">5.5</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">7</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">8.5</li>
                            <li class="line_sub-list_item">9.5</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">11</li>
                            <li class="line_sub-list_item">11.5</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table"><h2 class="modal_window_title modal_window_title-table-size">Размеры женской
                    одежды: носки и чулки</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line size-table_list_line-columns-2">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">РАЗМЕР СТОПЫ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">20,4-21,8</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">21,8-23,8</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">23,8-25,9</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">25,9-28,0</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">28,0-30,5</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">30,5-32,5</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">32,5-35,0</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-2">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА<span
                                        class="line_sub-list_item_value">EUR</span></li>
                            <li class="line_sub-list_item">35</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">37</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">39</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">41</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">43</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">45</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">47</li>
                            <li class="line_sub-list_item">48</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-2">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">21</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">23</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">25</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">27</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">29</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">31</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">33</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-2">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">S</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">S</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">M</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">L</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">XL</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">XXL</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">XXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-2">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">США/ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">US/UK</span></li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">7</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">8</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">9</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">10</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">11</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">12</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">13</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table"><h2 class="modal_window_title modal_window_title-table-size">Размеры женских
                    аксессуаров</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ЛАДОНИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">15,2</li>
                            <li class="line_sub-list_item">16,5</li>
                            <li class="line_sub-list_item">17,8</li>
                            <li class="line_sub-list_item">19</li>
                            <li class="line_sub-list_item">20,3</li>
                            <li class="line_sub-list_item">21,6</li>
                            <li class="line_sub-list_item">22,9</li>
                            <li class="line_sub-list_item">24</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS</li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">UK</span></li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">6,5</li>
                            <li class="line_sub-list_item">7</li>
                            <li class="line_sub-list_item">7,5</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">8,5</li>
                            <li class="line_sub-list_item">9</li>
                            <li class="line_sub-list_item">9,5</li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Женские головные уборы</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ГОЛОВЫ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">55</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">57</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">59</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">61</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS</li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА<span
                                        class="line_sub-list_item_value">EUR</span></li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">55</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">57</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">59</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">61</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ/США<span
                                        class="line_sub-list_item_value">UK/US</span></li>
                            <li class="line_sub-list_item">6,75</li>
                            <li class="line_sub-list_item">6,875</li>
                            <li class="line_sub-list_item">7</li>
                            <li class="line_sub-list_item">7,125</li>
                            <li class="line_sub-list_item">7,25</li>
                            <li class="line_sub-list_item">7,375</li>
                            <li class="line_sub-list_item">7,5</li>
                            <li class="line_sub-list_item">7,625</li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Женские ремни</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ДЛИНА<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">60 - 65</li>
                            <li class="line_sub-list_item">70 - 75</li>
                            <li class="line_sub-list_item">80 - 85</li>
                            <li class="line_sub-list_item">90 - 95</li>
                            <li class="line_sub-list_item">100</li>
                            <li class="line_sub-list_item">105</li>
                            <li class="line_sub-list_item">110</li>
                            <li class="line_sub-list_item">115</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS</li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table"><h2 class="modal_window_title modal_window_title-table-size">Размеры мужской
                    верхней одежды</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ГРУДИ <span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">90-94</li>
                            <li class="line_sub-list_item">94-98</li>
                            <li class="line_sub-list_item">98-102</li>
                            <li class="line_sub-list_item">102-106</li>
                            <li class="line_sub-list_item">106-110</li>
                            <li class="line_sub-list_item">110-114</li>
                            <li class="line_sub-list_item">114-118</li>
                            <li class="line_sub-list_item">118-122</li>
                            <li class="line_sub-list_item">122-126</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">74-78</li>
                            <li class="line_sub-list_item">78-82</li>
                            <li class="line_sub-list_item">82-86</li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">92-94</li>
                            <li class="line_sub-list_item">96-100</li>
                            <li class="line_sub-list_item">106-110</li>
                            <li class="line_sub-list_item">110-114</li>
                            <li class="line_sub-list_item">114-118</li>
                            <li class="line_sub-list_item">118-122</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">62</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА<span
                                        class="line_sub-list_item_value">EUR / GER / FR / IT</span></li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">62</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ГЕРМАНИЯ/ВЫСОКИЙ РОСТ<span
                                        class="line_sub-list_item_value">GER</span></li>
                            <li class="line_sub-list_item">86</li>
                            <li class="line_sub-list_item">90</li>
                            <li class="line_sub-list_item">94</li>
                            <li class="line_sub-list_item">98</li>
                            <li class="line_sub-list_item">102</li>
                            <li class="line_sub-list_item">106</li>
                            <li class="line_sub-list_item">110</li>
                            <li class="line_sub-list_item">114</li>
                            <li class="line_sub-list_item">118</li>
                            <li class="line_sub-list_item">122</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">США/ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">US/UK</span></li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XS/S</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">L/XL</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Мужские сорочки: рубашки</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line size-table_list_line-columns-3">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ШЕИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">37</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">39</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">41</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">43</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">45</li>
                            <li class="line_sub-list_item">46</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-3">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">US/UK</li>
                            <li class="line_sub-list_item">14</li>
                            <li class="line_sub-list_item">14,5</li>
                            <li class="line_sub-list_item">15</li>
                            <li class="line_sub-list_item">15,5</li>
                            <li class="line_sub-list_item">15,5</li>
                            <li class="line_sub-list_item">16</li>
                            <li class="line_sub-list_item">16,5</li>
                            <li class="line_sub-list_item">17</li>
                            <li class="line_sub-list_item">17,5</li>
                            <li class="line_sub-list_item">17,5</li>
                            <li class="line_sub-list_item">18</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-3">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">S</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">M</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">L</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">XL</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">XXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-3">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">РАЗМЕР РУБАШКИ</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">46-48</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">48-50</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">50-52</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">54-56</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">56-58</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table">
                <h2 class="modal_window_title modal_window_title-table-size">Размеры мужских брюк и джинс</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">74-78</li>
                            <li class="line_sub-list_item">78-82</li>
                            <li class="line_sub-list_item">82-86</li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">92-94</li>
                            <li class="line_sub-list_item">96-100</li>
                            <li class="line_sub-list_item">106-110</li>
                            <li class="line_sub-list_item">110-114</li>
                            <li class="line_sub-list_item">114-118</li>
                            <li class="line_sub-list_item">118-122</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ БЕДЕР<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">94-97</li>
                            <li class="line_sub-list_item">97-100</li>
                            <li class="line_sub-list_item">100-103</li>
                            <li class="line_sub-list_item">103-106</li>
                            <li class="line_sub-list_item">106-109</li>
                            <li class="line_sub-list_item">109-112</li>
                            <li class="line_sub-list_item">112-115</li>
                            <li class="line_sub-list_item">115-118</li>
                            <li class="line_sub-list_item">118-121</li>
                            <li class="line_sub-list_item">121-124</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS   </span></li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">62</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА<span
                                        class="line_sub-list_item_value">EUR / GER / FR / IT</span></li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">62</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ГЕРМАНИЯ ВЫСОКИЙ РОСТ<span
                                        class="line_sub-list_item_value">GER</span></li>
                            <li class="line_sub-list_item">86</li>
                            <li class="line_sub-list_item">90</li>
                            <li class="line_sub-list_item">94</li>
                            <li class="line_sub-list_item">98</li>
                            <li class="line_sub-list_item">102</li>
                            <li class="line_sub-list_item">106</li>
                            <li class="line_sub-list_item">110</li>
                            <li class="line_sub-list_item">114</li>
                            <li class="line_sub-list_item">118</li>
                            <li class="line_sub-list_item">122</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">США / ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">US / UK</span></li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS/XS</li>
                            <li class="line_sub-list_item">XS/S</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">L/XL</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Мужские джинсы</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line size-table_list_line-columns-4">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">70-72</li>
                            <li class="line_sub-list_item">73-74</li>
                            <li class="line_sub-list_item">75-77</li>
                            <li class="line_sub-list_item">78-79</li>
                            <li class="line_sub-list_item">80-82</li>
                            <li class="line_sub-list_item">83-84</li>
                            <li class="line_sub-list_item">85-87</li>
                            <li class="line_sub-list_item">88-89</li>
                            <li class="line_sub-list_item">90-94</li>
                            <li class="line_sub-list_item">95-98</li>
                            <li class="line_sub-list_item">99-103</li>
                            <li class="line_sub-list_item">104-108</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-4">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell">РОСТ<span
                                            class="line_sub-list_item_value">СМ</span></div>
                                <div class="line_sub-list_item_cell">ДЮЙМЫ</div>
                            </li>
                            <li class="line_sub-list_item">28</li>
                            <li class="line_sub-list_item">29</li>
                            <li class="line_sub-list_item">30</li>
                            <li class="line_sub-list_item">31</li>
                            <li class="line_sub-list_item">32</li>
                            <li class="line_sub-list_item">33</li>
                            <li class="line_sub-list_item">34</li>
                            <li class="line_sub-list_item">35</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">42</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-4">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">150-157
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L28</div>
                            </li>
                            <li class="line_sub-list_item">28/28</li>
                            <li class="line_sub-list_item">29/28</li>
                            <li class="line_sub-list_item">30/28</li>
                            <li class="line_sub-list_item">31/28</li>
                            <li class="line_sub-list_item">32/28</li>
                            <li class="line_sub-list_item">33/28</li>
                            <li class="line_sub-list_item">34/28</li>
                            <li class="line_sub-list_item">35/28</li>
                            <li class="line_sub-list_item">36/28</li>
                            <li class="line_sub-list_item">38/28</li>
                            <li class="line_sub-list_item">40/28</li>
                            <li class="line_sub-list_item">42/28</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-4">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">158-164
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L30</div>
                            </li>
                            <li class="line_sub-list_item">28/30</li>
                            <li class="line_sub-list_item">29/30</li>
                            <li class="line_sub-list_item">30/30</li>
                            <li class="line_sub-list_item">31/30</li>
                            <li class="line_sub-list_item">32/30</li>
                            <li class="line_sub-list_item">33/30</li>
                            <li class="line_sub-list_item">34/30</li>
                            <li class="line_sub-list_item">35/30</li>
                            <li class="line_sub-list_item">36/30</li>
                            <li class="line_sub-list_item">38/30</li>
                            <li class="line_sub-list_item">40/30</li>
                            <li class="line_sub-list_item">42/30</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-4">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">165-177
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L32</div>
                            </li>
                            <li class="line_sub-list_item">28/32</li>
                            <li class="line_sub-list_item">29/32</li>
                            <li class="line_sub-list_item">30/32</li>
                            <li class="line_sub-list_item">31/32</li>
                            <li class="line_sub-list_item">32/32</li>
                            <li class="line_sub-list_item">33/32</li>
                            <li class="line_sub-list_item">34/32</li>
                            <li class="line_sub-list_item">35/32</li>
                            <li class="line_sub-list_item">36/32</li>
                            <li class="line_sub-list_item">38/32</li>
                            <li class="line_sub-list_item">40/32</li>
                            <li class="line_sub-list_item">42/32</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-4">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">178-185
                                </div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L34</div>
                            </li>
                            <li class="line_sub-list_item">28/34</li>
                            <li class="line_sub-list_item">29/34</li>
                            <li class="line_sub-list_item">30/34</li>
                            <li class="line_sub-list_item">31/34</li>
                            <li class="line_sub-list_item">32/34</li>
                            <li class="line_sub-list_item">33/34</li>
                            <li class="line_sub-list_item">34/34</li>
                            <li class="line_sub-list_item">35/34</li>
                            <li class="line_sub-list_item">36/34</li>
                            <li class="line_sub-list_item">38/34</li>
                            <li class="line_sub-list_item">40/34</li>
                            <li class="line_sub-list_item">42/34</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line size-table_list_line-columns-4">
                        <ul class="size-table_list_line_sub-list">
                            <li class="line_sub-list_item line_sub-list_item-title line_sub-list_item--double">
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">> 185</div>
                                <div class="line_sub-list_item_cell line_sub-list_item_cell--other-font">L36</div>
                            </li>
                            <li class="line_sub-list_item">28/36</li>
                            <li class="line_sub-list_item">29/36</li>
                            <li class="line_sub-list_item">30/36</li>
                            <li class="line_sub-list_item">31/36</li>
                            <li class="line_sub-list_item">32/36</li>
                            <li class="line_sub-list_item">33/36</li>
                            <li class="line_sub-list_item">34/36</li>
                            <li class="line_sub-list_item">35/36</li>
                            <li class="line_sub-list_item">36/36</li>
                            <li class="line_sub-list_item">38/36</li>
                            <li class="line_sub-list_item">40/36</li>
                            <li class="line_sub-list_item">42/36</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table"><h2 class="modal_window_title modal_window_title-table-size">Размеры мужской
                    обуви</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">РАЗМЕР СТОПЫ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">24.5</li>
                            <li class="line_sub-list_item">25</li>
                            <li class="line_sub-list_item">25.5</li>
                            <li class="line_sub-list_item">26.5</li>
                            <li class="line_sub-list_item">27</li>
                            <li class="line_sub-list_item">27.5</li>
                            <li class="line_sub-list_item">28.5</li>
                            <li class="line_sub-list_item">29</li>
                            <li class="line_sub-list_item">29.5</li>
                            <li class="line_sub-list_item">30.5</li>
                            <li class="line_sub-list_item">31</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS   </span></li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">39</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">41</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">43</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">45</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">47</li>
                            <li class="line_sub-list_item">48</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА<span
                                        class="line_sub-list_item_value">EUR</span></li>
                            <li class="line_sub-list_item">39</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">41</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">43</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">45</li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">47</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">49</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">UK</span></li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">6.5</li>
                            <li class="line_sub-list_item">7.5</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">9</li>
                            <li class="line_sub-list_item">9.5</li>
                            <li class="line_sub-list_item">10.5</li>
                            <li class="line_sub-list_item">11.5</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">13</li>
                            <li class="line_sub-list_item">13.5</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-3">
                            <li class="line_sub-list_item line_sub-list_item-title">США<span
                                        class="line_sub-list_item_value">US</span></li>
                            <li class="line_sub-list_item">6.5</li>
                            <li class="line_sub-list_item">7</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">8.5</li>
                            <li class="line_sub-list_item">9.5</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">11</li>
                            <li class="line_sub-list_item">12</li>
                            <li class="line_sub-list_item">12.5</li>
                            <li class="line_sub-list_item">13.5</li>
                            <li class="line_sub-list_item">14</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table"><h2 class="modal_window_title modal_window_title-table-size">Размеры мужских
                    аксессуаров</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ЛАДОНИ<span
                                        class="line_sub-list_item_value">СМ    </span></li>
                            <li class="line_sub-list_item">17,8</li>
                            <li class="line_sub-list_item">19</li>
                            <li class="line_sub-list_item">20,3</li>
                            <li class="line_sub-list_item">21,6</li>
                            <li class="line_sub-list_item">22,9</li>
                            <li class="line_sub-list_item">24</li>
                            <li class="line_sub-list_item">25,4</li>
                            <li class="line_sub-list_item">26,7-27,9</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS</li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ<span
                                        class="line_sub-list_item_value">UK</span></li>
                            <li class="line_sub-list_item">7</li>
                            <li class="line_sub-list_item">7,5</li>
                            <li class="line_sub-list_item">8</li>
                            <li class="line_sub-list_item">8,5</li>
                            <li class="line_sub-list_item">9</li>
                            <li class="line_sub-list_item">9,5</li>
                            <li class="line_sub-list_item">10</li>
                            <li class="line_sub-list_item">10,5-11</li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Мужские головные уборы</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ГОЛОВЫ<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">55</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">57</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">59</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">61</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS</li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА<span
                                        class="line_sub-list_item_value">EUR</span></li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">55</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">57</li>
                            <li class="line_sub-list_item">58</li>
                            <li class="line_sub-list_item">59</li>
                            <li class="line_sub-list_item">60</li>
                            <li class="line_sub-list_item">61</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ВЕЛИКОБРИТАНИЯ/США<span
                                        class="line_sub-list_item_value">UK/US</span></li>
                            <li class="line_sub-list_item">6,75</li>
                            <li class="line_sub-list_item">6,875</li>
                            <li class="line_sub-list_item">7</li>
                            <li class="line_sub-list_item">7,125</li>
                            <li class="line_sub-list_item">7,25</li>
                            <li class="line_sub-list_item">7,375</li>
                            <li class="line_sub-list_item">7,5</li>
                            <li class="line_sub-list_item">7,625</li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Мужские ремни</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">ДЛИНА<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">75</li>
                            <li class="line_sub-list_item">80</li>
                            <li class="line_sub-list_item">85</li>
                            <li class="line_sub-list_item">90-95</li>
                            <li class="line_sub-list_item">100</li>
                            <li class="line_sub-list_item">105</li>
                            <li class="line_sub-list_item">110-115</li>
                            <li class="line_sub-list_item">120</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-4">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XXS</li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table"><h2 class="modal_window_title modal_window_title-table-size">Размеры мужского
                    белья</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-5">
                            <li class="line_sub-list_item line_sub-list_item-title">ОБХВАТ ТАЛИИ<span
                                        class="line_sub-list_item_value">СМ    </span></li>
                            <li class="line_sub-list_item">78-82</li>
                            <li class="line_sub-list_item">82-86</li>
                            <li class="line_sub-list_item">86-90</li>
                            <li class="line_sub-list_item">92-94</li>
                            <li class="line_sub-list_item">96-100</li>
                            <li class="line_sub-list_item">102-106</li>
                            <li class="line_sub-list_item">106-110</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-5">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item">46</li>
                            <li class="line_sub-list_item">48</li>
                            <li class="line_sub-list_item">50</li>
                            <li class="line_sub-list_item">52</li>
                            <li class="line_sub-list_item">54</li>
                            <li class="line_sub-list_item">56</li>
                            <li class="line_sub-list_item">58</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-5">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАРОДНЫЙ<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item">XS</li>
                            <li class="line_sub-list_item">S</li>
                            <li class="line_sub-list_item">M</li>
                            <li class="line_sub-list_item">L</li>
                            <li class="line_sub-list_item">XL</li>
                            <li class="line_sub-list_item">XXL</li>
                            <li class="line_sub-list_item">XXXL</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-5">
                            <li class="line_sub-list_item line_sub-list_item-title">ЕВРОПА/ФРАНЦИЯ<span
                                        class="line_sub-list_item_value">EU/FR</span></li>
                            <li class="line_sub-list_item">2</li>
                            <li class="line_sub-list_item">3</li>
                            <li class="line_sub-list_item">4</li>
                            <li class="line_sub-list_item">5</li>
                            <li class="line_sub-list_item">6</li>
                            <li class="line_sub-list_item">7</li>
                            <li class="line_sub-list_item">8</li>
                        </ul>
                    </li>
                </ul>
                <h2 class="size-table_title">Мужские носки</h2>
                <ul class="size-table_list">
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-6">
                            <li class="line_sub-list_item line_sub-list_item-title">СТОПА<span
                                        class="line_sub-list_item_value">СМ</span></li>
                            <li class="line_sub-list_item">21.5</li>
                            <li class="line_sub-list_item">22.5</li>
                            <li class="line_sub-list_item">23.5</li>
                            <li class="line_sub-list_item">24.5</li>
                            <li class="line_sub-list_item">25</li>
                            <li class="line_sub-list_item">25,5</li>
                            <li class="line_sub-list_item">26,5</li>
                            <li class="line_sub-list_item">27</li>
                            <li class="line_sub-list_item">27,5</li>
                            <li class="line_sub-list_item">28,5</li>
                            <li class="line_sub-list_item">29</li>
                            <li class="line_sub-list_item">29,5</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-6">
                            <li class="line_sub-list_item line_sub-list_item-title">РАЗМЕР ОБУВИ</li>
                            <li class="line_sub-list_item">35</li>
                            <li class="line_sub-list_item">36</li>
                            <li class="line_sub-list_item">37</li>
                            <li class="line_sub-list_item">38</li>
                            <li class="line_sub-list_item">39</li>
                            <li class="line_sub-list_item">40</li>
                            <li class="line_sub-list_item">41</li>
                            <li class="line_sub-list_item">42</li>
                            <li class="line_sub-list_item">43</li>
                            <li class="line_sub-list_item">44</li>
                            <li class="line_sub-list_item">45</li>
                            <li class="line_sub-list_item">46</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-6">
                            <li class="line_sub-list_item line_sub-list_item-title">МЕЖДУНАР.<span
                                        class="line_sub-list_item_value">INT</span></li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-3">S</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-3">M</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-3">L</li>
                        </ul>
                    </li>
                    <li class="size-table_list_line">
                        <ul class="size-table_list_line_sub-list size-table_list_line_sub-list-other-cells-6">
                            <li class="line_sub-list_item line_sub-list_item-title">РОССИЯ<span
                                        class="line_sub-list_item_value">RUS</span></li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">21</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">23</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">25</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">27</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">29</li>
                            <li class="line_sub-list_item line_sub-list_item-other-item-2">31</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table">
                <h2 class="modal_window_title modal_window_title-table-size">Размеры детской одежды</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list_for-child">
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Возраст</li>
                            <li class="column_sub-list_item">0-1 месяц</li>
                            <li class="column_sub-list_item">2 месяца</li>
                            <li class="column_sub-list_item">3 месяца</li>
                            <li class="column_sub-list_item">6 месяцев</li>
                            <li class="column_sub-list_item">9 месяцев</li>
                            <li class="column_sub-list_item">12 месяцев</li>
                            <li class="column_sub-list_item">18 месяцев</li>
                            <li class="column_sub-list_item">2 года</li>
                            <li class="column_sub-list_item">3 года</li>
                            <li class="column_sub-list_item">4 года</li>
                            <li class="column_sub-list_item">5 лет</li>
                            <li class="column_sub-list_item">6 лет</li>
                            <li class="column_sub-list_item">7 лет</li>
                            <li class="column_sub-list_item">8 лет</li>
                            <li class="column_sub-list_item">9 лет</li>
                            <li class="column_sub-list_item">10 лет</li>
                            <li class="column_sub-list_item">11 лет</li>
                            <li class="column_sub-list_item">12 лет</li>
                            <li class="column_sub-list_item">13 лет</li>
                            <li class="column_sub-list_item">14 лет</li>
                            <li class="column_sub-list_item">16 лет</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Рост</li>
                            <li class="column_sub-list_item">46-50</li>
                            <li class="column_sub-list_item">51-56</li>
                            <li class="column_sub-list_item">57-62</li>
                            <li class="column_sub-list_item">63-68</li>
                            <li class="column_sub-list_item">69-74</li>
                            <li class="column_sub-list_item">75-80</li>
                            <li class="column_sub-list_item">81-86</li>
                            <li class="column_sub-list_item">87-92</li>
                            <li class="column_sub-list_item">93-98</li>
                            <li class="column_sub-list_item">99-104</li>
                            <li class="column_sub-list_item">105-110</li>
                            <li class="column_sub-list_item">111-116</li>
                            <li class="column_sub-list_item">117-122</li>
                            <li class="column_sub-list_item">123-128</li>
                            <li class="column_sub-list_item">129-134</li>
                            <li class="column_sub-list_item">135-140</li>
                            <li class="column_sub-list_item">141-146</li>
                            <li class="column_sub-list_item">147-152</li>
                            <li class="column_sub-list_item">153-158</li>
                            <li class="column_sub-list_item">159-164</li>
                            <li class="column_sub-list_item">165-170</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Обхват груди (см)</li>
                            <li class="column_sub-list_item">41-43</li>
                            <li class="column_sub-list_item">43-45</li>
                            <li class="column_sub-list_item">45-47</li>
                            <li class="column_sub-list_item">47-49</li>
                            <li class="column_sub-list_item">49-51</li>
                            <li class="column_sub-list_item">51-53</li>
                            <li class="column_sub-list_item">52-54</li>
                            <li class="column_sub-list_item">53-55</li>
                            <li class="column_sub-list_item">54-56</li>
                            <li class="column_sub-list_item">55-57</li>
                            <li class="column_sub-list_item">57-59</li>
                            <li class="column_sub-list_item">59-61</li>
                            <li class="column_sub-list_item">61-63</li>
                            <li class="column_sub-list_item">63-65</li>
                            <li class="column_sub-list_item">65-68</li>
                            <li class="column_sub-list_item">68-71</li>
                            <li class="column_sub-list_item">71-74</li>
                            <li class="column_sub-list_item">74-77</li>
                            <li class="column_sub-list_item">77-80</li>
                            <li class="column_sub-list_item">80-83</li>
                            <li class="column_sub-list_item">83-87</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Обхват талии (см)</li>
                            <li class="column_sub-list_item">41-43</li>
                            <li class="column_sub-list_item">43-45</li>
                            <li class="column_sub-list_item">45-47</li>
                            <li class="column_sub-list_item">46-48</li>
                            <li class="column_sub-list_item">47-49</li>
                            <li class="column_sub-list_item">48-50</li>
                            <li class="column_sub-list_item">49-51</li>
                            <li class="column_sub-list_item">50-52</li>
                            <li class="column_sub-list_item">51-53</li>
                            <li class="column_sub-list_item">52-54</li>
                            <li class="column_sub-list_item">53-55</li>
                            <li class="column_sub-list_item">54-56</li>
                            <li class="column_sub-list_item">55-57</li>
                            <li class="column_sub-list_item">57-59</li>
                            <li class="column_sub-list_item">58-61</li>
                            <li class="column_sub-list_item">61-63</li>
                            <li class="column_sub-list_item">63-65</li>
                            <li class="column_sub-list_item">65-67</li>
                            <li class="column_sub-list_item">67-69</li>
                            <li class="column_sub-list_item">69-72</li>
                            <li class="column_sub-list_item">72-76</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Обхват бедер (см)</li>
                            <li class="column_sub-list_item">41-43</li>
                            <li class="column_sub-list_item">43-45</li>
                            <li class="column_sub-list_item">45-47</li>
                            <li class="column_sub-list_item">47-49</li>
                            <li class="column_sub-list_item">49-51</li>
                            <li class="column_sub-list_item">51-53</li>
                            <li class="column_sub-list_item">52-54</li>
                            <li class="column_sub-list_item">54-56</li>
                            <li class="column_sub-list_item">56-58</li>
                            <li class="column_sub-list_item">58-60</li>
                            <li class="column_sub-list_item">60-62</li>
                            <li class="column_sub-list_item">61-64</li>
                            <li class="column_sub-list_item">64-67</li>
                            <li class="column_sub-list_item">67-70</li>
                            <li class="column_sub-list_item">70-73</li>
                            <li class="column_sub-list_item">73-76</li>
                            <li class="column_sub-list_item">76-79</li>
                            <li class="column_sub-list_item">79-82</li>
                            <li class="column_sub-list_item">82-85</li>
                            <li class="column_sub-list_item">85-88</li>
                            <li class="column_sub-list_item">88-92</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Размер<br>(рост)</li>
                            <li class="column_sub-list_item">50</li>
                            <li class="column_sub-list_item">56</li>
                            <li class="column_sub-list_item">62</li>
                            <li class="column_sub-list_item">68</li>
                            <li class="column_sub-list_item">74</li>
                            <li class="column_sub-list_item">80</li>
                            <li class="column_sub-list_item">86</li>
                            <li class="column_sub-list_item">92</li>
                            <li class="column_sub-list_item">98</li>
                            <li class="column_sub-list_item">104</li>
                            <li class="column_sub-list_item">110</li>
                            <li class="column_sub-list_item">116</li>
                            <li class="column_sub-list_item">122</li>
                            <li class="column_sub-list_item">128</li>
                            <li class="column_sub-list_item">134</li>
                            <li class="column_sub-list_item">140</li>
                            <li class="column_sub-list_item">146</li>
                            <li class="column_sub-list_item">152</li>
                            <li class="column_sub-list_item">158</li>
                            <li class="column_sub-list_item">164</li>
                            <li class="column_sub-list_item">170</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Международный<br>размер<br>(рост/размер)</li>
                            <li class="column_sub-list_item">50 / 1M</li>
                            <li class="column_sub-list_item">56 / 2M</li>
                            <li class="column_sub-list_item">62 / 3M</li>
                            <li class="column_sub-list_item">68 / 6M</li>
                            <li class="column_sub-list_item">74 / 9M</li>
                            <li class="column_sub-list_item">80 / 12M</li>
                            <li class="column_sub-list_item">86 / 18M</li>
                            <li class="column_sub-list_item">92 / 2Y</li>
                            <li class="column_sub-list_item">98 / 3Y</li>
                            <li class="column_sub-list_item">104 / 4Y</li>
                            <li class="column_sub-list_item">110 / 5Y</li>
                            <li class="column_sub-list_item">116 / 6Y</li>
                            <li class="column_sub-list_item">122 / 7Y</li>
                            <li class="column_sub-list_item">128 / 8Y</li>
                            <li class="column_sub-list_item">134 / 9Y</li>
                            <li class="column_sub-list_item">140 / 10Y</li>
                            <li class="column_sub-list_item">146 / 11Y</li>
                            <li class="column_sub-list_item">152 / 12Y</li>
                            <li class="column_sub-list_item">158 / 13Y</li>
                            <li class="column_sub-list_item">164 / 14Y</li>
                            <li class="column_sub-list_item">170 / 16Y</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>   --->
        <div class="modal js-size-table">
            <div class="modal_close-bgr"></div>
            <div class="size-table">
                <h2 class="modal_window_title modal_window_title-table-size">Размеры детской обуви</h2>
                <button class="modal_cls-btn js-size-table-cls-btn"></button>
                <ul class="size-table_list_for-child size-table_list_for-child-shoes">
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Длина стопы (см)</li>
                            <li class="column_sub-list_item">9.5</li>
                            <li class="column_sub-list_item">10</li>
                            <li class="column_sub-list_item">10.5</li>
                            <li class="column_sub-list_item">11</li>
                            <li class="column_sub-list_item">11.5</li>
                            <li class="column_sub-list_item">12</li>
                            <li class="column_sub-list_item">12.5</li>
                            <li class="column_sub-list_item">13</li>
                            <li class="column_sub-list_item">13.5</li>
                            <li class="column_sub-list_item">14</li>
                            <li class="column_sub-list_item">14.5</li>
                            <li class="column_sub-list_item">15</li>
                            <li class="column_sub-list_item">15.5</li>
                            <li class="column_sub-list_item">16</li>
                            <li class="column_sub-list_item">16.5</li>
                            <li class="column_sub-list_item">17</li>
                            <li class="column_sub-list_item">17.5</li>
                            <li class="column_sub-list_item">18</li>
                            <li class="column_sub-list_item">18.5</li>
                            <li class="column_sub-list_item">19</li>
                            <li class="column_sub-list_item">19.5</li>
                            <li class="column_sub-list_item">20</li>
                            <li class="column_sub-list_item">20.5</li>
                            <li class="column_sub-list_item">21</li>
                            <li class="column_sub-list_item">21.5</li>
                            <li class="column_sub-list_item">22</li>
                            <li class="column_sub-list_item">22.5</li>
                            <li class="column_sub-list_item">23</li>
                            <li class="column_sub-list_item">23.5</li>
                            <li class="column_sub-list_item">24</li>
                            <li class="column_sub-list_item">24.5</li>
                            <li class="column_sub-list_item">25</li>
                            <li class="column_sub-list_item">25.5</li>
                            <li class="column_sub-list_item">26</li>
                            <li class="column_sub-list_item">26.5</li>
                            <li class="column_sub-list_item">27</li>
                            <li class="column_sub-list_item">27.5</li>
                            <li class="column_sub-list_item">28</li>
                            <li class="column_sub-list_item">28.5</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Россия (RUS)</li>
                            <li class="column_sub-list_item">16</li>
                            <li class="column_sub-list_item">16.5</li>
                            <li class="column_sub-list_item">17</li>
                            <li class="column_sub-list_item">18</li>
                            <li class="column_sub-list_item">19</li>
                            <li class="column_sub-list_item">19.5</li>
                            <li class="column_sub-list_item">20</li>
                            <li class="column_sub-list_item">21</li>
                            <li class="column_sub-list_item">22</li>
                            <li class="column_sub-list_item">22.5</li>
                            <li class="column_sub-list_item">23</li>
                            <li class="column_sub-list_item">24</li>
                            <li class="column_sub-list_item">25</li>
                            <li class="column_sub-list_item">25.5</li>
                            <li class="column_sub-list_item">26</li>
                            <li class="column_sub-list_item">27</li>
                            <li class="column_sub-list_item">28</li>
                            <li class="column_sub-list_item">28.5</li>
                            <li class="column_sub-list_item">29</li>
                            <li class="column_sub-list_item">30</li>
                            <li class="column_sub-list_item">31</li>
                            <li class="column_sub-list_item">31.5</li>
                            <li class="column_sub-list_item">32</li>
                            <li class="column_sub-list_item">33</li>
                            <li class="column_sub-list_item">34</li>
                            <li class="column_sub-list_item">34.5</li>
                            <li class="column_sub-list_item">35</li>
                            <li class="column_sub-list_item">36</li>
                            <li class="column_sub-list_item">37</li>
                            <li class="column_sub-list_item">37.5</li>
                            <li class="column_sub-list_item">38</li>
                            <li class="column_sub-list_item">39</li>
                            <li class="column_sub-list_item">40</li>
                            <li class="column_sub-list_item">40.5</li>
                            <li class="column_sub-list_item">41</li>
                            <li class="column_sub-list_item">42</li>
                            <li class="column_sub-list_item">43</li>
                            <li class="column_sub-list_item">43.5</li>
                            <li class="column_sub-list_item">44</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Европа (EUR)</li>
                            <li class="column_sub-list_item">17</li>
                            <li class="column_sub-list_item">17.5</li>
                            <li class="column_sub-list_item">18</li>
                            <li class="column_sub-list_item">19</li>
                            <li class="column_sub-list_item">20</li>
                            <li class="column_sub-list_item">20.5</li>
                            <li class="column_sub-list_item">21</li>
                            <li class="column_sub-list_item">22</li>
                            <li class="column_sub-list_item">23</li>
                            <li class="column_sub-list_item">23.5</li>
                            <li class="column_sub-list_item">24</li>
                            <li class="column_sub-list_item">25</li>
                            <li class="column_sub-list_item">26</li>
                            <li class="column_sub-list_item">26.5</li>
                            <li class="column_sub-list_item">27</li>
                            <li class="column_sub-list_item">28</li>
                            <li class="column_sub-list_item">29</li>
                            <li class="column_sub-list_item">29.5</li>
                            <li class="column_sub-list_item">30</li>
                            <li class="column_sub-list_item">31</li>
                            <li class="column_sub-list_item">32</li>
                            <li class="column_sub-list_item">32.5</li>
                            <li class="column_sub-list_item">33</li>
                            <li class="column_sub-list_item">34</li>
                            <li class="column_sub-list_item">35</li>
                            <li class="column_sub-list_item">35.5</li>
                            <li class="column_sub-list_item">36</li>
                            <li class="column_sub-list_item">37</li>
                            <li class="column_sub-list_item">38</li>
                            <li class="column_sub-list_item">38.5</li>
                            <li class="column_sub-list_item">39</li>
                            <li class="column_sub-list_item">40</li>
                            <li class="column_sub-list_item">41</li>
                            <li class="column_sub-list_item">41.5</li>
                            <li class="column_sub-list_item">42</li>
                            <li class="column_sub-list_item">43</li>
                            <li class="column_sub-list_item">44</li>
                            <li class="column_sub-list_item">44.5</li>
                            <li class="column_sub-list_item">45</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">Великобритания (UK)</li>
                            <li class="column_sub-list_item">1.5</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">2</li>
                            <li class="column_sub-list_item">3</li>
                            <li class="column_sub-list_item">4</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">4.5</li>
                            <li class="column_sub-list_item">5.5</li>
                            <li class="column_sub-list_item">6</li>
                            <li class="column_sub-list_item">6.5</li>
                            <li class="column_sub-list_item">7</li>
                            <li class="column_sub-list_item">7.5</li>
                            <li class="column_sub-list_item">8.5</li>
                            <li class="column_sub-list_item">9</li>
                            <li class="column_sub-list_item">9.5</li>
                            <li class="column_sub-list_item">10</li>
                            <li class="column_sub-list_item">11</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">11.5</li>
                            <li class="column_sub-list_item">12.5</li>
                            <li class="column_sub-list_item">13</li>
                            <li class="column_sub-list_item">13.5</li>
                            <li class="column_sub-list_item">1</li>
                            <li class="column_sub-list_item">2</li>
                            <li class="column_sub-list_item">2.5</li>
                            <li class="column_sub-list_item">3</li>
                            <li class="column_sub-list_item">3.5</li>
                            <li class="column_sub-list_item">4</li>
                            <li class="column_sub-list_item">5</li>
                            <li class="column_sub-list_item">5.5</li>
                            <li class="column_sub-list_item">6</li>
                            <li class="column_sub-list_item">6.5</li>
                            <li class="column_sub-list_item">7.5</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">8</li>
                            <li class="column_sub-list_item">9</li>
                            <li class="column_sub-list_item">9.5</li>
                            <li class="column_sub-list_item">10</li>
                            <li class="column_sub-list_item">10.5</li>
                        </ul>
                    </li>
                    <li class="size-table_list_column">
                        <ul class="column_sub-list">
                            <li class="column_sub-list_item">США (USA)</li>
                            <li class="column_sub-list_item">2</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">2.5</li>
                            <li class="column_sub-list_item">3.5</li>
                            <li class="column_sub-list_item">4.5</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">5</li>
                            <li class="column_sub-list_item">6</li>
                            <li class="column_sub-list_item">6.5</li>
                            <li class="column_sub-list_item">7</li>
                            <li class="column_sub-list_item">7.5</li>
                            <li class="column_sub-list_item">8</li>
                            <li class="column_sub-list_item">9</li>
                            <li class="column_sub-list_item">9.5</li>
                            <li class="column_sub-list_item">10</li>
                            <li class="column_sub-list_item">10.5</li>
                            <li class="column_sub-list_item">11.5</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">12</li>
                            <li class="column_sub-list_item">13</li>
                            <li class="column_sub-list_item">13.5</li>
                            <li class="column_sub-list_item">1</li>
                            <li class="column_sub-list_item">1.5</li>
                            <li class="column_sub-list_item">2.5</li>
                            <li class="column_sub-list_item">3</li>
                            <li class="column_sub-list_item">3.5</li>
                            <li class="column_sub-list_item">4</li>
                            <li class="column_sub-list_item">4.5</li>
                            <li class="column_sub-list_item">5.5</li>
                            <li class="column_sub-list_item">6</li>
                            <li class="column_sub-list_item">6.5</li>
                            <li class="column_sub-list_item">7</li>
                            <li class="column_sub-list_item">8</li>
                            <li class="column_sub-list_item">-</li>
                            <li class="column_sub-list_item">8.5</li>
                            <li class="column_sub-list_item">9.5</li>
                            <li class="column_sub-list_item">10</li>
                            <li class="column_sub-list_item">10.5</li>
                            <li class="column_sub-list_item">11</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal js-subscribe-product-receipts">
            <div class="modal_close-bgr js-subscribe-product-receipts-close"></div>
            <form class="modal_out-of-stock js-subscribe-product-receipts-form">
                <h2 class="modal_window_title"> Узнать о поступлении товара</h2>
                <button class="modal_cls-btn js-subscribe-product-receipts-close"></button>
                <p class="modal_out-of-stock_description">К сожалению, данного размера сейчас нет в наличии. Мы вас
                    уведомим, как только он появиться.</p>
                <ul class="dropdown_selection_list">
                    <li class="dropdown_selection_list_item">
                        <label class="dropdown_selection_input-wrap">
                            <input class="dropdown_selection_checked" type="checkbox">
                            <div class="dropdown_selection_for-checked"></div>
                            <p class="dropdown_selection_description">44</p>
                        </label>
                    </li>
                    <li class="dropdown_selection_list_item">
                        <label class="dropdown_selection_input-wrap">
                            <input class="dropdown_selection_checked" type="checkbox">
                            <div class="dropdown_selection_for-checked"></div>
                            <p class="dropdown_selection_description">44</p>
                        </label>
                    </li>
                    <li class="dropdown_selection_list_item">
                        <label class="dropdown_selection_input-wrap">
                            <input class="dropdown_selection_checked" type="checkbox">
                            <div class="dropdown_selection_for-checked"></div>
                            <p class="dropdown_selection_description">44</p>
                        </label>
                    </li>
                    <li class="dropdown_selection_list_item">
                        <label class="dropdown_selection_input-wrap">
                            <input class="dropdown_selection_checked" type="checkbox">
                            <div class="dropdown_selection_for-checked"></div>
                            <p class="dropdown_selection_description">44</p>
                        </label>
                    </li>
                </ul>
                <div class="home-subscribe_for-subscribe_wrap">
                    <input class="home-subscribe_for-subscribe_field js-subscribe-product-receipts-mail" type="text"
                           placeholder="Ваша эл. почта">
                    <button class="home-subscribe_for-subscribe_btn js-subscribe-product-receipts-send" type="submit">
                        Подписаться
                    </button>
                </div>
            </form>
        </div>
        <div class="modal js-add-product-to-cart">
            <div class="added-goods">
                <h2 class="modal_window_title">Товар добавлен в корзину</h2>
                <button class="modal_cls-btn js-add-product-to-cart-close"></button>
                <div class="added-goods_product">
                    <div class="added-goods_product_image">
                        <img class="added-goods_product_image_photo" src="<?php echo $images[0]['popup']; ?>">
                    </div>
                    <div class="added-goods_product_description">
                        <p class="added-goods_product_description_content added-goods_product_description_content-name">
                            <?php echo $heading_title; ?> <?php echo $manufacturer; ?></p>
                        <p class="added-goods_product_description_content">Размер: 42 (RU)</p>
                        <p class="added-goods_product_description_content added-goods_product_description_content-availability">
                            В наличии 1 шт.</p>
                        <p class="added-goods_product_description_content added-goods_product_description_content-price">
                            <?php if (!$special) { ?>
                            <?php echo $price; ?>
                            <?php } else { ?>
                            <?php echo $special; ?>
                            <?php } ?>
                        </p>
                    </div>
                    <!---div class="added-goods_product_see-more"><p class="added-goods_product_description_content">И
                        еще <a href='#' class='added-goods_product_description_link'>3 товара</a></p></div--->
                </div>
                <div class="added-goods_btns">
                    <button class="added-goods_btns_btn added-goods_btns_btn-continue">Продолжить покупки</button>
                    <button class="added-goods_btns_btn added-goods_btns_btn-ordering"
                            onclick="location.href = '<?php echo $shopping_cart; ?>';">Оформить заказ
                    </button>
                </div>
            </div>
        </div>
        <div class="catalog_breadcrumbs">
            <ul class="breadcrumbs_list">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs_list_item"><a class="breadcrumbs_list_item_link"
                                                     href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <div class="product-description">
            <div class="product-description-wrap"><h2
                        class="product-description-aside_name"><?php echo $manufacturer; ?></h2>
                <p class="product-description-aside_specification"><?php echo $heading_title; ?></p>

                <?php if (!$special) { ?>
                <p class="product-description-aside_price"><?php echo $price; ?></p>
                <?php } else { ?>
                <p class="product-description-aside_price"><?php echo $price; ?></p>
                <p class="product-description-aside_price"><?php echo $special; ?></p>
                <?php } ?>

            </div>
            <div class="product-description-aside"><h2
                        class="product-description-aside_name"><?php echo $manufacturer; ?></h2>
                <p class="product-description-aside_specification"><?php echo $heading_title; ?></p>
                <?php if (!$special) { ?>
                <p class="product-description-aside_price"><?php echo $price; ?></p>
                <?php } else { ?>
                <p class="product-description-aside_price"><?php echo $price; ?></p>
                <p class="product-description-aside_price"><?php echo $special; ?></p>
                <?php } ?>


                <ul class="aside_color-list">
                    <?php foreach ($color as $val) { ?>
                    <?php $background = '' ; ?>
                    <?php $class = '' ; ?>
                    <?php if(!empty($val['code'])) $background = 'style="background: ' . $val['code'] . '"'; ?>
                    <?php if(!empty($val['isCurrent'])) $class = 'aside_color-list_item_check'; ?>

                    <li class="aside_color-list_item <?php echo $class ; ?>">
                        <label class="aside_color-list_item_wrap">
                            <!--input class="aside_color-list_item_check" type="radio" name="color"--->
                            <a href="<?php echo $val['href']; ?>" title="<?php echo $val['color']; ?>">
                                <span class="aside_color-list_item_for-check" <?php echo $background; ?>></span>
                            </a>
                        </label>
                    </li>

                    <?php } ?>
                </ul>

                <?php if ($options) { ?>
                <?php foreach ($options as $option) { ?>
                <?php if ($option['type'] == 'select' && $option['option_id'] == 11) { ?>
                <div class="aside_size-select">
                    <div class="aside_size-select_for-click js-product-view-size-select-clicked"></div>
                    <input class="aside_size-select_direction js-product-view-size-select" type="text" readonly
                           placeholder="Выберите размер">
                    <div class="size-select_dropdown js-product-view-size-select-list">
                        <ul class="size-select_dropdown_list"
                            data-name="<?php echo $option['product_option_id']; ?>">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <li class="size-select_dropdown_list_item"
                                data-value="<?php echo $option_value['product_option_value_id']; ?>">
                                <span class="size-select_dropdown_list_link js-product-view-size-select-item"><?php echo $option_value['name']; ?></span>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>

                </div>

                <div class="product-description_links">
                    <a class="product-description_links_content" href="<?php echo $choose_sizes; ?>">Подобрать
                        размер</a>
                    <span class="product-description_links_content js-size-table-open-btn">Таблица размеров</span>
                </div>

                <?php } ?>
                <?php } ?>
                <?php } ?>

                <div class="product-description_in-basket">
                    <button class="product-description_in-basket_add"
                            onclick="cart.add('<?php echo $product_id; ?>', '<?php echo $minimum; ?>');">В корзину
                    </button>
                    <?php if(in_array($product_id, $wishlist_product)) { ?>
                    <button type="button"
                            class="product-description_in-basket_liked product-description_in-basket_liked-active"
                            onclick="wishlist.remove('<?php echo $product_id; ?>');"></button>
                    <?php } else { ?>
                    <button type="button" class="product-description_in-basket_liked"
                            onclick="wishlist.add('<?php echo $product_id; ?>');"></button>
                    <?php } ?>
                </div>
            </div>

            <?php if ($thumb || $images) { ?>
            <div class="product-description_gallery js-product-slider">
                <div class="product-description_gallery_for-active-photo"><img
                            class="product-description_gallery_photo js-product-slider-full-img"
                            src="<?php echo $images[0]['popup']; ?>"></div>
                <ul class="product-description_gallery_list">

                    <?php if ($images) { ?>
                    <?php foreach ($images as $image) { ?>
                    <li class="product-description_gallery_list_item product-description_gallery_list_item-active">
                        <img class="product-description_gallery_photo js-product-slider-item"
                             src="<?php echo $image['popup']; ?>">
                    </li>
                    <?php } ?>
                    <?php } ?>
                </ul>
            </div>

            <?php } else { ?>
            <li class="product-description_gallery_list_item">
                <img class="product-description_gallery_photo js-product-slider-item" src="<?php echo $thumb; ?>"></li>
            <?php } ?>

            <div class="product-description_description">
                <div class="product-description_description_titles">
                    <div class="product-description_description_title js-info-select" open="1">Описание</div>
                    <div class="product-description_description_title js-info-select" open="2">Информация о
                        материалах
                    </div>
                </div>
                <div class="product-description_description_info js-info-window" window="1">
                    <div class="product-description_description_wrap">

                        <div class="product-description_description_info_brand-description">
                            <h2 class="product-description_description_info_title product-description_description_info_title-brand-description"><?php echo $manufacturer; ?></h2>
                            <p class="product-description_description_info_content"><?php echo $manufacturer_desc; ?></p>
                        </div>

                        <div class="product-description_description_info_list">
                            <h2 class="product-description_description_info_title product-description_description_info_title-details">
                                Детали</h2>
                            <?php echo $detail; ?>
                        </div>

                        <div class="product-description_description_info_list">
                            <h2 class="product-description_description_info_title product-description_description_info_title-sizes">
                                Размеры</h2>
                            <?php echo $sizes; ?>
                        </div>

                    </div>
                </div>
                <div class="product-description_description_info js-info-window" window="2">
                    <div class="product-description_description_wrap">

                        <div class="product-description_description_info_list product-description_description_info_list-width">
                            <h2 class="product-description_description_info_title product-description_description_info_title-material">
                                Материал: <?php echo $material; ?></h2>
                            <?php echo $description; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php if($products) { ?>

        <div class="catalog_clothes"><h2 class="catalog_clothes_title"><?php echo $text_related; ?></h2>
            <ul class="clothes_list clothes_list-product-page">

                <?php foreach ($products as $product) { ?>
                <li class="clothes_list_item">
                    <div class="clothes_list_item_wrap">
                        <button class="clothes_like"></button>
                        <div class="clothes_new"><p class="clothes_new_content">new</p></div>
                        <div class="clothes_discount"><p class="clothes_discont_content">75%</p></div>
                        <a href="<?php echo $product['href']; ?>"><img class="clothes_photo"
                                                                       src="<?php echo $product['thumb']; ?>"></a>
                        <p class="clothes_name"><a
                                    href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
                        <p class="clothes_description">Качели вечернее платье с кружевом</p>

                        <?php if (!$product['special']) { ?>
                        <div class="clothes_prices"><p class="clothes_prices_price"><?php echo $product['price']; ?></p>
                        </div>
                        <?php } else { ?>
                        <div class="clothes_prices"><p
                                    class="clothes_prices_price"><?php echo $product['special']; ?></p></div>
                        <div class="clothes_prices"><p class="clothes_prices_price"><?php echo $product['price']; ?></p>
                        </div>
                        <?php } ?>

                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>

        <?php } ?>

    </section>
</section>
<script>var pageName = 'ProductViewPage';</script>

<?php echo $footer; ?>
