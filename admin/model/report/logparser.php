<?php
class ModelReportLogparser extends Model {
	public function getListParserAction() {
		$query = $this->db->query('SELECT DATE_FORMAT(data_start,"%d.%m.%Y %H:%i") as date_start, 
            DATE_FORMAT(data_end,"%d.%m.%Y %H:%i") as date_end, insItems, updItems, name
            FROM oc_list_log_update ORDER BY data_start DESC LIMIT 0,10'
        );

		return $query->rows;
	}
}