<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-shopping-cart"></i>Лог работы парсеров</h3>
  </div>
  <div class="table-responsive">
    <table class="table">
      <thead>
      <tr>
        <td></td>
        <td>Начало работы</td>
        <td>Конец работы</td>
        <td>Элементов обновлено</td>
        <td>Элементов добавлено</td>
      </tr>
      </thead>
      <tbody>
      <?php if ($activities) { ?>
      <?php foreach ($activities as $order) { ?>
      <tr>
        <td><?php echo $order['name']; ?></td>
        <td><?php echo $order['date_start']; ?></td>
        <td><?php echo $order['date_end']; ?></td>
        <td><?php echo $order['updItems']; ?></td>
        <td><?php echo $order['insItems']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>
</div>