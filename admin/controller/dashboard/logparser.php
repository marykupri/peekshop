<?php
class ControllerDashboardLogparser extends Controller {
	public function index() {
		$this->load->language('dashboard/activity');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['token'] = $this->session->data['token'];

		$data['activities'] = array();

		$this->load->model('report/logparser');

		$results = $this->model_report_logparser->getListParserAction();

		foreach ($results as $result) {

			$data['activities'][] = array(
                'name'       => $result['name'],
				'date_start' => $result['date_start'],
                'date_end'   => $result['date_end'],
                'updItems'   => $result['updItems'],
                'insItems'   => $result['insItems']
			);
		}

		return $this->load->view('dashboard/logparser.tpl', $data);
	}
}
