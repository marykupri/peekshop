'use strict';

var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

function rootPath(url) {
    return path.resolve(__dirname, url);
}

var PAGES = ['home', 'catalog', 'product-view', 'ordering', 'my-account', 'account-order', 'account-data', 'account-coupon', 'account-favorites-authorized', 'account-favorites-unauthorized', 'home-subscribe', '404-page', 'about-page', 'help-simple', 'offer-page', 'brands-page'];

var TemplateArray = [];
var Entryes = [rootPath("src/ts/script.bundle.ts"), rootPath("src/scss/app.scss")];

PAGES.forEach(function (item) {

    TemplateArray.push(new HtmlWebpackPlugin({
        filename: item + ".html",
        inject: 'head',
        chunks: [item],
        hash: true,
        template: "templates/" + item + ".pug",
        minify: false
    }));
});

module.exports = {
    context: path.resolve('./src'),
    entry: Entryes,
    output: {
        path: __dirname + '/dist',
        filename: 'script.bundle.js'
    },
    node: {
        fs: 'empty'
    },

    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [{
            test: /.ts$/,
            loader: 'awesome-typescript-loader?minimize=false'
        }, {
            test: /\.(sass|scss)$/,
            loader: ExtractTextPlugin.extract(['css-loader?minimize=true', 'autoprefixer-loader?browsers=last 2 versions', 'sass-loader'])
        }, {
            test: /\.pug/,
            loaders: ['html-loader?minimize=false', 'pug-html-loader']
        }]
    },
    plugins: [].concat(TemplateArray, [new ExtractTextPlugin({
        filename: 'style.bundle.css',
        allChunks: true
    }), new CopyWebpackPlugin([{ from: __dirname + '/src/assets', to: 'assets' }])]),
    devServer: {

        proxy: {
            '/api/**': {
                target: 'http://localhost:3001',
                secure: false
            }
        }
    }
};