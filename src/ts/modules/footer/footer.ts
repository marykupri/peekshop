declare let $: any;

export class Footer {

    constructor () {
        $(document).ready(() => {
            this.infoBtnToggle();
        });
    }    

    infoBtnToggle() {
        let btns = $('.info_title');

        let that = this

        btns.off( 'click' );

        if ( window.innerWidth < 1024 ) {
            btns.on( 'click', function() {

                $(this).next().toggleClass('info-active')

            })  
        }
    }
}