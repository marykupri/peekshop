import { SideMenuSwipe } from './side-menu-swipe';
import { ModalOpenInterface, ModalOpen } from './../modal-open/modal-open';
import * as Inputmask from "inputmask";
declare let $: any;

(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})($);


class FormValidate {
    constructor ( options ) {
        this.formInit(options);
    }

    customSubmitFunc: any;

    customSubmit (callback) {
        this.customSubmitFunc = callback;
    }

    formInit (options) {
        let isFormValid = false;
        $(options.form).on('submit', ( e ) => {
            e.preventDefault();
            options.formValues.forEach((item) => {
                item.valid = ($(item.input).val().substr(-1) !== '_' && $(item.input).val().length > 0 ) ? true : false;
                $(item.input).css({
                    'border': ( ($(item.input).val().substr(-1) !== '_' && $(item.input).val().length > 0 ) ? '1px solid #323232' : '1px solid red')
                });
            });
            isFormValid = options.formValues.every( (item) => {
                return item.valid === true;
            });
            if (isFormValid) {
                $(options.form).off('submit');
                if (typeof this.customSubmitFunc === 'function') {
                    this.customSubmitFunc ($(options.form).serializeFormJSON());
                } else {
                    $(options.form).submit();
                }
            } else {
                let inputs = '';
                options.formValues.forEach((item, i) => {inputs += (item.input + (i < options.formValues.length - 1 ? ',': ''))});
                $(inputs).on('input', ( e ) => {
                    $(e.target).css({
                        'border': ( ($(e.target).val().substr(-1) !== '_' && $(e.target).val().length > 0 ) ? '1px solid #323232' : '1px solid red')
                    });
                });
            }
            
        });
    }
}

export class Header {

    headerNode = $('#header');

    subMenuSubscribers = [];

    entranceModalWindow: any;
    entranceModalWindowConfig: ModalOpenInterface = {
        modalWindow : {
            regular: '.js-header-entrance-form',
            active: '.modal-active'
        },
        openBtn: '.js-header-entrance-btn',
        clsBtn: '.js-header-entrance-close'
    }

    registrationModalWindow: any;
    registrationModalWindowConfig: ModalOpenInterface = {
        modalWindow : {
            regular: '.js-header-registration-form',
            active: '.modal-active'
        },
        openBtn: '.js-header-registration-btn',
        clsBtn: '.js-header-registration-close'
    }

    lostpasswordModalWindow: any;
    lostpasswordModalWindowConfig: ModalOpenInterface = {
        modalWindow : {
            regular : '.js-lost-password-form',
            active : 'modal-active'
        },
        openBtn: '.js-lost-password-btn',
        clsBtn: '.js-lost-password-close'
    }

    successfullRegistrationWindow: any;
    successfullRegistrationWindowConfig: ModalOpenInterface = {
        modalWindow: {
            regular: '.js-successfull-registration-form',
            active: '.modal-active'
        },
        openBtn: '.js-successfull-registration-btn',
        clsBtn: '.js-successfull-registration-close'
    }

    successfullLostpasswordWindow: any;
    successfullLostpasswordWindowConfig: ModalOpenInterface = {
        modalWindow: {
            regular: '.js-successfull-lostpassword-form',
            active: '.modal-active'
        },
        openBtn: '.js-successfull-lostpassword-btn',
        clsBtn: '.js-successfull-lostpassword-close'
    }


    constructor ( ) {
        $(document).ready( ( ) => {
            this.sideMenuToggle();
            this.centralBtnToggle();
            this.onResize();
            this.subMenuToggle();
            this.myAccountMenu();
            this.registrationFormValidate();
            this.openSearchField();
            this.scrollBar();
        });

        this.loginFormValidate ( );
        this.restoringFormValidate ( );

        this.entranceModalWindow = new ModalOpen( this.entranceModalWindowConfig, ( ) => {
            // after open callback;
        });

        this.registrationModalWindow = new ModalOpen( this.registrationModalWindowConfig, ( ) => {
            // after open callback;
        });

        this.lostpasswordModalWindow = new ModalOpen( this.lostpasswordModalWindowConfig, ( ) => {
            // after open callback;
        });

        this.successfullRegistrationWindow = new ModalOpen( this.successfullRegistrationWindowConfig, () => {
            setTimeout( ( ) => {
                this.successfullRegistrationWindow.closeModal ( );
                // location.reload();
            }, 5000);
        });

        this.successfullLostpasswordWindow = new ModalOpen( this.successfullLostpasswordWindowConfig, () => {
            setTimeout( ( ) => {
                this.successfullLostpasswordWindow.closeModal ( );
            }, 5000);
        });
    }

    openSearchField ( ) {
        $('.right-nav_unit-search').on( 'click', ( ) => {
            $('.search-field').toggleClass('search-field-active');
        } )

        $('.search-field_cls-btn').on( 'click', ( ) => {
            $('.search-field').removeClass('search-field-active');
        } )
        $(document).on('click', (e) => {
            var container = $(".search-field");
            var container2 = $('.right-nav_unit-search');
        
            if (!container.is(e.target) && container.has(e.target).length === 0  && !container2.is(e.target) && container2.has(e.target).length === 0){
                container.removeClass('search-field-active');
            }
        });
    }

    scrollBar ( ) {
        $('.search-field_list').slimScroll({
            position: 'right',
            height: '300px',
            railVisible: true,
            alwaysVisible: true
        });
    }

    myAccountMenu ( ) {
        $('.js-header-myaccount-btn').off( 'click' );
        $('.js-header-myaccount-btn').on( 'click', ( ) => {
            $('.js-header-myaccount-menu').toggleClass('right-nav_unit_my-account-active');
        });
        $(window).on('scroll', () => {
            if ( window.innerWidth > 1023 ) {
                $('.js-header-myaccount-menu').removeClass('right-nav_unit_my-account-active');
            }
        });
    }


    restoringFormValidate() {
        // Inputmask('email').mask('.js-restoring-mail');
        let options = {
            form: document.querySelector('#js-restoring-form'),
            formValues: [{input: '.js-restoring-mail', valid: false}]
        }
        let restoringForm = new FormValidate(options).customSubmit((data) => {

            let url = '/index.php?route=account/forgotten';
            $.post(url, data, (res) => {
                // console.log(res);
                var obj = JSON.parse(res);
                if(obj.warning) {
                    $('.js-restoring-mail').css("border","1px solid red");
                    $('.js-lost-password-error').text(obj.warning);
                }else{
                    this.lostpasswordModalWindow.closeModal();
                    this.successfullLostpasswordWindow.openModal ( );
                }

            });

        });
    }


    loginFormValidate ( ) {
        // Inputmask('email').mask('.js-login-mail');
        let options = {
            form : document.querySelector('#js-login-form'),
            formValues : [
                { input: '.js-login-mail', valid: false },
                { input: '.js-login-password', valid: false }
            ]
        }
        let loginForm = new FormValidate (options).customSubmit((data) => {
            let url = '/index.php?route=account/login';
            $.post(url, data, (res) => {
                var obj = JSON.parse(res);
                if(obj.warning) {
                    $('.js-login-mail, .js-login-password').css("border","1px solid red");
                    $('.js-login-error').text(obj.warning);
                }else{
                    this.entranceModalWindow.closeModal();
                    window.location.href='/index.php?route=account/account';
                //     this.successfullLostpasswordWindow.openModal ( );
                }

            });
        })
    }


    registrationFormValidate ( ) {

        let form = $( '#registration-form' );
        form.submit( ( event ) => { event.preventDefault( ); });

        let isFormValid = false;

        let formFieldsRevise = {
            mail: { selector: '.js-header-registration-mail', val: false },
            // если нужно, раскомментировать поле с подпиской на новости
            // newssubscribe: { selector: '.js-header-registration-subscribe', val: false },
            agreement: { selector: '.js-header-registration-agree-marker', val: false },
            password: { selector: '.js-header-registration-password', val: false },
            name: { selector: '.js-header-registration-name', val: false },
        }
        function reviseForm ( ) {
            isFormValid = Object.keys( formFieldsRevise ).every( item => formFieldsRevise[item]['val'] === true )
            Object.keys(formFieldsRevise).forEach( item => {
                if(!formFieldsRevise[item]['val']) {
                    $(formFieldsRevise[item]['selector']).css({
                        'border': '1px solid red'
                    });
                }
            });
        }

        // Inputmask('email').mask('.js-header-registration-mail');
        $('.js-header-registration-mail').on('input', (e) => {
            if($(e.target).val().substr(-1) !== '_') {
                formFieldsRevise.mail.val = true;
                $(e.target).css({
                    'border': '1px solid #323232'
                });
            } else {
                formFieldsRevise.mail.val = false;
            }
        });

        $('.js-header-registration-password').on('input', (e) => {
            // добавить логику валидации пароля при регистрации
            if($(e.target).val().length > 0) {
                formFieldsRevise.password.val = true;
                $(e.target).css({
                    'border': '1px solid #323232'
                });
            } else {
                formFieldsRevise.password.val = false;
            }
        });

        $('.js-header-registration-name').on('input', (e) => {
            // добавить логику валидации имени при регистрации
            if($(e.target).val().length > 0) {
                formFieldsRevise.name.val = true;
                $(e.target).css({
                    'border': '1px solid #323232'
                });
            } else {
                formFieldsRevise.name.val = false;
            }
        });

        $('.js-header-registration-agree-input').on('change', (e) => {
            formFieldsRevise.agreement.val = $(e.target).is(':checked');
            if ($(e.target).is(':checked')) {
                $('.js-header-registration-agree-marker').css({
                    'border': '1px solid #323232'
                });
            }
        });

        this.successfullRegistrationWindow.insteadOpenWindow( ( ) => {
            reviseForm ( );
            if ( isFormValid ) {
                // отправка данных с формы регистрации на сервер
                let url = '/index.php?route=account/register';
                let data = form.serializeFormJSON();

                $.post(url, data, (res) => {
                    var obj = JSON.parse(res);
                    if(obj.warning) {
                        $('.js-header-registration-mail').css("border","1px solid red");
                        $('.js-registration-error').text(obj.warning);
                    }else{
                        // $('.right-nav_unit-sign-in').css('display','none');
                        // $('.right-nav_unit-account').css('display','block');
                        // this.registrationModalWindow.closeModal();
                        this.successfullRegistrationWindow.openModal();

                    }
                });
                // при удачной отправке
                // разблокировать и открыть окно оповещения
                // this.successfullRegistrationWindow.undisableOpenBtn ( );
            }
        });

    }

    // only mobile version
    sideMenuSwipe = new SideMenuSwipe( () => {}, () => {});

    // only mobile version
    sideMenuToggle ( ) {
        let toggleBtn = $('.show-menu');
        
        toggleBtn.off('click')

        if ( window.innerWidth < 1024 ) {
            toggleBtn.on('click', ( ) => {
                this.headerNode.toggleClass('header-active');
                this.allMenusClose ( );
            })
        }
    }
    // only mobile version
    centralBtnToggle ( ) {
        let btns = $('.center-nav_unit');

        let that = this

        btns.off( 'click' );

        if ( window.innerWidth < 1024 ) {
            btns.on( 'click', function ( ) {
                if(!$(this).hasClass('center-nav_unit-active')) {
                    that.allMenusClose ( );
                }

                $('.center-nav_unit').removeClass('center-nav_unit-active');
                $(this).addClass('center-nav_unit-active');

            })  
        }
    }
    // only mobile version
    subMenuToggle ( ) {
        let btns = $('.section-center_list_item_link');

        let that = this;

        btns.off( 'click' );

        if ( window.innerWidth < 1024 ) {
            btns.on( 'click', function ( ) {
                $(this).toggleClass('section-center_list_item_link-active');
                $('.center-nav_unit-without-sub-menu').animate({
                    'top': $('.center-nav_unit-active .center-nav_sub-menu').outerHeight() - 158 - 70
                },1);
                $('.right-nav_unit:last-of-type').animate({
                    'top': $('.center-nav_unit-active .center-nav_sub-menu').outerHeight() - 89 - 70
                },1);
            });
        }
    }
    // only mobile version
    onResize ( ) {
        $(window).on('resize', ( ) => {
            this.centralBtnToggle();
            this.sideMenuToggle();
            this.subMenuToggle();
        })
    }
    // only mobile version
    allMenusClose ( ) {
        $('.section-center_list_item_link').removeClass('section-center_list_item_link-active');
        $('.center-nav_unit-without-sub-menu').animate({
            'top': $('.center-nav_unit-active .center-nav_sub-menu').outerHeight( ) - 158 - 70
        },1)
        $('.right-nav_unit:last-of-type').animate({
            'top': $('.center-nav_unit-active .center-nav_sub-menu').outerHeight( ) - 89 - 70
        },1)
    }
}

