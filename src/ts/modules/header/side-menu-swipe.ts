declare let $ : any;

export class SideMenuSwipe {

    startPoint: any;
    newPoint: any;

    constructor ( openCallback, closeCallback ) {

        $(document).ready( ( ) => {
            let that = this;
            let header = document.querySelector("header");
            let menu = document.querySelector('.js-menu-swipe-close');
            menu.addEventListener('touchstart', touchStartEvent);
    
            function touchStartEvent(e) {
    
                if ( header.classList.contains('header-active') ) {
                    menu.removeEventListener('touchstart', touchStartEvent);
                    var startPoint = e.touches[0].pageX;
                    that.startPoint = startPoint;
                    window.addEventListener('touchmove', touchMoveEvent);
                    window.addEventListener('touchend', touchEndEvent);
                }
    
                function touchMoveEvent(e) {
                    var newLeft = e.touches[0].pageX;
                    that.newPoint = newLeft;
                    if ( that.startPoint > that.newPoint ) {
                        header.style.transform = 'translateX(' + newLeft + 'px)';
                    }
                }
                
                function touchEndEvent(e) {
                    if ( that.startPoint > that.newPoint && that.startPoint - that.newPoint > 30 ) {
                        header.classList.remove('header-active');
                        header.style.transform = 'translateX(0px)';
                        header.removeAttribute('style')
                    } else {
                        header.removeAttribute('style')
                    }
                    window.removeEventListener('touchmove', touchMoveEvent);
                    window.removeEventListener('touchend', touchEndEvent);
                    menu.addEventListener('touchstart', touchStartEvent);
                }
    
                return false;
    
                function getCoords(elem) {
                    var box = elem.getBoundingClientRect();
                    return {
                        top: box.top + pageYOffset,
                        left: box.left + pageXOffset
                    };
                }
            }
        });
        
    }
}