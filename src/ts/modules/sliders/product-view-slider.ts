
declare let $ : any;
declare let Promise : any;

export interface ProductViewSliderConfig {
    sliderBlock : string;
    viewFullclassName : string;
    items : {
        className : string;
        activeClassName : string;
    }
}

export class ProductViewSlider {

    constructor (private config : ProductViewSliderConfig ) {
        this.init();
    }

    public rebind () {
        $( this.config.sliderBlock ).off('click');
        $( this.config.items.className ).off('click');
    }

    public init () {
        $(document).ready( ( ) => {
            let sliderBlock = $( this.config.sliderBlock );
            let fullViewImage = $( this.config.viewFullclassName );
            let items = $( this.config.items.className );

            fullViewImage.attr( 'src', $( this.config.items.className ).first().attr('src') );
            fullViewImage.css({
                opacity: 1
            });

            $( this.config.items.className ).first().addClass( this.config.items.activeClassName );

            sliderBlock.on('click', items, ( e ) => {
                fullViewImage.attr( 'src', e.target.src );
                items.removeClass( this.config.items.activeClassName );
                $(e.target).addClass( this.config.items.activeClassName );
            });
        });
    }
}