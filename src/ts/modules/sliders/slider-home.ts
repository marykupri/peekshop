

export interface SliderHomeInterface {
    sliderId: string; 
    slides: any;
    btns: any;
    nav: string;
    period: number;
}

export class SliderHome {
    
    slider: any;

    slides = [];

    btns = [];

    options: SliderHomeInterface;

    currentSlide: number = 0;

    sliderInterval: any;

    constructor( options: SliderHomeInterface ) {
        this.options = options;
        this.slider = document.getElementById(options.sliderId);
        let nav = document.createElement('div');
        nav.classList.add(options.nav);


        Array.prototype.forEach.call( document.querySelectorAll(options.slides.name), (slide, i) => {
            this.slides.push(slide);
            let btn = document.createElement('div');
            btn.classList.add(options.btns.regular);

            if( i == this.currentSlide ) {
                btn.classList.add(options.btns.active);
                this.slides[i].classList.add(options.slides.active);
            }

            nav.appendChild(btn);
            btn.addEventListener('click', () => {
                this.removeInterval();
                this.changeIndex(i);
                this.setInterval();

            })

            this.btns.push(btn)
        })

        this.slider.appendChild(nav);

        this.setInterval();
    }

    changeIndex(val: number) {
        this.currentSlide = val;

        this.btns.forEach((element, i) => {
            element.classList.remove(this.options.btns.active);
            this.slides[i].classList.remove(this.options.slides.active);
            this.slides[i].classList.add(this.options.slides.disabled);
        });

        this.btns[this.currentSlide].classList.add(this.options.btns.active);

        this.slides[this.currentSlide].classList.add(this.options.slides.active);
        this.slides[this.currentSlide].classList.remove(this.options.slides.disabled);
    }

    setInterval() {
        this.sliderInterval = setInterval(() => {
            this.changeIndex((this.currentSlide < this.btns.length - 1) ? this.currentSlide + 1 : 0);
        },this.options.period)
    }

    removeInterval() {
        clearInterval(this.sliderInterval);
    }
}