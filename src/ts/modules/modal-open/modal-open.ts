declare let $: any;

class SubscribeCloserObserver  {
    subscribers = [];
    subscribe ( val ) {
        this.subscribers.push(val);
    }
    closeWindows () {
        this.subscribers.forEach(window => {
            window.closeModal();
        });
    }
}

let subscribeCloserObserver = new SubscribeCloserObserver( );

export class WindowScroll {

    currentScroll: number = window.scrollY;

    rememberScrollPosition: number = 0 ;

    bodyTopPosition: number = -window.scrollY;

    constructor ( ) {
        window.addEventListener('scroll', ( event ) => {
            this.currentScroll = window.scrollY
            this.bodyTopPosition = -window.scrollY;
        });
    }

    public block ( ) {
        $('body').css({
            'position': 'fixed',
            "top": this.bodyTopPosition,
            "overflow": "hidden"
        });
        this.rememberScroll( );
    }

    public unblock ( ) {
        $('body').css({
            'position': 'relative',
            "top": '0',
            "overflow-x": "hidden",
            'overflow-y': 'auto'
        });
        $(window).scrollTop(this.rememberScrollPosition)
    }

    rememberScroll ( ) {
        this.rememberScrollPosition = this.currentScroll;
    }
}



export interface ModalOpenInterface {
    clsBtn: string;
    modalWindow: {
        regular: string,
        active: string
    }
    openBtn: string;
}

export class ModalOpen {

    constructor(private options: ModalOpenInterface, private callback = ( ) => { return }) {
        subscribeCloserObserver.subscribe(this);

        let openBtns = $(this.options.openBtn);
        let clsBtn = $(this.options.clsBtn);
        
        openBtns.off('click');
        let that = this;
        openBtns.on('click', function ( e ) {
            that.reviuseInsteadFunc(this);
        })

        clsBtn.off('click');
        clsBtn.on('click', ( ) => {
            this.closeModal( );
        })
    }

    scroll = new WindowScroll();

    insteadOpenWindow( callback:any = ( ) => { return } ){
        this.insteadOpenFunc = callback;
    }

    insteadOpenFunc:any = false;
    reviuseInsteadFunc (element?) {
        if ( typeof this.insteadOpenFunc === 'function' ) {
            this.insteadOpenFunc( element );
        } else {
            this.openModal();
        }
    }

    openModal (  ) {
        subscribeCloserObserver.closeWindows();
        this.scroll.block( );
        $(this.options.modalWindow.regular).addClass(this.options.modalWindow.active.replace('.', ''));
        this.callback( );
        $('.header').removeClass('header-active');
    }

    closeModal ( ) {
        this.scroll.unblock( );
        $(this.options.modalWindow.regular).removeClass(this.options.modalWindow.active.replace('.', ''));
    }
}