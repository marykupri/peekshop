
declare let $ : any;


// интерфейс для создания списка в пустом блоке
export interface SelectCreateConfig {
    selector : string;
    clickedBlock : {
        className : string;
        title : {
            className : string;
            value : string;
        };
        result : {
            className : string;
            value : string;
            name : string;
        };
    };
    list : {
        className : string;
        activeClassName : string;
        options : {
            className : string;
            values : string[];
        };
    }
}

// интерфейс для инициализации списка с готовой разметкой
export interface SelectInitConfig {
    clickedBlock : {
        className : string;
        result : {
            className : string;
        };
    };
    list : {
        className : string;
        activeClassName : string;
        options : {
            className : string;
        };
    }
}

export class Select {

    config: any;

    constructor (  ) {
        
    }

    private selected : boolean = false;
    private currentValue: string = '';
    public getCurrentValue ( ) {
        return this.currentValue;
    }
    public isSelected ( ) {
        return this.selected;
    }


    // создает список в пустом блоке
    create ( config : SelectCreateConfig ) {
        this.config = config;
        $(document).ready( ( ) => {
            // блок селекта
            let select = document.querySelector(this.config.selector);
            select.innerHTML = '';
            let clickedBlock = document.createElement('div');
            clickedBlock.classList.add(this.config.clickedBlock.className);
    
            // заголовок блока селекта
            let title = document.createElement('div');
            title.classList.add(this.config.clickedBlock.title.className);
            title.innerHTML = this.config.clickedBlock.title.value;
    
            // инпут с результатом
            let input = document.createElement('input');
            input.type = 'text';
            input.setAttribute('readonly', 'readonly');
            input.name = this.config.clickedBlock.result.name;
            input.classList.add(this.config.clickedBlock.result.className);
            input.value = this.config.clickedBlock.result.value;
    
            clickedBlock.appendChild(title);
            clickedBlock.appendChild(input);
    
            // выпадающий список
            let list = document.createElement('ul');
            list.classList.add(this.config.list.className);
    
            // при клике на инпут добавлять клас актив списку
            clickedBlock.addEventListener('click', ( ) => {
                list.classList.toggle(this.config.list.activeClassName);
            });
    
            // наполнение списка
            this.config.list.options.values.forEach( ( item ) => {
                let option = document.createElement('div');
                option.classList.add(this.config.list.options.className);
                option.innerHTML = item;
                // при клике подставлять новое значение в инпут результата
                option.addEventListener('click', ( ) => {
                    input.value = item ;
                    this.selected = true;
                    this.currentValue = item;
                    // и удалять у списка класс актив
                    list.classList.remove(this.config.list.activeClassName);
                });
                list.appendChild( option );
            });
    
    
            select.appendChild( clickedBlock );
            select.appendChild( list );
        });
    }


    // инициализация списка с готовой разметкой
    public init ( config: SelectInitConfig ) {
        this.config = config;
        $(document).ready( ( ) => {
            let clickedBlock = $(this.config.clickedBlock.className);
            let option = $(this.config.list.options.className);
            let result = $(this.config.clickedBlock.result.className);
            let list = $(this.config.list.className);
            let that = this;

            clickedBlock.on('click', ( ) => {
                list.toggleClass(this.config.list.activeClassName);

                if ( list.hasClass( this.config.list.activeClassName )) {
                    $(window).on('scroll', removeListClass );
                }
            });

            function removeListClass ( ) {
                list.removeClass( that.config.list.activeClassName );
                $(window).off('scroll', removeListClass);
            }

            
            option.on('click', function ( ) {
                result.val( $(this).html() );
                that.selected = true;
                that.currentValue = $(this).html();
                list.removeClass(that.config.list.activeClassName);
            });
            console.log(option,clickedBlock);
        });

        return this;
    }

    public rebind () {
        console.log('rebind');
        $(this.config.clickedBlock.result.className).val('');
        this.selected = false;
        this.currentValue = '';
        $(this.config.clickedBlock.className).off('click');
        $(this.config.list.options.className).off('click');
    }
}