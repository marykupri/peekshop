import { SelectInitConfig, Select } from './modules/form-elements/select';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
import * as Inputmask from "inputmask";
declare let $ : any;


export class OrderingPage {
    
    header: any ;

    footer: any ;

    constructor ( ) {
        
        this.header = new Header( );

        this.footer = new Footer( );

        this.orderFormSelect( );

        this.productOrderCounter( );

        this.inputValidate( );

        this.textInInput();
        
    }

    textInInput ( ) {
        $( document ).ready( () => {
            let inputs = document.querySelectorAll('.order-product_form_fields_content');
            let inputsArr = Array.prototype.slice.call(inputs);

            inputsArr.forEach( (item) => {

                if(item.value.length >= 1) {
                    item.nextElementSibling.classList.add('order-product_form_fields_text-active')
                }

                item.onblur = () => {
                    if(item.value.length >= 1) {
                        item.nextElementSibling.classList.add('order-product_form_fields_text-active')
                    } else {
                        item.nextElementSibling.classList.remove('order-product_form_fields_text-active')
                    }
                }

            });
        })
    }

    inputValidate ( ) {
        Inputmask({regex: "[0-9]*"}).mask('.js-post-index');
        Inputmask({mask: "+7(999)999-99-99", showMaskOnHover: false}).mask('.js-phone-delivery');
    }

    orderFormSelect ( ) {
        $(document).ready( ( ) => {
            $(`.js-order-form[form="1"]`).addClass('ordering_order-product_form-active');
            $('.js-order-select[open="1"]').addClass('ordering_order-product_title-active');
            $('.js-order-select').on('click', function( ) {
                $('.js-order-select').removeClass('ordering_order-product_title-active');
                $(this).addClass('ordering_order-product_title-active');
                $('.js-order-form').removeClass('ordering_order-product_form-active');
                $(`.js-order-form[form="${this.getAttribute('open')}"]`).addClass('ordering_order-product_form-active');
            });
        });
    }


    productOrderCounter ( ) {
        $(document).ready( ( ) => {
            let btnPlus = $('.js-product-counter-plus');
            let btnMinus = $('.js-product-counter-minus');
    
            btnMinus.on('click', function ( ) {
                inputChangeValue(this, -1);
            });
    
            btnPlus.on('click', function ( ) {
                inputChangeValue(this, 1);
            });
    
            function inputChangeValue (elem, val) {
                let input = $(elem).closest('.js-product-counter').find('.js-product-counter-input');
                input.val( ( Number(input.val()) + val === 0 ) ? '1' : Number(input.val()) + val );

                var quant = input.data('quantity');
                var value = input.val();

                $.ajax({
                    url: 'index.php?route=checkout/cart/edit',
                    type: 'post',
                    data: quant + '=' + value,
                    // dataType: 'json',
                    success: function (json) {
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            }
        });
    }

    selectAddressConfig : SelectInitConfig = {
        clickedBlock : {
            className : '.js-account-address',
            result : {
                className: '.js-account-address-input'
            }
        },
        list : {
            className : '.js-account-address-list',
            activeClassName : 'account-data_choose-location_list-active',
            options : {
                className : '.js-account-address-item'
            }
        }
    }
    selectAddress = new Select( ).init( this.selectAddressConfig );

}