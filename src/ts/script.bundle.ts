import { ProductViewPage } from './product-view';
import { OrderingPage } from './ordering';
import { OfferPage } from './offer-page';
import { MyAccountPage } from './my-account';
import { HomeSubscribePage } from './home-subscribe';
import { HelpPage } from './help-simple';
import { CatalogPage } from './catalog';
import { BrandsPage } from './brands-page';
import { AccountOrderPage } from './account-order';
import { AccountFavoritesUnauthorizedPage } from './account-favorites-unauthorized';
import { AccountFavoritesAuthorizedPage } from './account-favorites-authorized';
import { AccountDataPage } from './account-data';
import { HomePage } from './home';
import { ErrorPage } from './404-page';
import { AboutPage } from './about-page';
import { AccountCouponPage } from './account-coupon';
declare let pageName : any ;

let PAGES = [
    {
        page : 'HomePage',
        class : HomePage
    },{
        page : 'ErrorPage',
        class : ErrorPage
    },{
        page : 'AboutPage',
        class : AboutPage
    },{
        page : 'AccountDataPage',
        class : AccountDataPage
    },{
        page : 'AccountCouponPage',
        class : AccountCouponPage
    },{
        page : 'AccountFavoritesAuthorizedPage',
        class : AccountFavoritesAuthorizedPage
    },{
        page : 'AccountFavoritesUnauthorizedPage',
        class : AccountFavoritesUnauthorizedPage
    },{
        page : 'AccountOrderPage',
        class : AccountOrderPage
    },{
        page : 'BrandsPage',
        class : BrandsPage
    },{
        page : 'CatalogPage',
        class : CatalogPage
    },{
        page : 'HelpPage',
        class : HelpPage
    },{
        page : 'HomeSubscribePage',
        class : HomeSubscribePage
    },{
        page : 'MyAccountPage',
        class : MyAccountPage
    },{
        page : 'OfferPage',
        class : OfferPage
    },{
        page : 'OrderingPage',
        class : OrderingPage
    },{
        page : 'ProductViewPage',
        class : ProductViewPage
    }
]

window.onload = ( ) => {

    let Page = PAGES.filter( item => {
        
        return pageName === item.page;

    })[0]['class'];

    pageName = new Page( );

}



