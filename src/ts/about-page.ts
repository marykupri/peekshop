import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
import * as Inputmask from "inputmask";
declare let $ : any;



export class AboutPage {
    
    header: any ;

    footer: any ;

    constructor ( ) {
        
        this.header = new Header ( );

        this.footer = new Footer ( );

        this.sendMessageForm ( );

    }

    sendMessageForm ( ) {
        // $(document).ready( ( ) => {
        //     Inputmask('email').mask('.js-about-page-message-email');
        // });

        let form = $('#about-page-message-form');
        let mailInput = $(".js-about-page-message-email");
        let mailMessage = $(".js-about-page-message-text");
        let isFormValid = false ;

        function formValidate ( ) {
            $(mailInput).css({
                'border-bottom': (mailInput.val().substr(-1) !== '_' && mailInput.val().length !== 0 ) ? '1px solid #323232' : '1px solid red'
            });
            $(mailMessage).css({
                'border-bottom': (mailMessage.val().substr(-1) !== '_' && mailMessage.val().length !== 0 ) ? '1px solid #323232' : '1px solid red'
            });
            isFormValid = ( mailInput.val().substr(-1) !== '_' && mailInput.val().length !== 0 && mailMessage.val().substr(-1) !== '_' && mailMessage.val().length !== 0 ) ? true : false ;
        }

        form.submit( ( event ) => { 
            event.preventDefault( ); 

            formValidate ( );

            if ( isFormValid ) {

                mailInput.off('input');
                let data = form.serializeFormJSON ( );
                let url = '/index.php?route=information/contact/feedback';
                $.post(url, data, (res) => {
                    console.log(res);
                    res = JSON.parse(res);
                    if(res.email)
                        $(mailInput).css({
                            'border-bottom':'1px solid red'
                        });
                    if(res.message)
                        $(mailMessage).css({
                            'border-bottom':'1px solid red'
                        });
                    if(res.success){
                        $(mailInput).val('');
                        $(mailMessage).val('');
                    }

                });

                // отправка сообщения на сервер
                // ...

            } else {

                mailInput.on('input', ( ) => {
                    formValidate ( );
                });

                mailMessage.on('input', ( ) => {
                    formValidate ( );
                });

            }
        });
    }
}