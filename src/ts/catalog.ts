import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;

export class CatalogPage {

    header: any ;

    footer: any ;

    constructor ( ) {

        this.header = new Header( );

        this.footer = new Footer( );

    }


    sortingDropdownList = new DropdownList (
        '',
        { selector : '.js-category-select-btn', activeClass : 'selected-type_selection_content--active' },
        { selector : '.js-category-select-list', activeClass : 'selected-type_dropdown_selection--active' },
        true,
        true
    )

    clothesCategoryDropdownList = new DropdownList (
        '',
        { selector: '.js-clothes-category-select-btn', activeClass: 'catalog_aside_list_item_link--active' },
        { selector: '.js-clothes-category-select-list', activeClass: 'catalog_aside_sub-list--active' },
        true,
        true
    )


    // only mobile
    filterOrCategoryDropdownList = new DropdownList (
        'sorting',
        { selector: '.js-select-filter-btn', activeClass: 'filtration_sections_item-active' },
        { selector: '.js-select-filter-list', activeClass: 'catalog_aside_list--active' },
        false,
        false
    );


}



class DropdownList {
    constructor ( startVal: string,  btn : { selector: string; activeClass: string; }, list : { selector: string; activeClass: string; }, documentClickClose : boolean, scrollClose : boolean ) {
        $(document).ready( ( ) => {

            $('a.catalog_link-selected').parent().parent().addClass('catalog_aside_sub-list--active');
            $('a.catalog_link-selected').parent().parent().prev().addClass('catalog_aside_list_item_link--active');

            $(`${ list.selector }[list="${ startVal }"]`).addClass(`${ list.activeClass }`);
            $(`${ btn.selector }[open="${ startVal }"]`).addClass(`${ btn.activeClass }`);

            $(document).on('click', ( e ) => {

                if ( $(e.target).hasClass( btn.selector.replace('.', '') ) ) {

                    let currentBtn = e.target;

                    if ( $( currentBtn ).hasClass(`${ btn.activeClass }`) ) {

                        $(`${ btn.selector }`).removeClass(`${ btn.activeClass }`);
                        $(`${ list.selector }`).removeClass(`${ list.activeClass }`);

                    } else {

                        $(`${ btn.selector }`).removeClass(`${ btn.activeClass }`);
                        $( currentBtn ).addClass(`${ btn.activeClass }`);
                        $(`${ list.selector }`).removeClass(`${ list.activeClass }`);
                        $(`${ list.selector }[list="${ currentBtn.getAttribute('open') }"]`).addClass(`${ list.activeClass }`);

                    }

                }
                // else {
                //     if ( documentClickClose ) {
                //         $(`${ btn.selector }`).removeClass(`${ btn.activeClass }`);
                //         $(`${ list.selector }`).removeClass(`${ list.activeClass }`);
                //     }
                // }



            });

            if ( scrollClose ) {
                $(window).on('scroll', ( ) => {
                    if ( window.innerWidth >= 946 ) {
                        $(`${ btn.selector }`).removeClass(`${ btn.activeClass }`);
                        $(`${ list.selector }`).removeClass(`${ list.activeClass }`);
                    }
                });
            }

        });
    }
}