import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;

interface RangeSelectorConfig {
    startValue : number;
    min : number;
    max : number;
    input : string;
    display : string;
    btn : string;
    slide : string;
    plus : string;
    minus : string;
}

class RangeSelector {

    value : number;
    
    minValue: number;
    maxValue: number;
    selectorDisplay : any;
    selectorInput: any;
    selectorButton: any;
    selectorSlide: any;
    plusBtn : any;
    minusBtn : any;

    constructor ( options : RangeSelectorConfig ) {
        this.value = options.min;
        this.minValue = options.min;
        this.maxValue = options.max;
        this.selectorInput = $( options.input );
        this.selectorButton = $( options.btn );
        this.selectorSlide = $( options.slide );
        this.selectorDisplay = $( options.display );
        this.plusBtn = $( options.plus );
        this.minusBtn = $( options.minus );
        this.changeValue ( options.startValue - options.min );
        this.activate( );

    }

    getValue ( ) {
        return this.value;
    }

    changeValue ( val ) {
        let buttonCoords = this.getCoords(this.selectorButton);
        let sliderCoords = this.getCoords(this.selectorSlide);
        let valuePeriod = Number(this.maxValue) - Number(this.minValue);
        let step = Number(this.selectorSlide.outerWidth() - this.selectorButton.outerWidth()) / Number(valuePeriod);

        
        let value = this.value + val;
        
        if ( value >= this.minValue && value <= this.maxValue ) {
            let newLeft = buttonCoords.left - sliderCoords.left + Number(step) * val;
            this.selectorInput.val(value);
            this.value = value;
            this.selectorDisplay.html(String(value).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
            this.selectorButton.css({
                'left': newLeft + 'px'
            });
        }
    }

    activate ( ) {

        this.plusBtn.on('click', ( ) => {
            this.changeValue( 1 );
        });

        this.minusBtn.on('click', ( ) => {
            this.changeValue( -1 );
        });

        this.selectorButton.on('mousedown', (e) => {
            
            let buttonCoords = this.getCoords(this.selectorButton);
            let shiftX = e.pageX - buttonCoords.left;
            let sliderCoords = this.getCoords(this.selectorSlide);

            $(document).on('mousemove', (e) => {
                let newLeft = e.pageX - shiftX - sliderCoords.left;

                if (newLeft < 0) {
                    newLeft = 0;
                }
                let rightEdge = this.selectorSlide.outerWidth() - this.selectorButton.outerWidth();
                
                if (newLeft > rightEdge) {
                    newLeft = rightEdge;
                }

                let valuePeriod = Number(this.maxValue) - Number(this.minValue);
                let step = Number(this.selectorSlide.outerWidth() - this.selectorButton.outerWidth()) / Number(valuePeriod);
                let value = Number(this.minValue) + parseFloat((Number(newLeft) / Number(step)).toFixed(0));
                this.selectorInput.val(value);
                this.selectorDisplay.html(String(value).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
                this.value = value;
                
                this.selectorButton.css({
                    'left': newLeft + 'px'
                })
                
            });

            $(document).on('mouseup', (e) => {
                $(document).off('mousemove');
                $(document).off('mouseup');
            });

            return false;

        });

    }

    getCoords(elem) {
        var box = elem.offset();
        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        };
    }
    
}

export class AccountCouponPage {
    
    header: any ;

    footer: any ;

    constructor ( ) {
        
        this.header = new Header ( );

        this.footer = new Footer ( );

        this.couponBuyForm ( );

        this.couponListSelect ( );

    }

    couponBuyForm ( ) {
        let slideSelectro = new RangeSelector( {
            startValue : 1843,
            min : 1000,
            max : 5000,
            display : '.js-coupon-selector-display',
            input : '.js-coupon-selector-input',
            btn : '.js-coupon-selector-btn',
            slide : '.js-coupon-selector-slide',
            plus : '.js-coupon-selector-plus',
            minus : '.js-coupon-selector-minus'
        } )
    }

    couponListSelect ( ) {
        $(document).ready( ( ) => {
            $(`.js-coupon-list-window[list="active"]`).addClass('account-coupon_coupons-active');
            $('.js-coupon-list-btn[open="active"]').addClass('account_menu_list_item-active');
            $('.js-coupon-list-btn').on('click', function( ) {
                $('.js-coupon-list-btn').removeClass('account_menu_list_item-active');
                $(this).addClass('account_menu_list_item-active');
                $('.js-coupon-list-window').removeClass('account-coupon_coupons-active');
                $(`.js-coupon-list-window[list="${this.getAttribute('open')}"]`).addClass('account-coupon_coupons-active');
            });
        });
    }
}
