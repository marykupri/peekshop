import * as Inputmask from 'inputmask';
import { ModalOpenInterface, ModalOpen, WindowScroll } from './modules/modal-open/modal-open';
import { ProductViewSlider, ProductViewSliderConfig } from './modules/sliders/product-view-slider';
import { Select, SelectInitConfig } from './modules/form-elements/select';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;


export class ProductViewPage {
    
    header: any ;

    footer: any ;

    scrollLocker = new WindowScroll();
    

    constructor () {
        
        this.header = new Header();

        this.footer = new Footer();

        this.subscribeMail( );

        this.infoBlocks( );
        
        this.sizeTable();

    }

    sizeTable() {

        $('.js-size-table-open-btn').on('click', () => {
            $('.js-size-table').addClass('modal-active');
            this.scrollLocker.block();
        })
        $('.js-size-table-cls-btn').on('click', () => {
            $('.js-size-table').removeClass('modal-active');
            this.scrollLocker.unblock();
        })
    }


    subscribeProductReceiptsNotifyConfig : ModalOpenInterface = {
        modalWindow: {
            regular: '.js-subscribe-product-receipts',
            active: 'modal-active'
        },
        clsBtn: '.js-subscribe-product-receipts-close',
        openBtn: '.js-subscribe-product-receipts-open'
    }
    subscribeProductReceiptsNotifyModal = new ModalOpen( this.subscribeProductReceiptsNotifyConfig, ( ) => {
        $('.js-product-view-size-select-list').removeClass('size-select_dropdown-active');
    });

    subscribeMail ( ) {
        $(document).ready( ( ) => {
            
            let form = $('subscribe-product-receipts-form');
            let inputMail = $('.js-subscribe-product-receipts-mail');
            let sendBtn = $('.js-subscribe-product-receipts-send');
            sendBtn.attr('disabled', 'disabled');

            Inputmask('email').mask('.js-subscribe-product-receipts-mail');

            inputMail.on('input', ( e ) => {
                if ( $(e.target).val().substr(-1) !== '_' && $(e.target).val().length !== 0 ) {
                    sendBtn.removeAttr('disabled');
                } else {
                    sendBtn.attr('disabled', 'disabled');
                }

                inputMail.css({
                    border: (( inputMail.val().substr(-1) === '_' ) ? '1px solid red' : '1px solid #bdbdbd')
                });
            });

        });
    }

    infoBlocks ( ) {
        $(document).ready( ( ) => {
            $(`.js-info-window[window="1"]`).addClass('product-description_description_info-active');
            $('.js-info-select[open="1"]').addClass('product-description_description_title-active');
            $('.js-info-select').on('click', function( ) {
                $('.js-info-select').removeClass('product-description_description_title-active');
                $(this).addClass('product-description_description_title-active');
                $('.js-info-window').removeClass('product-description_description_info-active');
                $(`.js-info-window[window="${this.getAttribute('open')}"]`).addClass('product-description_description_info-active');
            });
        });
    }

    productSliderConfig : ProductViewSliderConfig = {
        sliderBlock : '.js-product-slider',
        viewFullclassName : '.js-product-slider-full-img',
        items : {
            className : '.js-product-slider-item',
            activeClassName : 'product-description_gallery_photo-active'
        }
    }
    productSlider = new ProductViewSlider( this.productSliderConfig );

    selectSizeConfig : SelectInitConfig = {
        clickedBlock : {
            className : '.js-product-view-size-select-clicked',
            result : {
                className : '.js-product-view-size-select'
            }
        },
        list : {
            className : '.js-product-view-size-select-list',
            activeClassName : 'size-select_dropdown-active',
            options : {
                className : '.js-product-view-size-select-item'
            }
        }
    }
    selectSize = new Select( ).init( this.selectSizeConfig );
}
