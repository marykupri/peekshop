import * as Inputmask from 'inputmask';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;

export class HelpPage {
    
    header: any ;

    footer: any ;

    constructor () {
        
        this.header = new Header();

        this.footer = new Footer();

        $(document).ready(() => {
            $('.js-question-submit').attr('disabled', 'disabled');
            Inputmask('email', {
                oncomplete : ( ) => {
                    $('.js-question-submit').removeAttr('disabled');
                }
            }).mask(".js-question-input");
            $(document).on('click', '.js-question-item-btn', function ( ) {
                $(this).closest('.js-question-item').toggleClass('help-simple_content_list_item-active');
            });
        });
        
    }
}