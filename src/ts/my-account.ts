import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;

export class MyAccountPage {
    
    header: any ;

    footer: any ;

    constructor ( ) {
        
        this.header = new Header( );

        this.footer = new Footer( );

        this.listBlocks( )
    }

    listBlocks ( ) {
        $(document).ready( ( ) => {
            $(`.js-account-list-view[list="active"]`).addClass('my-account_orders_goods-active');
            $('.js-account-list-select[open="active"]').addClass('account_menu_list_item-active');
            $('.js-account-list-select').on('click', function( ) {
                $('.js-account-list-select').removeClass('account_menu_list_item-active');
                $(this).addClass('account_menu_list_item-active');
                $('.js-account-list-view').removeClass('my-account_orders_goods-active');
                $(`.js-account-list-view[list="${this.getAttribute('open')}"]`).addClass('my-account_orders_goods-active');
            });
        });
    }
}