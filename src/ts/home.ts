import * as Inputmask from 'inputmask';
import { ModalOpenInterface } from './modules/modal-open/modal-open';
import { SliderHomeInterface } from './modules/sliders/slider-home';
import { SliderHome } from './modules/sliders/slider-home';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
import { ModalOpen } from './modules/modal-open/modal-open';

declare let $ : any;

export class HomePage {
    
    header: any ;

    footer: any ;

    slider: any ;

    subscribeModal: any;

    sliderOptions: SliderHomeInterface = {
        sliderId: 'slider-home',
        slides: {
            name: '.slider_slide',
            active: 'slider_slide-active',
            disabled: 'slider_slide-disabled'
        },
        btns: {
            active: 'slider_bar_btn-active',
            regular: 'slider_bar_btn'
        },
        nav: 'slider_bar',
        period: 5000
    };

    subscribeModalOptions: ModalOpenInterface = {
        modalWindow: {
            regular: '.js-home-subscribe-modal',
            active: 'modal-active'
        },
        clsBtn: '.js-home-subscribe-close',
        openBtn: '.js-home-subscribe-btn'
    }
    

    constructor ( ) {
        this.header = new Header();

        this.footer = new Footer();

        this.slider = new SliderHome(this.sliderOptions);
        
        this.newsSubscribe( );
    }


    newsSubscribe ( ) {
        // let isMailValid = false;
        // $('.js-home-subscribe-btn').css('cursor', 'not-allowed');
        // Inputmask('email', {
        //     oncomplete: () => {
               $('.js-home-subscribe-btn').css('cursor', 'pointer');
        //        isMailValid = true;
        //     }
        // }).mask('.js-home-subscribe-input');

        // if($('.js-home-subscribe-input').val().length > 0 ) {
        //     $('.js-home-subscribe-btn').css('cursor', 'pointer');
        //     isMailValid = true;
        // }
        this.subscribeModal = new ModalOpen(this.subscribeModalOptions);
        this.subscribeModal.insteadOpenWindow(() => {
            // if (isMailValid) {
                var data = $('.js-home-subscribe-input').val();
                if(data.length > 0 ) {
                    let url = '/index.php?route=common/subscribe';
                    $.post(url, {'email': data}, (res) => {
                        // console.log(res);
                    });

                    //this.subscribeModal.undisableOpenBtn();
                    this.subscribeModal.openModal();
                    $('.js-home-subscribe-input').val('');
                    this.newsSubscribe();
                }else{
                    $('.js-home-subscribe-input').css('border','1px solid red');
                }
            // }
        });
    }
}

