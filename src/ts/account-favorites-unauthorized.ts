import  * as Inputmask from 'inputmask';
import { ModalOpenInterface, ModalOpen } from './modules/modal-open/modal-open';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;

export class AccountFavoritesUnauthorizedPage {
    
    header: any ;

    footer: any ;

    subscribeProductReceiptsNotifyConfig : ModalOpenInterface = {
        modalWindow: {
            regular: '.js-subscribe-product-receipts',
            active: 'modal-active'
        },
        clsBtn: '.js-subscribe-product-receipts-close',
        openBtn: '.js-subscribe-product-receipts-open'
    }
    subscribeProductReceiptsNotifyModal = new ModalOpen( this.subscribeProductReceiptsNotifyConfig );



    constructor ( ) {
        
        this.header = new Header( );

        this.footer = new Footer( );

        this.subscribeMail( );

    }

    subscribeMail ( ) {
        $(document).ready( ( ) => {
            
            let form = $('subscribe-product-receipts-form');
            let inputMail = $('.js-subscribe-product-receipts-mail');
            let sendBtn = $('.js-subscribe-product-receipts-send');
            sendBtn.attr('disabled', 'disabled');

            Inputmask('email').mask('.js-subscribe-product-receipts-mail');

            inputMail.on('input', ( e ) => {
                if ( $(e.target).val().substr(-1) !== '_' && $(e.target).val().length !== 0 ) {
                    sendBtn.removeAttr('disabled');
                } else {
                    sendBtn.attr('disabled', 'disabled');
                }

                inputMail.css({
                    border: (( inputMail.val().substr(-1) === '_' ) ? '1px solid red' : '1px solid #bdbdbd')
                });
            });

        });
    }
}