import { ModalOpenInterface } from './modules/modal-open/modal-open';
import { ModalOpen } from './modules/modal-open/modal-open';
import * as Inputmask from 'inputmask';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;

export class HomeSubscribePage {
    
    header: any ;

    footer: any ;

    subscribeModal: any;

    subscribeModalOptions: ModalOpenInterface = {
        modalWindow: {
            regular: '.js-home-subscribe-modal',
            active: 'modal-active'
        },
        clsBtn: '.js-home-subscribe-close',
        openBtn: '.js-home-subscribe-btn'
    }

    constructor ( ) {
        
        this.header = new Header( );

        this.footer = new Footer( );

        this.newsSubscribe( );
    }

    newsSubscribe ( ) {
        // let isMailValid = false;
        // $('.js-home-subscribe-btn').css('cursor', 'not-allowed');
        // Inputmask('email', {
        //     oncomplete: () => {
                $('.js-home-subscribe-btn').css('cursor', 'pointer');
        //         isMailValid = true;
        //     }
        // }).mask('.js-home-subscribe-input');
        this.subscribeModal = new ModalOpen(this.subscribeModalOptions);
        this.subscribeModal.insteadOpenWindow(() => {
            // if (isMailValid) {
            var data = $('.js-home-subscribe-input').val();
            if(data.length > 0 ) {
                let url = '/index.php?route=common/subscribe';
                $.post(url, {'email': data}, (res) => {
                    // console.log(res);
                });

                //this.subscribeModal.undisableOpenBtn();
                this.subscribeModal.openModal();
                $('.js-home-subscribe-input').val('');
                this.newsSubscribe();
            }else{
                $('.js-home-subscribe-input').css('border','1px solid red');
            }
            // }
        });



    }
}