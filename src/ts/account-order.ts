import * as Inputmask from 'inputmask';
import { ProductViewSlider, ProductViewSliderConfig } from './modules/sliders/product-view-slider';
import { Select, SelectInitConfig } from './modules/form-elements/select';
import { ModalOpenInterface, ModalOpen } from './modules/modal-open/modal-open';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
declare let $ : any;

export class AccountOrderPage {
    
    header: any ;

    footer: any ;

    quickViewWindowConfig : ModalOpenInterface = {
        modalWindow : {
            regular : '.js-product-quick-view',
            active : 'modal-active'
        },
        openBtn : '.js-product-quick-view-open',
        clsBtn : '.js-product-quick-view-close'
    }
    quickViewWindow = new ModalOpen( this.quickViewWindowConfig );

    constructor ( ) {
        
        this.header = new Header( );

        this.footer = new Footer( );

        this.subscribeMail( );

        this.quickViewWindow.insteadOpenWindow((e) => {
            let id = $(e).data('id');
            console.log(id);

            $.post("index.php?route=account/order/getInfoOrderproduct", {product_id: id})
                .done((data) => {

                    this.sizeSelect.rebind();
                    this.previewSlider.rebind();

                    $(".quick-view .product-description").html(data);

                    this.sizeSelect.init(this.sizeSelectConfig);
                    this.previewSlider.init();

                    // $('.js-order-to-busket').off('click');
                    // $('.js-order-to-busket').on('click', () => {
                    //     console.log(this.sizeSelect.getCurrentValue());
                    //     console.log(this.sizeSelect.isSelected());
                    // });

                    this.quickViewWindow.openModal();
                });

        });

    }

    previewSliderConfig : ProductViewSliderConfig = {
        sliderBlock : '.js-product-view-slider',
        viewFullclassName : '.js-product-view-slider-full-img',
        items : {
            className : '.js-product-view-slider-item',
            activeClassName : 'product-description_gallery_photo-active'
        }
    }
    previewSlider = new ProductViewSlider( this.previewSliderConfig );
    
    sizeSelectConfig : SelectInitConfig = {
        clickedBlock : {
            className : '.js-product-view-size-select-clicked',
            result : {
                className : '.js-order-size-select'
            }
        },
        list : {
            className : '.js-order-size-select-list',
            activeClassName : 'size-select_dropdown-active',
            options : {
                className : '.js-order-size-select-item'
            }
        }
    }

    sizeSelect = new Select( ).init(this.sizeSelectConfig);

    subscribeProductReceiptsNotifyConfig : ModalOpenInterface = {
        modalWindow: {
            regular: '.js-subscribe-product-receipts',
            active: 'modal-active'
        },
        clsBtn: '.js-subscribe-product-receipts-close',
        openBtn: '.js-subscribe-product-receipts-open'
    }
    subscribeProductReceiptsNotifyModal = new ModalOpen( this.subscribeProductReceiptsNotifyConfig, ( ) => {
        $('.js-product-view-size-select-list').removeClass('size-select_dropdown-active');
    });

    subscribeMail ( ) {
        $(document).ready( ( ) => {
            
            let form = $('subscribe-product-receipts-form');
            let inputMail = $('.js-subscribe-product-receipts-mail');
            let sendBtn = $('.js-subscribe-product-receipts-send');
            sendBtn.attr('disabled', 'disabled');

            Inputmask('email').mask('.js-subscribe-product-receipts-mail');

            inputMail.on('input', ( e ) => {
                if ( $(e.target).val().substr(-1) !== '_' && $(e.target).val().length !== 0 ) {
                    sendBtn.removeAttr('disabled');
                } else {
                    sendBtn.attr('disabled', 'disabled');
                }

                inputMail.css({
                    border: (( inputMail.val().substr(-1) === '_' ) ? '1px solid red' : '1px solid #bdbdbd')
                });
            });

        });
    }
}