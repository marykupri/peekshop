
import { Header } from './modules/header/header';

import { Footer } from './modules/footer/footer';


export class BrandsPage {
    
    header: any ;

    footer: any ;

    constructor () {
        
        this.header = new Header();

        this.footer = new Footer();
    }
}