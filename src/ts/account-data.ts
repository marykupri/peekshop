import { Select, SelectCreateConfig, SelectInitConfig } from './modules/form-elements/select';
import { Header } from './modules/header/header';
import { Footer } from './modules/footer/footer';
import * as Inputmask from "inputmask";
declare let $: any;

export class AccountDataPage {
    
    header: any ;

    footer: any ;

    
    selectClothesConfig : SelectInitConfig = {
        clickedBlock : {
            className : '.js-account-size-clothes',
            result : {
                className : '.js-account-size-clothes-input'
            }
        },
        list : {
            className : '.js-account-size-clothes-list',
            activeClassName : 'account-data_size_select-size_list-active',
            options : {
                className : '.js-account-size-clothes-item'
            }
        }
    }
    selectClothes = new Select( ).init(this.selectClothesConfig);


    selectShoesConfig : SelectInitConfig = {
        clickedBlock : {
            className : '.js-account-size-shoes',
            result : {
                className : '.js-account-size-shoes-input'
            }
        },
        list : {
            className : '.js-account-size-shoes-list',
            activeClassName : 'account-data_size_select-size_list-active',
            options : {
                className : '.js-account-size-shoes-item'
            }
        }
    }
    selectShoes = new Select( ).init( this.selectShoesConfig );

    
    selectAddressConfig : SelectInitConfig = {
        clickedBlock : {
            className : '.js-account-address',
            result : {
                className: '.js-account-address-input'
            }
        },
        list : {
            className : '.js-account-address-list',
            activeClassName : 'account-data_choose-location_list-active',
            options : {
                className : '.js-account-address-item'
            }
        }
    }
    selectAddress = new Select( ).init( this.selectAddressConfig );



    constructor () {
        
        this.header = new Header();

        this.footer = new Footer();

        this.textInInput();

        this.addAddress();

        this.inputValidate( );

    }

    textInInput ( ) {
        $( document ).ready( () => {
            let inputs = document.querySelectorAll('.account-data_field_personal-data');
            let inputsArr = Array.prototype.slice.call(inputs);

            inputsArr.forEach( (item) => {

                if(item.value.length >= 1) {
                    item.nextElementSibling.classList.add('account-data_field_indicator-active')
                }

                item.onblur = () => {
                    if(item.value.length >= 1) {
                        item.nextElementSibling.classList.add('account-data_field_indicator-active')
                    } else {
                        item.nextElementSibling.classList.remove('account-data_field_indicator-active')
                    }
                }

            });
        })
    }

    inputValidate ( ) {
        Inputmask({mask: "99/99/9999", placeholder: "дд/мм/гггг", showMaskOnHover: false}).mask('.js-born-date');
        Inputmask({mask: "+7(999)999-99-99", showMaskOnHover: false}).mask('.js-phone-delivery');
    }

    addAddress () {
        $( document ).ready( () => {
            let newAddress = document.createElement('li');
            let parent = document.querySelector('.account-data_form_addresses_wrap');
            let addBtn = document.querySelector('.account-data_add-address');

            let count = 0;

            newAddress.className = "account-data_form_address";
            newAddress.innerHTML = `
                                    <label class="account-data_field">

                                        <input type='text' class="account-data_field_personal-data" name="address[][country]">
                                        <p class="account-data_field_indicator">Страна</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[][city]">
                                        <p class="account-data_field_indicator">Город</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[][postcode]">
                                        <p class="account-data_field_indicator">Индекс</p>

                                    </label>

                                    <label class="account-data_field">

                                        <input type='text' class="account-data_field_personal-data" name="address[][address_1]">
                                        <p class="account-data_field_indicator">Улица</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[][house_number]">
                                        <p class="account-data_field_indicator">Дом</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[][apartment_number]">
                                        <p class="account-data_field_indicator">кв./офис</p>

                                    </label>`

                                    //<a class="account-data_form_address_delete js-address-delete">Сохранить</a>`

            addBtn.addEventListener("click", () => {
                // parent.appendChild(newAddress.cloneNode(true));

                count = count - 1;

                let tpl = `
                                    <label class="account-data_field">

                                        <input type='text' class="account-data_field_personal-data" name="address[` + count + `][country]">
                                        <p class="account-data_field_indicator">Страна</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[` + count + `][city]">
                                        <p class="account-data_field_indicator">Город</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[` + count + `][postcode]">
                                        <p class="account-data_field_indicator">Индекс</p>

                                    </label>

                                    <label class="account-data_field">

                                        <input type='text' class="account-data_field_personal-data" name="address[` + count + `][address_1]">
                                        <p class="account-data_field_indicator">Улица</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[` + count + `][house_number]">
                                        <p class="account-data_field_indicator">Дом</p>

                                    </label>

                                    <label class="account-data_field account-data_field-width">

                                        <input type='text' class="account-data_field_personal-data" name="address[` + count + `][apartment_number]">
                                        <p class="account-data_field_indicator">кв./офис</p>

                                    </label>

                                    <span class="account-data_form_address_delete js-address-delete_nosave">Удалить</span>`;

                $(".account-data_form_addresses_wrap").append('<li class="account-data_form_address">' + tpl + '</li>');
            })
        })
    }
}