<?php
date_default_timezone_set('Europe/Moscow');
require_once 'db-classes.php';
$DB = new Database();

$sql = "SELECT p.product_id, pd.name, p.model, pd.description, ovd.name as color, m.name as manufacturer
        FROM oc_product p 
        join oc_product_description pd on p.product_id = pd.product_id and pd.`language_id` = 2
        join oc_product_option_value pov on p.product_id = pov.product_id and pov.option_id = 13
        join oc_option_value_description ovd on pov.option_value_id = ovd.option_value_id and ovd.language_id = 2
        join oc_manufacturer m on p.manufacturer_id = m.manufacturer_id
        where p.product_id > 205";
$query = $DB->query($sql);
$query = $DB->rows($query);

//echo '<pre>';
//var_dump($query);
//echo '</pre>';
//die();

$FlNm = __DIR__ . '/../temp/dataItem.csv';

$Handl=fopen($FlNm, 'w');
if($Handl)
{
    foreach($query as $item => $value)
    {
        $sql = "select pod.name
                from oc_product_option_value pov
                left join oc_option_value_description pod on pov.option_value_id = pod.option_value_id and pod.language_id = 2
                where pov.option_id = 11 and pov.product_id =  " . $value['product_id'];

        $sizes = $DB->query($sql);
        $sizes = $DB->rows($sizes);

        $arr = array();
        foreach ($sizes as $size){
            $arr[] = $size['name'];
        }

        $sizes = implode(',' , $arr);

        $descr = str_replace(array(";","&lt;li&gt;", "&lt;/li&gt;","amp;","&lt;/ul&gt;","nbsp;","&amp;", "&#39;", "&quot;"),array("//"," - ", " ","&",""), $value['description']);
        $descr = preg_replace('/&lt;ul class=product-infoList--twoCol&gt;/','', $descr);

//        $short_descr = str_replace(array("nbsp;","&amp;", "&#39;", ";","amp;","&quot;"),array(" ","&","'","//"," - ", ""), $value['short_descr']);

        $brand = htmlentities($value['manufacturer']);
        $brand = str_replace(array("&amp;", "&#39;","amp;","&quot;"),array("&",""," - ", ""), $value['manufacturer']);

//        $stucture = str_replace(array("&amp;"),array("&"), $value['stucture']);

        $value['name'] = str_replace(array("&quot;","&amp;","&#39;"),array(" ","&",""), $value['name']);
        $value['manufacturer'] = str_replace(array("&#39;"),array("&"), $value['manufacturer']);

        $Strz =trim($value['name']).';';
        $Strz .=' ;';
        $Strz .=trim($value['model']).';';
        $Strz .=trim($brand).';';
        $Strz .=trim($value['color']).';';
        $Strz .=' ;';
        $Strz .=trim($sizes).';';
        $Strz .=' ;';
//        $Strz .=trim($short_descr).';';
        $Strz .=trim($descr).';';
        $Strz .=' ;';
//        $Strz .=trim($stucture).';';
        $Strz .="\r\n";

//        $Strz = iconv('windows-1251','utf-8' ,$Strz);

//        $Strz = iconv("UTF-8", "ISO-8859-1", $Strz);

//        var_dump($Strz);
//        die;

        fwrite($Handl,$Strz);
    }
    fclose($Handl);
}

$filename = 'dataItem.csv';
header("Content-type: application/x-download");
header("Content-Disposition: attachment; filename=$filename");
readfile(__DIR__ . '/../temp/'.$filename);
unlink(__DIR__ . '/../temp/'.$filename);

?>