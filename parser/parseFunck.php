<?php
error_reporting(E_ALL);

require_once "db-classes.php";

$db = new Database();

$size_code_arr = getSize();
$color_code_arr = getColor();
$material_code_arr = getMaterial();

function get_brand_id($brand){
    global $db;
    if(empty($brand)) return '';

    $brand = htmlspecialchars_decode(trim($brand),ENT_QUOTES);
    $brand = str_replace("'","\'",$brand);
    $sql = "select manufacturer_id from oc_manufacturer where name like '$brand'";
    $res = $db->query($sql);
    $row = $db->row($res);

    if(!$row && !empty($brand)){
        $brand = str_replace("'","\'",$brand);
        $sql = "INSERT INTO `oc_manufacturer`(`name`, `sort_order`) VALUES ('$brand',0)";
        $db->query($sql);
        $id = $db->insert_id;
        $sql = "INSERT INTO `oc_manufacturer_to_store`(`manufacturer_id`, `store_id`) VALUES ($id,0)";
        $db->query($sql);
        return $id;
    }

    $id = $row['manufacturer_id'];

    return $id;
}

function insertItem($data){

    if(empty($data)) return;
//    if(!$itemId = checkProductExist($data['model']))
        $itemId = addItem($data);
//    else
//        updateItem($itemId,$data);
        if($itemId) {
            insertItemOption($data, $itemId);
//        insertItemFilter($data, $itemId);
            updateImages($data, $itemId);
            return $itemId;
        }

        return false;
}

function updateItem($id,$data){
    global $db;
    if(empty($id)) return;

    $sql = "UPDATE " . DB_PREFIX . "product
        SET status = '" . (int)$data['status'] . "', price = '" . (float)$data['price'] . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', 
        parent_shop_id = " . (int)$data['id_shop'] . ", parent_shop_product_id = " . (int)$data['id_link'] . ", date_modified = now()
        WHERE product_id='".$id."'";

    $db->query($sql);

    foreach ($data['description'] as $language_id =>$value) {
        $sql = "UPDATE `oc_product_description` SET  `name`='" . $value['name'] . "' WHERE `product_id`=$id and `language_id`=1";
        $db->query($sql);
    }

    insertItemOption($data, $id);
//    insertItemFilter($data, $id);
    updateImages($data, $id);
}

function getSize() {
    global $db;

    $sql = "select vd.option_value_id, vd.name
        from " . DB_PREFIX . "option_value_description as vd
        WHERE language_id = 1
        AND option_id = 11";

    $data = getAllOptions($sql);

    return $data;
}

function getColor() {
    global $db;

    $sql = "select vd.option_value_id, vd.name
        from " . DB_PREFIX . "option_value_description as vd
        WHERE language_id = 1
        AND option_id = 13";

    $data = getAllOptions($sql);

    return $data;
}

function getMaterial() {
    global $db;

    $sql = "select vd.option_value_id, vd.name
        from " . DB_PREFIX . "option_value_description as vd
        WHERE language_id = 1
        AND option_id = 14";

    $data = getAllOptions($sql);

    return $data;
}

function getAllOptions($sql){
    global $db;

    $result_db = $db->query($sql);

    $data = array();

    while ($item = $result_db->fetch_array()) {
        $name = strtolower(trim($item['name']));
        $data[$item['option_value_id']] = $name;
    }

    return $data;
}

function addItem($data){
    global $db;

    $sql ="INSERT INTO `oc_product`(`model`,`quantity`, `stock_status_id`,`manufacturer_id`,`shipping`, `price`,`tax_class_id`, `minimum`, `status`,`parent_shop_id`,`parent_shop_product_id`,`date_added`,`date_modified`) 
            VALUES ('" . $db->sql($data['model']) . "'," . (int)$data['quantity'] . "," . (int)$data['stock_status_id'] . "," . (int)$data['manufacturer_id'] . "," . (int)$data['shipping'] . "," . (float)$data['price'] . "," . (int)$data['tax_class_id'] . "," . (int)$data['minimum'] . "," . (int)$data['status'] . "," . (int)$data['id_shop'] . "," . (int)$data['id_link'] . ",NOW(),NOW())";

    $db->query($sql);
    $product_id = $db->insert_id;

    foreach ($data['description'] as $language_id =>$value) {
        $sql ="INSERT INTO `oc_product_description`(`product_id`, `language_id`, `name`, `description`,`detail`,`material`,`meta_title`)
                VALUES (" . (int)$product_id . ",1,'" . $value['name'] . "','" . $value['description'] . "','" . $value['details'] . "','" . $value['material'] . "','" . $value['short_descr'] . "')";

        $db->query($sql);

        $sql ="INSERT INTO `oc_product_description`(`product_id`, `language_id`, `name`, `description`,`detail`,`material`,`meta_title`)
                VALUES (" . (int)$product_id . ",2,'" . $value['name'] . "','" . $value['description'] . "','" . $value['details'] . "','" . $value['material'] . "','" . $value['short_descr'] . "')";

        $db->query($sql);
    }

    // таблица привязки продукта к товару
    $sql = "INSERT into " . DB_PREFIX . "product_to_store values ('" . (int)$product_id . "','0')";
    $db->query($sql);

    if ($data['category']) {
        // определение родительских категорий
        $sql = "SELECT * from oc_category_path as cp
            where category_id in ('" . (int)$data['category'] . "')";// and `level` > 0
        $res = $db->query($sql);

        while ($item = $res->fetch_array()) {
            // таблица привязки категории к продукту
            $sql = "INSERT into " . DB_PREFIX . "product_to_category values('" . (int)$product_id . "','" . (int)$item['path_id'] . "')";
            $db->query($sql);
        }
    }

    return $product_id;
}

function checkProductExist($code) {
    global $db;

    $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE model = '".$code."' LIMIT 1";
    $res = $db->query($sql);
    $product = $res->fetch_array();
    return isset($product['product_id']) ? $product['product_id'] : false;
}

function checkProductImgsExist($code) {
    global $db;

    $sql = "SELECT image FROM " . DB_PREFIX . "product WHERE model = '".$code."' LIMIT 1";
    $res = $db->query($sql);
    $product = $res->fetch_array();
    return !empty($product['image']) ? true : false;
}

function insertItemOption($data,$product_id){
    global $db;

    if($data['item_size']) {

        $option_id = 11;

        $sql = "SELECT product_option_id 
                    from oc_product_option 
                      where product_id = " . $product_id . " and option_id = " . $option_id;
        $query = $db->query($sql);
        $row = $db->row($query);

        if (!$row) {
            $sql = "INSERT INTO `oc_product_option` (`product_id`, `option_id`, `required`) VALUES ('" . $product_id . "', $option_id, 1)";
            $db->query($sql);

            $product_option_id = $db->insert_id;
        } else
            $product_option_id = $row['product_option_id'];

        $sql = "UPDATE `oc_product_option_value` 
                  SET `active`=0 
                    WHERE `product_id`= $product_id and `product_option_id`= $product_option_id";
        $db->query($sql);

        foreach ($data['item_size'] as $size_code) {
            $option_value_id = getOptionValueId($size_code['name'],$option_id);
//            updateSizeCode($option_value_id,$size_code['code']);
            $size_code_id = getSizeCodeId($size_code['code']);
            addProductOption($product_id,$option_id, $option_value_id, $product_option_id, $data,$size_code_id);
        }
    }

    if(isset($data['item_color']) && !empty($data['item_color'])) {

        $option_id = 13;

        $sql = "SELECT product_option_id from oc_product_option where product_id = " . $product_id . " and option_id = " . $option_id;
        $query = $db->query($sql);
        $row = $db->row($query);

        if (!$row) {
            $sql = "INSERT INTO `oc_product_option` (`product_id`, `option_id`, `required`) VALUES ('" . $product_id . "', $option_id, 0)";
            $db->query($sql);

            $product_option_id = $db->insert_id;
        } else
            $product_option_id = $row['product_option_id'];

        $sql = "UPDATE `oc_product_option_value` SET `active`=0 
                  WHERE `product_id`= $product_id and `product_option_id`= $product_option_id";
        $db->query($sql);

        foreach ($data['item_color'] as $color_code) {
            $option_value_id = getOptionValueId($color_code,$option_id);
            addProductOption($product_id,$option_id, $option_value_id, $product_option_id, $data);
        }
    }

    if(isset($data['materials']) && !empty($data['materials'])) {

        $option_id = 14;

        $sql = "SELECT product_option_id from oc_product_option where product_id = " . $product_id . " and option_id = " . $option_id;
        $query = $db->query($sql);
        $row = $db->row($query);

        if (!$row) {
            $sql = "INSERT INTO `oc_product_option` (`product_id`, `option_id`, `required`) VALUES ('" . $product_id . "', $option_id, 0)";
            $db->query($sql);

            $product_option_id = $db->insert_id;
        } else
            $product_option_id = $row['product_option_id'];

        $sql = "UPDATE `oc_product_option_value` SET `active`=0 
                  WHERE `product_id`= $product_id and `product_option_id`= $product_option_id";
        $db->query($sql);

        foreach ($data['materials'] as $material_code) {
            $option_value_id = getOptionValueId($material_code,$option_id);
            addProductOption($product_id,$option_id, $option_value_id, $product_option_id, $data);
        }
    }
}

function insertItemFilter($data,$product_id){
    global $db;

    if(isset($data['manufacturer_id'])){

        $filter_id = '';
        $sql = "select fd.filter_id from oc_filter_description fd join oc_manufacturer m on m.name = fd.name where m.manufacturer_id = " . $data['manufacturer_id'] . " limit 1";
        $result = $db->query($sql);
        $res = $result->fetch_array();

        if(!$res){
            $sql = "select name from oc_manufacturer where manufacturer_id = " . $data['manufacturer_id'];
            $result = $db->query($sql);
            $name = $result->fetch_array();

            $filter_id = addFilterInGroup(3,$name['name']);

        }else{
            $filter_id = $res['filter_id'];
        }

        $sql = "REPLACE INTO `oc_product_filter`(`product_id`, `filter_id`) VALUES ($product_id,$filter_id)";
        $db->query($sql);
    }

    if(isset($data['item_color']) && !empty($data['item_color'])){

        foreach ($data['item_color'] as $color) {
            $color = trim($color);
            $sql = "select fd.filter_id from oc_filter_description fd where fd.name = '" . $color . "' limit 1";

            $result = $db->query($sql);
            $res = $result->fetch_array();

            if (!$res) {
                $filter_id = addFilterInGroup(2, $color);
            } else {
                $filter_id = $res['filter_id'];
            }

            $sql = "REPLACE INTO `oc_product_filter`(`product_id`, `filter_id`) VALUES ($product_id,$filter_id)";
            $db->query($sql);
        }
    }

    if(isset($data['item_size']) && !empty($data['item_size'])){

        foreach ($data['item_size'] as $size => $val) {
            $sql = "select fd.filter_id from oc_filter_description fd where fd.name = '" . $val['name'] . "' limit 1";

            $result = $db->query($sql);
            $res = $result->fetch_array();

            if (!$res) {
                $filter_id = addFilterInGroup(4, $val['name']);
            } else {
                $filter_id = $res['filter_id'];
            }

            $sql = "REPLACE INTO `oc_product_filter`(`product_id`, `filter_id`) VALUES ($product_id,$filter_id)";
            $db->query($sql);
        }
    }

    if(isset($data['materials']) && !empty($data['materials'])){

        foreach ($data['materials'] as $materials => $val) {
            $sql = "select fd.filter_id from oc_filter_description fd where fd.name = '" . $val . "' limit 1";

            $result = $db->query($sql);
            $res = $result->fetch_array();

            if (!$res) {
                $filter_id = addFilterInGroup(5, $val);
            } else {
                $filter_id = $res['filter_id'];
            }

            $sql = "REPLACE INTO `oc_product_filter`(`product_id`, `filter_id`) VALUES ($product_id,$filter_id)";
            $db->query($sql);
        }
    }
}

function addFilterInGroup($group,$name){
    global $db;

    $sql = "INSERT INTO `oc_filter`(`filter_group_id`, `sort_order`) VALUES ($group,0)";
    $db->query($sql);
    $filter_id = $db->insert_id;

    $sql = "INSERT INTO `oc_filter_description`(`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES ($filter_id,1,$group,'" . $name . "')";
    $db->query($sql);
    $sql = "INSERT INTO `oc_filter_description`(`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES ($filter_id,2,$group,'" . $name . "')";
    $db->query($sql);

    return $filter_id;
}

/**
 * Получение option_value_id
 * Сохранение размера, если нет
 * @param $size_code
 * @return bool|int|string
 */
function getOptionValueId($code,$id_option) {
    global $size_code_arr;
    global $color_code_arr;
    global $material_code_arr;

    $code_arr = array();

    $code_srch = strtolower(trim($code));

    if($id_option == 11)
        $code_arr = $size_code_arr;
    if($id_option == 13)
        $code_arr = $color_code_arr;
    if($id_option == 14)
        $code_arr = $material_code_arr;

    if (!array_search($code_srch, $code_arr)) {
        // добавление размера
        global $db;
        $sql = "INSERT INTO `oc_option_value` (`option_id`, `image`, `sort_order`) VALUES ($id_option, 'no_image.jpg', 5)";
        $db->query($sql);

        $code = trim($code);

        $option_id = $db->insert_id;

        $sql = "INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES (" . (int)$option_id . ", 1, $id_option, '" . $db->sql($code) . "');";
        $db->query($sql);
        $sql = "INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES (" . (int)$option_id . ", 2, $id_option, '" . $db->sql($code) . "');";
        $db->query($sql);

//        $size_code_arr[$option_id] = trim($code);
        if($id_option == 11)
            $size_code_arr[$option_id] = trim($code);
        if($id_option == 13)
            $color_code_arr[$option_id] = trim($code);
        if($id_option == 14)
            $material_code_arr[$option_id] = trim($code);

        return $option_id;
    } else {
        foreach ($code_arr as $key=>$color){
            if ($color == $code_srch) {return $key;}
        }
    }

    return false;
}

function addProductOption($product_id,$option_id, $option_value_id, $product_option_id, $data, $size_code_id = null){
    global $db;

    if (!$option_value_id) {return false;}

    $sql="select * from `oc_product_option_value`
            where `product_option_id` = " . (int)$product_option_id . " and
                `product_id` = " . $product_id . " and
                `option_id` = " . $option_id . " and 
                `option_value_id` = " . (int)$option_value_id;

    $result = $db->query($sql);
    $res = $result->fetch_array();

    if(!empty($res)){
        $sql = "UPDATE `oc_product_option_value` SET `active` = 1,`code_size` = " . (int)$size_code_id . "
            WHERE `product_option_value_id` = ".$res['product_option_value_id'];
        $db->query($sql);
        return;
    }

    $sql = "REPLACE INTO `oc_product_option_value`
            (`product_option_id`, `product_id`, `option_id`,
            `option_value_id`,`code_size`,`quantity`, `subtract`,
            `price`, `price_prefix`, `points`,
            `points_prefix`, `weight`, `weight_prefix`)
            VALUES
            (" . (int)$product_option_id . ", " . $product_id . ", " . $option_id . ",
            " . (int)$option_value_id . "," . (int)$size_code_id . "," . (int)$data['quantity'] . ", 1,
            0, '+', 0, '+', 0.00000000, '+')";
    $db->query($sql);
}

function updateSizeCode($option_id,$code){
    global $db;

    $sql = "UPDATE `oc_option_value` SET `size_code`='$code' WHERE `option_value_id` = $option_id";
    $db->query($sql);

}

function getSizeCodeId($code){
    global $db;

    $size_code_id = '';

    $sql = "select id from `oc_option_size_code` where size_code = '" . $code . "'";

    $result = $db->query($sql);
    $res = $result->fetch_array();

    if (!$res) {
        $sql = "INSERT INTO `oc_option_size_code`(`size_code`) VALUES ('" . $code . "')";
        $db->query($sql);
        $size_code_id = $db->insert_id;
    } else {
        $size_code_id = $res['id'];
    }

    return $size_code_id;
}

function updateCategoryFilters(){
    global $db;

    $sql = "select category_id from oc_category_path cp where cp.level = 0";
    $result = $db->query($sql);
    $rows = $db->rows($result);

    foreach ($rows as $category) {
        $sql = "select pf.filter_id from oc_product_filter pf join oc_product_to_category pc on pc.product_id = pf.product_id where pc.category_id = " . $category['category_id'];
        $result = $db->query($sql);
        $filter = $db->rows($result);

        if(!empty($filter)){
            foreach ($filter as $val){
                $sql = "REPLACE INTO `oc_category_filter`(`category_id`, `filter_id`) VALUES (" . $category['category_id'] . ", " . $val['filter_id'] . " )";
                $db->query($sql);
            }
        }
    }
}

function updateImages($data,$itemId){

    global $db;

    if(!empty($data['images'])) {

        foreach ($data['images'] as $image => $img) {

            if ($image == 'main') {
                $sql = "UPDATE `" . DB_PREFIX . "product` SET `image`= 'catalog/product/" . $img . "' where `product_id`=" . $itemId;
                $db->query($sql);
            }

            if ($image == 'slider') {
                $sql = "DELETE FROM `" . DB_PREFIX . "product_image` WHERE product_id = " . $itemId;
                $db->query($sql);

                foreach ($img as $slider => $val) {
                    $sql = "INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$itemId . "', image = 'catalog/product/" . $val . "', sort_order = '" . (int)$slider . "'";
                    $db->query($sql);
                }
            }
        }

    }
}

function startUpdateProducts($type){
    global $db;

    $name = '';
    if($type == 1)
        $name = "Парсер товаров";
    if($type == 2)
        $name = "Парсер ссылок";

    $sql = "INSERT INTO `oc_list_log_update`(`name`,`type`,`data_start`, `active`) VALUES ('$name', $type,NOW(),1)";
    $db->query($sql);
}

function endUpdateProducts($type,$update,$insert){
    global $db;

    $sql = "UPDATE `oc_list_log_update` SET `data_end`=NOW(),`updItems`=$update,`insItems`=$insert,`active`=0 WHERE `active`=1 and `type` = $type";
    $db->query($sql);
}