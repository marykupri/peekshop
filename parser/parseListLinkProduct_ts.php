<?php

use VDB\Spider\Discoverer\XPathExpressionDiscoverer;
use Symfony\Component\EventDispatcher\Event;
use VDB\Spider\Event\SpiderEvents;
use VDB\Spider\StatsHandler;
use VDB\Spider\Spider;

require_once __DIR__ . '../../vendor/autoload.php';
require_once "db-classes.php";

require_once "parseFunck.php";

$db = new Database();

startUpdateProducts('2');
$itemUpdate = 0;
$itemInsert = 0;

$sql = "UPDATE list_link_products_ts set active = 0";
$db->query($sql);

$query = $db->query("SELECT id, url FROM list_link_category_ts");
$link_category = $db->rows($query);

foreach ($link_category as $category) {
    $data = file_get_html($category['url']);

    $url = array();

    if (count($data->find('div.content div.search-result-options div.pagination li.page-number'))) {
        foreach ($data->find('div.content div.search-result-options div.pagination li.page-number') as $div) {
            $url[] = $div->href;
        }
    }

    $data->clear();
    unset($data);

    $pages = count($url);
    $count = 48;

    for ($i = 1; $i < $pages; $i++) {
        $data = file_get_html($category['url'] . '?start=' . $i * $count);

        $tempArr = array();
        if (count($data->find('div.tiles-container div.grid-tile div.product-tile div.quickview-anchor div.product-image a'))) {
            foreach ($data->find('div.tiles-container div.grid-tile div.product-tile div.quickview-anchor div.product-image a') as $div) {
                $tempArr[] = $div->href;
            }
        }

        $data->clear();
        unset($data);

        foreach ($tempArr as $link) {
            $sql = "select * from list_link_products_ts where url like '$link'";// and (category = " . $category['id'] . " or category = 0)
            $query = $db->query($sql);
            $link_product = $db->rows($query);

            if (count($link_product) > 0) {
                $id = '';
                foreach ($link_product as $row)
                    $id = $row['id'];
                $sql = "UPDATE list_link_products_ts set active = 1 , category = " . $category['id'] . " where id = " . $id;//. " and ( category = " . $category['id'] . " or category = 0 )"
                $itemUpdate++;
            } else {
                $sql = "INSERT INTO list_link_products_ts (url,category,active) VALUES('$link', " . $category['id'] . ",1)";
                $itemInsert++;
            }

            $db->query($sql);
        }


    }
}

endUpdateProducts('2',$itemUpdate,$itemInsert);

?>