<?php

include_once dirname(__DIR__) . '/config.php';

set_time_limit(0);
ini_set('max_execution_time', 0);

error_reporting(E_ERROR | E_WARNING | E_PARSE);

class Database
{
    public $mysql;
    public $insert_id;

    public function __construct($host = 'localhost', $user = DB_USERNAME, $pass = DB_PASSWORD, $db_name = DB_DATABASE){
        try {
            $this->mysql = new mysqli($host, $user, $pass);
            if ($this->mysql->connect_error) {
                throw new Exception("Не могу подключиться");
            }
            $this->mysql->query('SET NAMES UTF8');
            if (!$this->mysql->select_db($db_name)) {
                throw new Exception("Не могу выбрать БД");
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $this->mysql->query('SET NAMES UTF8');
    }

    public function  __destruct(){
        $this->mysql->close();
    }

    public function query($SQL, $need_result = 1){
        try {
            $res = $this->mysql->query($SQL);
            $this->insert_id = $this->mysql->insert_id;
                 return $res;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getRows($sql)
    {
        $result = $this->query($sql);

        if ($this->mysql->errno !== 0) {
            throw new RuntimeException('Error: ' . mysqli_error($this->mysql) . '<br />Error No: ' . mysqli_errno($this->mysql) . '<br />' . $sql);
        }
        if (is_object($result)) {
            $i = 0;

            $data = array();

            while ($row = $result->fetch_assoc()) {
                $data[$i] = $row;

                $i++;
            }

            $result->close();

            $rows = $data;

            unset($data);

            return $rows;
        }
        return true;
    }

    public function getRow($sql)
    {
        $result = $this->query($sql);

        if ($this->mysql->errno !== 0) {
            throw new RuntimeException('Error: ' . mysqli_error($this->mysql) . '<br />Error No: ' . mysqli_errno($this->mysql) . '<br />' . $sql);
        }
        if (is_object($result)) {
            $data = array();

            if ($row = $result->fetch_assoc()) {
                $data[0] = $row;
            }

            $result->close();

            $returnData = isset($data[0]) ? $data[0] : array();

            unset($data);

            return $returnData;
        }
        return true;
    }

    public function real_escape_string($str) {
        return $this->mysql->real_escape_string($str);
    }

    public function sql($str) {
        return $this->real_escape_string($str);
    }

    public function rows(mysqli_result $obj) {
        $result = array();
        while ($item = $obj->fetch_array()) {
            $result[] = $item;
        }
        return $result;
    }

    public function row(mysqli_result $obj) {
        return $obj->fetch_array();
    }
}
