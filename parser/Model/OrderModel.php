<?php

namespace Nemepik\Model;

use Database;

class OrderModel
{
    private $db;

    /**
     * OrderModel constructor.
     * @param $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function getOrder($orderId)
    {
        $sql = 'SELECT order_id FROM oc_order WHERE order_id = ' . $orderId;
        $order = $this->db->getRow($sql);

        $sql = 'SELECT op.product_id, op.name, op.model, op.quantity, p.parent_shop_id, p.parent_shop_product_id,
                psp.parent_shop_product_url as parser_path, ps.parent_shop_url AS parsed_shop
            FROM oc_order_product AS op 
            LEFT JOIN oc_product AS p ON p.product_id = op.product_id
            LEFT JOIN oc_parent_shops_product AS psp ON psp.parent_shop_product_id = p.parent_shop_product_id
            LEFT JOIN oc_parent_shops AS ps ON ps.parent_shop_id = p.parent_shop_id
            WHERE order_id = ' . (int)$orderId;
        $order['products'] = $this->db->getRows($sql);


        return $order;
    }
}