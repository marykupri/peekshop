<?php

use VDB\Spider\Discoverer\XPathExpressionDiscoverer;
use Symfony\Component\EventDispatcher\Event;
use VDB\Spider\Event\SpiderEvents;
use VDB\Spider\StatsHandler;
use VDB\Spider\Spider;

require_once __DIR__ . '../../vendor/autoload.php';
require_once "db-classes.php";

require_once "parseFunck.php";

$db = new Database();

startUpdateProducts('2');
$itemUpdate = 0;
$itemInsert = 0;

$sql = "UPDATE oc_parent_shops_product set active = 0";
$db->query($sql);

$query = $db->query("SELECT parent_shop_category_id as id, parent_shop_category_url as url,parent_shop_id 
                        FROM oc_parent_shops_category 
                          where parent_shop_id=1");
$link_category = $db->rows($query);

foreach ($link_category as $category) {

    $data = file_get_html($category['url']);

    if($data) {

        $url = array();

        if (count($data->find('div.js-filter div.pagination-wrapper ol li a'))) {
            foreach ($data->find('div.js-filter div.pagination-wrapper ol li a.qa-pagination-last') as $div) {
                $url[] = $div->href;
            }
        }

        $data->clear();
        unset($data);

        $last_page = array_pop($url);
        $last_page = explode('?', $last_page);
        if (isset($last_page[1]))
            $page = str_replace('page=', '', $last_page[1]);

        for ($i = 1; $i <= $page; $i++) {

            $currUrl = $category['url'] . '?page=' . $i;

            $code = get_headers($currUrl);
            if ($code[0] == "HTTP/1.1 200 OK") {

                $data = file_get_html($currUrl);
                if ($data !== false) {

//        echo count($data->find('div.prvWrapper ol.productList li.productList-item article a.js-productTile-link')) . "\n\n";//li.productList-item article.productTile a.js-productTile-link

                    $tempArr = array();
                    $itemUrlArr = array();
                    if (count($data->find('div.prvWrapper ol.productList li.productList-item article'))) {
                        $prop = 'data-product-id';
                        foreach ($data->find('div.prvWrapper ol.productList li.productList-item article') as $div) {
                            $item_id = $div->$prop;
                            $href = '';

                            $data_str = str_get_html($div);
                            if (count($data_str->find(' a[itemprop=url]'))) {
                                foreach ($data_str->find(' a[itemprop=url]') as $str) {
                                    $href = $str->href;
                                }
                            }

                            $tmpItemArr = array();

                            if (!array_search($item_id, $tempArr) && !empty($href)) { //зачем эта странная проверка в массиве?!
                                $tempArr[] = $item_id;
                                $tmpItemArr['id'] = $item_id;
                                $tmpItemArr['url'] = $href;

                                $itemUrlArr[] = $tmpItemArr;
                            }
                        }
                    }
                }

                $data->clear();
                unset($data);

                foreach ($itemUrlArr as $url) {
                    $sql = "select * from oc_parent_shops_product where product_model = " . $url['id'];// and (category = " . $category['id'] . " or category = 0)
                    $query = $db->query($sql);
                    $link_product = $db->rows($query);

                    if (count($link_product) > 0) {
                        $id = '';
                        foreach ($link_product as $row)
                            $id = $row['parent_shop_product_id'];
                        $sql = "UPDATE oc_parent_shops_product set active = 1 , parent_shop_category_id = " . $category['id'] . " where parent_shop_product_id = " . $id;//. " and ( category = " . $category['id'] . " or category = 0 )"
                        $itemUpdate++;
                    } else {
                        $sql = "INSERT INTO oc_parent_shops_product (product_model,parent_shop_product_url,parent_shop_category_id,parent_shop_id,active) VALUES('" . $url['id'] . "', '" . $url['url'] . "', " . $category['id'] . "," . $category['parent_shop_id'] . ",1)";
                        $itemInsert++;
                    }

                    $db->query($sql);
                }
            }
        }

        sleep(10);
    }
}

endUpdateProducts('2',$itemUpdate,$itemInsert);

?>