<?php

date_default_timezone_set('Europe/Moscow');

define('LOGIN', 'marykupri@gmail.com');
define('PASSWORD', 'MK123123');
define('PC_URL', 'https://www.peek-cloppenburg.at');

require __DIR__ . '/../config.php';
require __DIR__ . '/../vendor/autoload.php';

use Nemepik\Model\OrderModel;
use Nemepik\PeekCloppenburgClient;

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

/**
 * @param $orderData
 * @return mixed
 */
function _rearangeOrderData($orderData)
{
    $storesArray = [];
    foreach ($orderData['products'] as $key => $value) {
        $storesArray[$value['parsed_shop']][] = $value;
    }
    unset($orderData['products']);
    $orderData['stores'] = $storesArray;
    return $orderData;
}

$orderModel = new OrderModel(new Database('172.22.0.4', DB_USERNAME, DB_PASSWORD, DB_DATABASE));
$orderData = _rearangeOrderData($orderModel->getOrder(44));

foreach ($orderData['stores'] as $storeUrl => $productData) {
    if ($storeUrl === PC_URL) {
        $client = new PeekCloppenburgClient();
        foreach ($productData as $product) {
            for ($i = 0; $i < $product['quantity']; $i++) {
                $client->addToCart($product);
            }
            die;
        }
        $client->checkoutOrder();
    }
}

