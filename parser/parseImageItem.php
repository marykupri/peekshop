<?php
date_default_timezone_set('Europe/Moscow');
require_once __DIR__ . '../../vendor/autoload.php';
require_once "db-classes.php";
require_once "parseFunck.php";

$db = new Database();

$page = 25;
$count = 0;

$query = $db->query("SELECT count(*) as cnt FROM list_link_products where category = 10");

$row = $db->row($query);

//foreach ($query as $item => $url)
$count = $row['cnt'];

$count = ceil($count / $page);

$arr_images = array();

for ($i = 0; $i <= $count; $i++) {

    $start = $i * $page;

    $query = $db->query("SELECT id,url,active FROM list_link_products where category = 10 limit " . $start . ',' . $page);
    $query = $db->rows($query);

        $items = array();

        foreach ($query as $item => $url) {

            if (!preg_match("/page=[0-9]/i", $url['url'])) {

                $src_img = '';
                $artikul = '';

                $code=get_headers($url['url']);
                if($code[0] == "HTTP/1.1 200 OK") {
                    ini_set("max_execution_time", "1200");

                    $data = file_get_html($url['url']);

                    if (count($data->find('div#tabpanel-description'))) {
                        foreach ($data->find('div#tabpanel-description div.grid meta[itemprop=sku]') as $div)
                            $artikul = $div->content;
                    }

                    if (count($data->find('div.productGallery--full div'))) {
                        $property = 'data-original-set';
                        $prop = 'srcset';
                        $i=0;
                        foreach ($data->find('div.productGallery--full div img') as $div)
                        {
                            if($div->$property)
                                $images =  $div->$property;
                            if($div->$prop)
                                $images =  $div->$prop;

                            $images = explode(', ' , $images);

                            $vowels = array("1x", "2x", "//");

                            foreach ($images as $item =>$image) {
                                $image = str_replace($vowels, '', $image);

                                $path = __DIR__ . '/../image/catalog/product/';

                                if (preg_match("~900x1200f.jpg~", $image)) {

                                    $type = explode('/', $image);
                                    $type = array_pop($type);
                                    $type = explode('.', $type);
                                    $type = array_pop($type);
                                    $filename = $artikul . '_' . $i . '.' . $type;

                                    $fpath = $path . $artikul . '_' . $i . '.' . $type;

                                    $file_url = 'http://' . $image ;

                                    if (!file_exists($fpath)) {
                                        copy($file_url, $fpath);
                                    }
                                    if($i == 0)
                                        $arr_images[$artikul]['main'] = $filename;
                                    $arr_images[$artikul]['slider'][] = $filename;
                                }
                            }
                            $i++;
                        }
                    }

                    $data->clear();
                    unset($data);
                }
            }
        }
        sleep(5);
    }

    foreach ($arr_images as $image => $img){

        $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE model = '".$image."'";
        $res = $db->query($sql);
        $product = $res->fetch_array();

        $sql = "UPDATE `" . DB_PREFIX . "product` SET `image`= 'catalog/product/" . $img['small'] . "' where `product_id`=".$product['product_id'];
        $db->query($sql);

        $sql = "DELETE FROM `" . DB_PREFIX . "product_image` WHERE product_id = " . $product['product_id'];
        $db->query($sql);

        foreach ($img['slider'] as $slider => $val){
            $sql = "INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = 'catalog/product/" . $this->db->escape($val) . "', sort_order = '" . (int)$slider . "'";
            $db->query($sql);
        }
    }

?>