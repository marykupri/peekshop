<?php
date_default_timezone_set('Europe/Moscow');
ini_set("allow_url_fopen", true);

require_once __DIR__ . '../../vendor/autoload.php';
require_once "db-classes.php";
require_once "parseFunck.php";

$baseUrl = 'https://www.peek-cloppenburg.at';

$db = new Database();

$page = 25;
$count = 0;

$itemUpdate = 0;
$itemInsert = 0;

startUpdateProducts('1');

$query = $db->query("SELECT count(*) as cnt FROM oc_parent_shops_product where parent_shop_id = 1");
$row = $db->row($query);

//foreach ($query as $item => $url)
$count = $row['cnt'];

$count = ceil($count / $page);

for ($i = 0; $i <= $count; $i++) {

    $start = $i * $page;

    $query = $db->query("SELECT parent_shop_product_id as id,parent_shop_product_url as url,product_model,active 
                          FROM oc_parent_shops_product 
                          WHERE parent_shop_id = 1
                          limit " . $start . ',' . $page);
    $query = $db->rows($query);

    $items = array();

    foreach ($query as $item => $url) {

        if (!preg_match("/page=[0-9]/i", $url['url'])) {

            $name = '';
            $brand = '';
            $color = array();
            $shortDescr = '';
            $description = '';
            $labelSizeArr = array();
            $labelSize = '';
            $labelMaterial = '';
            $breadcr = '';
            $artikul = '';
            $price = '';
            $specprice = '';
            $isNew = 0;
            $isSale = 0;
            $struct = array();

            $pageIsFalse = false;

            if ($url['active'] == 1) {

                $currUrl = $baseUrl . $url['url'];

//                $code = get_headers($currUrl);
//                if ($code[0] == "HTTP/1.1 200 OK") {

                    $data = file_get_html($currUrl);

                    if ($data !== false) {

//                    if (count($data->find('div#tabpanel-description div.grid div.grid-cell meta[itemprop=sku]'))) {
//                        foreach ($data->find('div#tabpanel-description div.grid div.grid-cell meta[itemprop=sku]') as $div)
//                            $artikul = $div->content;
//                    }

                        $artikul = $url['product_model'];

                        if (count($data->find('p.product-price--details meta[itemprop=price]'))) {
                            foreach ($data->find('p.product-price--details meta[itemprop=price]') as $div)
                                $price = $div->content;
                        }

                        if (count($data->find('p.product-price--details strong span.is-reduced'))) {
                            foreach ($data->find('p.product-price--details strong span.is-reduced') as $div)
                                $specprice = preg_replace('~[^0-9,]~', '', $div->innertext);
                        }

                        if (count($data->find('p.product-price--details strong span.is-regular'))) {
                            foreach ($data->find('p.product-price--details strong span.is-regular') as $div)
                                $price = preg_replace('~[^0-9,]~', '', $div->innertext);
                        }

                        if (count($data->find('div.productDetails-flagList span.is-sale'))) {
                            foreach ($data->find('div.productDetails-flagList span.is-sale') as $div)
                                $isSale = 1;
                        }

                        if (count($data->find('div.productDetails-flagList span.is-new'))) {
                            foreach ($data->find('div.productDetails-flagList span.is-new') as $div)
                                $isNew = 1;
                        }

                        if ($data->find('div.product-sizeVariants a[itemprop=isSimilarTo]')) {
                            foreach ($data->find('div.product-sizeVariants a[itemprop=isSimilarTo]') as $div) {

                                $href = $div->href;
                                $href = explode('/?', urldecode($href));
                                $code = str_replace('size=', '', $href[1]);

                                $span = '';

                                $data_str = str_get_html($div);
                                if (count($data_str->find('span.product-sizeLabel[aria-expanded=true]'))) {
                                    foreach ($data_str->find('span.product-sizeLabel[aria-expanded=true]') as $str) {
                                        $span = $str->innertext;
                                    }
                                } else {
                                    if (count($data_str->find('span.product-sizeLabel'))) {
                                        foreach ($data_str->find('span.product-sizeLabel') as $str) {
                                            $span = $str->innertext;
                                        }
                                    }
                                }

                                $tmpSizeArr['name'] = $span;
                                $tmpSizeArr['code'] = $code;

                                $labelSize[] = $tmpSizeArr;

                            }
                        }

                        if (empty($labelSize))
                            if ($data->find('div.product-sizeVariants span.product-sizeLabel')) {
                                foreach ($data->find('div.product-sizeVariants span.product-sizeLabel') as $div) {
                                    $href = 'data-href';
                                    $href = $div->$href;
                                    $href = explode('/?', urldecode($href));
                                    $code = str_replace('size=', '', $href[1]);

                                    $span = $div->innertext;

                                    $tmpSizeArr['name'] = $span;
                                    $tmpSizeArr['code'] = $code;

                                    $labelSize[] = $tmpSizeArr;

                                }
                            }

                        if (count($data->find('p[itemprop=brand] a')))
                            foreach ($data->find('p[itemprop=brand] a') as $div)
                                if (!empty($div->innertext))
                                    $brand = trim($div->innertext);

                        if (empty($brand))
                            if (count($data->find('p[itemprop=brand]')))
                                foreach ($data->find('p[itemprop=brand]') as $div)
                                    if (!empty($div->innertext))
                                        $brand = trim($div->innertext);

                        if (count($data->find('h1.product-title--details span[itemprop=color]')))
                            foreach ($data->find('h1.product-title--details span[itemprop=color]') as $div)
                                if (!empty($div->innertext))
                                    $color[] = trim($div->innertext);

                        if (!$itemId = checkProductExist($artikul)) {

                            if (count($data->find('h1.product-title--details'))) {
                                foreach ($data->find('h1.product-title--details') as $div)
                                    $shortDescr = htmlspecialchars(strip_tags($div->innertext, '<li>'));
                            }

                            if (count($data->find('div#tabpanel-description'))) {
                                foreach ($data->find('div#tabpanel-description div.grid ul.product-infoList--twoCol') as $div)
                                    $description = htmlspecialchars(strip_tags($div->innertext, '<li>'));

                                $description = str_replace(array("&lt;li&gt;", "&lt;/li&gt;", "&amp;nbsp;"), array(" - ", ";", " "), $description);
                            }

                            if (count($data->find('h1.product-title--details span[itemprop=name]')))
                                foreach ($data->find('h1.product-title--details span[itemprop=name]') as $div)
                                    if (!empty($div->innertext))
                                        $name = trim($div->innertext);

//                            if (count($data->find('h1.product-title--details span[itemprop=color]')))
//                                foreach ($data->find('h1.product-title--details span[itemprop=color]') as $div)
//                                    if (!empty($div->innertext))
//                                        $color[] = trim($div->innertext);

//                        if (count($data->find('p[itemprop=brand] a')))
//                            foreach ($data->find('p[itemprop=brand] a') as $div)
//                                if (!empty($div->innertext))
//                                    $brand = trim($div->innertext);
//
//                        if (empty($brand))
//                            if (count($data->find('p[itemprop=brand]')))
//                                foreach ($data->find('p[itemprop=brand]') as $div)
//                                    if (!empty($div->innertext))
//                                        $brand = trim($div->innertext);

                            $arr_images = array();
                            $arr_images = getParseItemImages($data, $artikul);

                            $data->clear();
                            unset($data);

                            $brand_id = get_brand_id($brand);

                            $data_item = array(
                                'model' => $artikul,
                                'quantity' => 10,
                                'language_id' => 2,
                                'minimum' => 1,
                                'status' => $url['active'],
                                'stock_status_id' => 7,
                                'manufacturer_id' => $brand_id,
                                'shipping' => 1,
                                'price' => $price,
                                'specprice' => $specprice,
                                'tax_class_id' => 9,
                                'description' => array(array(
                                    'name' => htmlspecialchars($name, ENT_QUOTES, "UTF-8"),
                                    'short_descr' => htmlspecialchars($shortDescr, ENT_QUOTES, "UTF-8"),
                                    'description' => htmlspecialchars($description, ENT_QUOTES, "UTF-8"),
                                    'details' => '',
                                    'material' => '',
                                )),
                                'item_size' => $labelSize,
                                'item_color' => $color,
                                'category' => 64,
                                'id_shop' => 1,
                                'id_link' => $url['id'],
                                'images' => $arr_images,
                                'isNew' => $isNew,
                                'isSale' => $isSale
                            );

//                $pr_filter_arr = array();
//                if (isset($color)) {
//                    $pr_filter_arr[] = array(
//                        'type_id' => 5,
//                        'attribute_id' => 12,
//                        'text' => $color,
//                    );
//                    $data_item['filter'] = $pr_filter_arr;
//                }

                            $result = insertItem($data_item);
                            if ($result)
                                $itemInsert++;

                        } else {
                            $images = checkProductImgsExist($artikul);
                            $arr_images = array();

                            if (!$images) {
                                $arr_images = getParseItemImages($data, $artikul);
                            }

                            $data->clear();
                            unset($data);

                            $brand_id = get_brand_id($brand);

                            $data_item = array(
                                'model' => $artikul,
                                'quantity' => 10,
                                'status' => $url['active'],
                                'price' => $price,
                                'manufacturer_id' => $brand_id,
                                'specprice' => $specprice,
                                'description' => array(array(
                                    'name' => htmlspecialchars($name, ENT_QUOTES, "UTF-8"),
                                )),
                                'item_size' => $labelSize,
                                'item_color' => $color,
                                'id_shop' => 1,
                                'id_link' => $url['id'],
                                'images' => $arr_images,
                                'isNew' => $isNew,
                                'isSale' => $isSale
                            );

                            updateItem($itemId, $data_item);
                            $itemUpdate++;
                        }
                    } else
                       $pageIsFalse = true;
//                } else
//                    $pageIsFalse = true;
            } else
                $pageIsFalse = true;

            if ($pageIsFalse) {
                $sql = "UPDATE `oc_product` SET `status`=0, date_modified = now() WHERE `model` = " . $url['product_model'];
                $db->query($sql);
                $itemUpdate++;
            }
        }
    }
    sleep(10);
}

endUpdateProducts('1', $itemUpdate, $itemInsert);

function getParseItemImages($data, $artikul)
{
    $arr_images = array();
    if (count($data->find('div.productGallery--full div'))) {
        $property = 'data-original-set';
        $prop = 'srcset';
        $i1 = 0;
        foreach ($data->find('div.productGallery--full div img') as $div) {
            if ($div->$property)
                $images = $div->$property;
            if ($div->$prop)
                $images = $div->$prop;

            $images = explode(', ', $images);

            $vowels = array(" 1x", " 2x");

            foreach ($images as $item => $image) {
                $image = str_replace($vowels, '', $image);

                $path = __DIR__ . '/../image/catalog/product/';

                if (preg_match("~900x1200f.jpg~", $image)) {

                    $type = explode('/', $image);
                    $type = array_pop($type);
                    $type = explode('.', $type);
                    $type = array_pop($type);
                    $filename = $artikul . '_' . $i1 . '.' . $type;

                    $fpath = $path . $filename;

                    $file_url = 'http:' . $image;

                    if (!file_exists($fpath)) {
                        copy($file_url, $fpath);
                    }
                    if ($i1 == 0)
                        $arr_images['main'] = $filename;
                    $arr_images['slider'][] = $filename;

                }
            }
            $i1++;
        }
    }

    return $arr_images;
}

//updateCategoryFilters();

?>
