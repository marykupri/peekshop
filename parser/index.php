<?php
error_reporting(E_ALL);
/**
 * http://volgoprint.ru/_daemons/get_gifts_daemon.php
 * - сопоставление категорий на разных сайтах (выпадающий список - volgoprint.ru)
 * также устанавливает связи в таблице oc_product_to_category, но не удаляет их: надо внимательно пользоваться, либо модифицировать
 *
 * доп функции:
 * _daemons/get_gifts_daemon.php?step0=1
 * - начальные установки - парсит tree.xml - создаёт таблицы от gifts, и заполняет их
 *
 * _daemons/get_gifts_daemon.php?do_parse_products=1
 * - парсит stock.xml и product.xml и заполняет из них данные по товарам, скачивает изображения товаров
 * - xml-файлы - хранятся локально
 */

require_once "db-classes.php";

$db = new Database();

$color_code_arr = getColors();

//getDataItem();

function getDataItem() {
        $FNameI = 'files/dataItem.csv';

        $Handl = fopen($FNameI, 'r');

        if ($Handl) {

            while (!feof($Handl))
                if (!feof($Handl)) {
                    $SBuf = fgets($Handl);
                    if (!empty($SBuf)) {
                        $mass = explode(';', $SBuf);
                        $name = '';
                        $artikul = '';
                        $brand = '';
                        $color = '';
                        $size = '';
                        $short_descr = '';
                        $description = '';
//                        $structure = '';


                        if (!empty($mass[0])) {
                            $name = trim($mass[0]);
                            $name = iconv("ISO-8859-1", "UTF-8", $name);
                        }

                        if (!empty($mass[1])) {
                            $artikul = trim($mass[1]);
                        }

                        if (isset($mass[2])) {
                            $brand = trim($mass[2]);
                            $brand = iconv("ISO-8859-1", "UTF-8", $brand);
                        }

                        if (isset($mass[3])) {
                            $color = trim($mass[3]);
                            $color = iconv("ISO-8859-1", "UTF-8", $color);
                        }

                        if (isset($mass[4])) {
                            $size = trim($mass[4]);
                            $size = explode('|',$size);
                        }

                        if (isset($mass[4])) {
                            $short_descr = trim($mass[5]);
                            $short_descr = iconv("ISO-8859-1", "UTF-8", $short_descr);
                        }

                        if (!empty($mass[6])) {
                            $description = trim($mass[6]);
                            $description = str_replace('//',';',$description);
                            $description = iconv("ISO-8859-1", "UTF-8", $description);
                        }

//                        if (!empty($mass[7])) {
//                            $structure = trim($mass[7]);
//                            $structure = iconv("ISO-8859-1", "UTF-8", $structure);
//                        }

                        $brand_id = get_brand_id($brand);

                        $data_item = array(
                            'model' => $artikul,
                            'quantity' => 10,
                            'language_id' => 2,
                            'minimum' => 1,
                            'status' => 1,
                            'stock_status_id' => 7,
                            'manufacturer_id' => $brand_id,
                            'shipping' => 1,
                            'price' => 100,
                            'tax_class_id' => 9,
                            'description' => array(array(
                                'name' => htmlspecialchars($name,ENT_QUOTES,"UTF-8"),
                                'short_descr' => htmlspecialchars($short_descr,ENT_QUOTES,"UTF-8"),
                                'description' => htmlspecialchars($description,ENT_QUOTES,"UTF-8")
                            )),
                            'item_size' => $size,
                            'category' => 64
                        );

                        //------

                        $pr_filter_arr = array();
                        if (isset($color)){
                            $pr_filter_arr[] = array(
                                'type_id' => 5,
                                'attribute_id' => 12,
                                'text' => $color,
                            );
                            $data_item['filter'] = $pr_filter_arr;
                        }

                        insertItem($data_item);

                    }
                }

        } else {
            $ResOut = "Файл  $FNameI не открылся\n";
        }

}

function get_brand_id($brand){
    global $db;
    if(empty($brand)) return 0;

    $brand = htmlspecialchars(trim($brand),ENT_QUOTES,"UTF-8");
    $sql = "select manufacturer_id from oc_manufacturer where name like '$brand'";
    $res = $db->query($sql);
    $row = $db->row($res);

    if(!$row){
        $brand = str_replace("'","\'",$brand);
        $sql = "INSERT INTO `oc_manufacturer`(`name`, `sort_order`) VALUES ('$brand',0)";
        $db->query($sql);
        $id = $db->insert_id;
        $sql = "INSERT INTO `oc_manufacturer_to_store`(`manufacturer_id`, `store_id`) VALUES ($id,0)";
        $db->query($sql);
        return $id;
    }

    $id = $row['manufacturer_id'];

    return $id;
}

function insertItem($data){

    if(empty($data)) return;

    if(!$itemId = checkProductExist($data['model']))
        $itemId = addItem($data);

    insertItemOption($data,$itemId);
    insertItemAtribute($data,$itemId);
}

function getColors() {
    global $db;

    $sql = "select vd.option_value_id, vd.name
        from " . DB_PREFIX . "option_value_description as vd
        WHERE language_id = 1
        AND option_id = 12";
    $result_db = $db->query($sql);

    $data = array();

    while ($item = $result_db->fetch_array()) {
        $data[$item['option_value_id']] = $item['name'];
    }

    return $data;
}

function addItem($data){
    global $db;

    $sql ="INSERT INTO `oc_product`(`model`,`quantity`, `stock_status_id`,`manufacturer_id`,`shipping`, `price`,`tax_class_id`, `minimum`, `status`, `date_added`) 
            VALUES (" . $db->sql($data['model']) . "," . (int)$data['quantity'] . "," . (int)$data['stock_status_id'] . "," . (int)$data['manufacturer_id'] . "," . (int)$data['shipping'] . "," . (float)$data['price'] . "," . (int)$data['tax_class_id'] . "," . (int)$data['minimum'] . "," . (int)$data['status'] . ",NOW())";

    $db->query($sql);
    $product_id = $db->insert_id;

    foreach ($data['description'] as $language_id =>$value) {
        $sql ="INSERT INTO `oc_product_description`(`product_id`, `language_id`, `name`, `description`)
                VALUES (" . (int)$product_id . ",1,'" . $value['name'] . "','" . $value['description'] . "')";

        $db->query($sql);
    }

    // таблица привязки продукта к товару
    $sql = "INSERT into " . DB_PREFIX . "product_to_store values ('" . (int)$product_id . "','0')";
    $db->query($sql);

    if ($data['category']) {
        // определение родительских категорий
        $sql = "SELECT * from oc_category_path as cp
            where category_id in ('" . (int)$data['category'] . "')
            and `level` > 0";
        $res = $db->query($sql);

        while ($item = $res->fetch_array()) {
            // таблица привязки категории к продукту
            $sql = "INSERT into " . DB_PREFIX . "product_to_category values('" . (int)$product_id . "','" . (int)$item['path_id'] . "')";
            $db->query($sql);
        }
    }

    return $product_id;
}

function checkProductExist($code) {
    global $db;

    $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE model = '".$code."' LIMIT 1";
    $res = $db->query($sql);
    $product = $res->fetch_array();
    return isset($product['product_id']) ? $product['product_id'] : false;
}

function insertItemOption($data,$product_id){
    global $db;

    $sql = "INSERT INTO `oc_product_option` (`product_id`, `option_id`, `required`) VALUES ('" . $product_id . "', 11, 1)";
    $db->query($sql);

    $product_option_id = $db->insert_id;

    foreach ($data['item_size'] as $size_code){
        $option_value_id = getOptionValueId($size_code);
        addProductOption($product_id, $option_value_id, $product_option_id, $data);
    }

}

function insertItemAtribute($data,$product_id){
    global $db;

    if (!empty($data['filter'])) {
        foreach ($data['filter'] as $f) {

            if (!empty($f['attribute_id'])) {
                $sql = "REPLACE INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`)
                                VALUES ('" . (int)$product_id . "', '" . (int)$f['attribute_id'] . "', 1, '" . $db->sql($f['text']) . "')";
                $db->query($sql);
            }
        }
    }
}

/**
 * Получение option_value_id
 * Сохранение размера, если нет
 * @param $size_code
 * @return bool|int|string
 */
function getOptionValueId($code) {
    global $color_code_arr;

    if (!in_array($code, $color_code_arr)) {
        // добавление размера
        global $db;
        $sql = "INSERT INTO `oc_option_value` (`option_id`, `image`, `sort_order`) VALUES (11, 'no_image.jpg', 5)";
        $db->query($sql);

        $option_id = $db->insert_id;

        $sql = "INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES (" . (int)$option_id . ", 1, 11, '" . $db->sql($code) . "');";
        $db->query($sql);

        $color_code_arr[$option_id] = $code;

        return $option_id;
    } else {
        foreach ($color_code_arr as $key=>$color){
            if ($color == $code) {return $key;}
        }
    }

    return false;
}

function addProductOption($product_id, $option_value_id, $product_option_id, $data){
    global $db;

    if (!$option_value_id) {return false;}

    $sql="select * from `oc_product_option_value`
            where `product_option_id` = ".(int)$product_option_id." and
            `product_id` = ".$product_id." and
            `option_id` = 11 and `option_value_id` = ".(int)$option_value_id;
    $result = $db->query($sql);
    $res = $result->fetch_array();

    if(!empty($res))
        return;

    $sql = "REPLACE INTO `oc_product_option_value`
            (`product_option_id`, `product_id`, `option_id`,
            `option_value_id`, `quantity`, `subtract`,
            `price`, `price_prefix`, `points`,
            `points_prefix`, `weight`, `weight_prefix`)
            VALUES
            (".(int)$product_option_id.", ".$product_id.", 11,
            ".(int)$option_value_id.", ".(int)$data['quantity'].", 1,
            0, '+', 0,
            '+', 0.00000000, '+')";

    $db->query($sql);
}