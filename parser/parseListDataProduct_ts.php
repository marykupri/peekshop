<?php
date_default_timezone_set('Europe/Moscow');
ini_set("allow_url_fopen", true);

require_once __DIR__ . '../../vendor/autoload.php';
require_once "db-classes.php";
require_once "parseFunck.php";

$baseUrl = 'https://www.peek-cloppenburg.at';

$db = new Database();

$page = 25;
$count = 0;

$itemUpdate = 0;
$itemInsert = 0;

startUpdateProducts('1');

$query = $db->query("SELECT count(*) as cnt FROM list_link_products_ts");
$row = $db->row($query);

$count = $row['cnt'];

$count = ceil($count / $page);

for ($i = 0; $i <= $count; $i++) {

    $start = $i * $page;

    $query = $db->query("SELECT id,url,active FROM list_link_products_ts limit " . $start . ',' . $page);
    $query = $db->rows($query);

    $items = array();

    foreach ($query as $item => $url) {

        $name = '';
        $brand = 'Thomas Sabo';
        $color = '';
        $shortDescr = '';
        $description = '';
        $details = '';
        $labelSizeArr = array();
        $labelMaterialArr = array();
        $labelMaterial = '';
        $labelSize = '';
        $artikul = '';
        $price = '';
        $specprice = '';
        $isNew = 0;
        $isSale = 0;

        if ($url['active'] == 1) {
//
            $currUrl = $url['url'];

            $code = get_headers($currUrl);
            if ($code[0] == "HTTP/1.1 200 OK") {

                $data = file_get_html($currUrl);

                if (count($data->find('div.product-information meta[itemprop=identifier]'))) {
                    foreach ($data->find('div.product-information meta[itemprop=identifier]') as $div)
                        $artikul = $div->content;

                    $artikul = trim(str_replace('sku:', '', $artikul));
                }

                if (count($data->find('div.product-information span div.product-price span.price-sales meta[itemprop=price]'))) {
                    foreach ($data->find('div.product-information span div.product-price span.price-sales meta[itemprop=price]') as $div)
                        $price = $div->content;
                }

                if (count($data->find('div.product-information span div.product-variations'))) {

                    foreach ($data->find('div.product-information span div.product-variations div.size-container select option') as $div) {
                        if ($div->innertext != 'Größe')
                            $labelSizeArr[] = trim($div->innertext);
                    }

                    if (empty($labelSizeArr))
                        foreach ($data->find('div.product-information span div.product-variations div.size-container div.size-display') as $div) {
                            $size = str_replace('Länge:', '', $div->innertext);
                            $labelSizeArr[] = trim($size);
                        }
                }

                if (count($data->find('div.product-details'))) {
                    foreach ($data->find('div.product-details div.productdetail-description') as $div)
                        $description = htmlspecialchars($div->innertext);

                    $description = str_replace(array("&lt;li&gt;", "&lt;br /&gt;", "&amp;nbsp;"), array(" - ", "; ", " "), $description);

                    foreach ($data->find('div.product-details div.productdetail-attributes div.row') as $div)
                        $details .= $div->innertext;

                    $details = str_replace(array("</span><span class=\"value\">", "</span><span class=\"label\">", "&amp;nbsp;"), array("</span>: <span class=\"value\">", "</span>; <span class=\"label\">", " "), $details);
                    $details = htmlspecialchars(strip_tags($details));

                    $temp_arr = explode(';', $details);
                    foreach ($temp_arr as $val)
                        if(preg_match('~Farbe:~is', $val)) {
                            $val = str_replace('Farbe:' ,'',trim($val));
                            $color = explode(',', $val);
                        }
                }

                $brand_id = get_brand_id($brand);

                if (!$itemId = checkProductExist($artikul)) {

                    if (count($data->find('div.product-information div.product-name'))) {
                        foreach ($data->find('div.product-information div.product-name span.line') as $div)
                            $shortDescr .= $div->innertext . " - ";

                        foreach ($data->find('div.product-information div.product-name span.series') as $div)
                            $shortDescr .= $div->innertext . " - ";
                    }

                    if (count($data->find('div.product-information div.product-name h1')))
                        foreach ($data->find('div.product-information div.product-name h1') as $div)
                            if (!empty($div->innertext))
                                $name = trim($div->innertext);

                    $shortDescr .= $name;

                    if (count($data->find('div.product-information span div.product-variations li.config-color')))
                        foreach ($data->find('div.product-information span div.product-variations li.config-color ul') as $div) {
                            $property = 'data-textbefore';
                            $material = $div->$property;
                            $material = trim(str_replace('Variation:', '', $material));
                            $labelMaterial = $material;
                            $material = explode(';', $material);
                            foreach ($material as $value){
                                $labelMaterialArr[] = $value;
                            }
                        }

                    $arr_images = array();
                    $arr_images = getParseItemImages($data, $artikul);

                    $data->clear();
                    unset($data);

                    $data_item = array(
                        'model' => $artikul,
                        'quantity' => 10,
                        'language_id' => 2,
                        'minimum' => 1,
                        'status' => $url['active'],
                        'stock_status_id' => 7,
                        'manufacturer_id' => $brand_id,
                        'shipping' => 1,
                        'price' => $price,
//                            'specprice' => $specprice,
                        'tax_class_id' => 9,
                        'description' => array(array(
                            'name' => htmlspecialchars($name, ENT_QUOTES, "UTF-8"),
                            'short_descr' => htmlspecialchars($shortDescr, ENT_QUOTES, "UTF-8"),
                            'description' => htmlspecialchars($description, ENT_QUOTES, "UTF-8"),
                            'details' => htmlspecialchars($details, ENT_QUOTES, "UTF-8"),
                            'material' => $labelMaterial, //строка материалы чтобы сохранить в столбец "material"
                        )),
                        'item_size' => $labelSizeArr, //массив размеров
                        'materials' => $labelMaterialArr, //массив материалов
                        'item_color' => $color, //массив цветов
                        'category' => 64,
                        'id_shop' => 2,
                        'id_link' => $url['id'],
                        'images' => $arr_images
                    );

                    insertItem($data_item);
                    $itemInsert++;

                } else {
                    $images = checkProductImgsExist($artikul);
                    $arr_images = array();

                    if (!$images) {
                        $arr_images = getParseItemImages($data, $artikul);
                    }

                    $data->clear();
                    unset($data);

                    $data_item = array(
                            'model' => $artikul,
                            'quantity' => 10,
                            'status' => $url['active'],
                            'price' => $price,
                            'manufacturer_id' => $brand_id,
//                            'specprice' => $specprice,
                            'item_size' => $labelSize,
                            'item_color' => $color,
                            'id_shop' => 2,
                            'id_link' => $url['id'],
                            'images' => $arr_images
                        );

                        updateItem($itemId, $data_item);
                        $itemUpdate++;
                }
            }
            }else{
                $artikul = explode('-', $url['url']);
                $artikul = explode('_', array_pop($artikul));

                $sql = "UPDATE `oc_product` SET `status`=0 WHERE `id_link` = " . $url['id'];
                $db->query($sql);
                $itemUpdate++;
            }
        }
    sleep(5);
}

endUpdateProducts('1',$itemUpdate,$itemInsert);

function getParseItemImages($data, $artikul)
{
    $arr_images = array();
    if (count($data->find('div.product-images div.product-image-slider span.slide-item'))) {
        $property = 'data-zoom-url';
        $i1 = 0;
        foreach ($data->find('div.product-images div.product-image-slider span.slide-item') as $div) {
            $image = $div->$property;

            $path = __DIR__ . '/../image/catalog/product/';

            $type = explode('?', $image);
            $type = explode('/', $type[0]);
            $type = array_pop($type);
            $type = explode('.', $type);
            $type = array_pop($type);
            $filename = $artikul . '_' . $i1 . '.' . $type;

            $fpath = $path . $filename;

            $file_url = $image;

            if (!file_exists($fpath)) {
                copy($file_url, $fpath);
            }
            if ($i1 == 0)
                $arr_images['main'] = $filename;
            $arr_images['slider'][] = $filename;

            $i1++;
        }
    }

    return $arr_images;
}

updateCategoryFilters();

?>