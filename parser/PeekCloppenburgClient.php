<?php

namespace Nemepik;

use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * @property Client $client
 */
class PeekCloppenburgClient
{

    private $client;

    public function __construct()
    {
        $client = new Client([
            'base_uri' => PC_URL,
            'timeout' => 2.0,
            'cookies' => true,
        ]);

        $responseSearchCSRF = $client->request('GET', '/myaccount/login');

        $body = $responseSearchCSRF->getBody();

        $csrf = $this->_getCSFR($body, 'div.loginBox form input[name=_csrf]');

        $responseLogin = $client->request('POST', '/myaccount/login/',
            [
                'form_params' => [
                    '_csrf' => $csrf,
                    'email' => LOGIN,
                    'password' => PASSWORD
                ]
            ]
        );

//    dump($responseLogin);
//    $body = $responseLogin->getBody();
//    echo $body;

        $this->client = $client;
    }


    /**
     * @param $body
     * @return bool|mixed|string
     */
    private function _getCSFR($body, $selector)
    {
        $data = HtmlDomParser::str_get_html($body);

        $csrf = '';
        $foundedObjects = $data->find($selector);
        if (count($foundedObjects)) {
            foreach ($foundedObjects as $div) {
                $csrf = $div->value;
            }
        }

        $data->clear();
        unset($data);
        return $csrf;
    }

    /**
     * @param array $product
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getProductCard($product)
    {
        return $this->getPage($product['parser_path']);
    }


    private function getCheckoutPage()
    {
        return $this->getPage('/checkout/cart/');
    }


    /**
     * @param array $product
     */
    public function addToCart($product)
    {
        $csrf = $this->_getCSFR($this->getProductCard($product), 'div.js-slice-colourVariantSwitch form input[name=_csrf]');
        //TODO получать реальный размер товара
        $product['size'] = '_1,0';
        $response = $this->client->request('POST', '/cart/add/' . $product['model'] . $product['size'],
            [
                'form_params' => [
                    '_csrf' => $csrf
                ]
            ]
        );
//        echo $response->getBody();
    }

    public function checkoutOrder()
    {
        $this->getCheckoutPage();
    }


    private function getPage($path) {
        $response = $this->client->request('GET', $path);
        return $response->getBody();
    }
}